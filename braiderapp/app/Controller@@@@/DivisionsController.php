<?php
ob_start();
 class DivisionsController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  var $helpers=array("Html","Form","Session");

 //   function beforeFilter(){
	// 	$this->disableCache();
	// 	parent::beforeFilter();
 //        if(!$this->CheckAdminSession() && $this->request->prefix=='admin' && !in_array($this->request->action,array('admin_addShow','admin_viewShow'))){
	// 			$this->redirect(array('action' => 'addShow','admin' => true));
	// 			exit();
	// 	}
	// }
  function admin_add_division()
  {
 $this->layout="admin";

   
   $this->loadModel('Division');

   if ($this->request->is('post')) {

      $data = $this->data;
       if($this->Division->save($data)){
        $this->Session->write('error','Username or Password is invalid.');    
          return $this->redirect(array('action' => 'admin_view_division'));
    
       } else {
        $this->Session->write('error','Username or Password is invalid.');    
      }


            
    }

  }

 
  function admin_view_division()
  {
     $this->layout="admin";
    $this->set('divisions', $this->Division->find('all'));
  
   }  
  

  function admin_edit_division($id = null)
  { 

     $this->layout="admin";
 
        if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $divisions = $this->Division->findById($id);
    if (!$divisions) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Division->id = $id;
        if ($this->Division->save($this->request->data)) {
            //$this->Session->setFlash(__('Your post has been updated.'));
            return $this->redirect(array('action' => 'admin_view_division'));
        }
        //$this->Session->setFlash(__('Unable to update your post.'));
    }

    if (!$this->request->data) {
        $this->request->data = $divisions;
    }

}



public function admin_deleteDivision($id) {
   if ($this->request->is('post')) {
    if ($this->Division->delete($id)) {
      //$this->Session->setFlash(__('The post number %s has been deleted.', h($id)));
      return $this->redirect(array('action' => 'admin_view_division'));
    }
  }
}
  
 


 }

?>
