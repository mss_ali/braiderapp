<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	 var $components=array('Session','Email','RequestHandler','Paginator');
	function checkAdminSession(){
		$adminSession = $this->Session->read('User');
		if(isset($adminSession['id']) && isset($adminSession['email']) && ($adminSession['usertype'] == 'admin')){
			return true;
		}
		else{
			return false;
		}
	}

	function UniqueKey() {
        return md5(uniqid(rand(), true));
    }

    public function validateFields($data,$fields){
	    $error = array();
	    foreach($fields as $key=>$value){
	      if (!isset($data[$value]) || !$data[$value]) {
	              $error[]=$value." "."is required.";
	             }
	    }

	    return $error;

	}

	public function upload_image($content=null,$imgname=null,$dest=null,$extn=null){
        if($content){
        	    $dataarray=explode('base64,', $content);
        	    if(count($dataarray)==2){
        	    	$extn=explode('/', $dataarray[0]);
        	    	if(count($extn)==2){
        	    		$extn=rtrim($extn[1],';');
	        	    	if($extn){
	        	    		$img_name = $imgname."-".time();
			                $data = $dataarray[1];
			                $data = str_replace(' ', '+', $data);
			                $data = base64_decode($data);
			                file_put_contents($dest.$img_name.".".$extn, $data);

			                chmod($dest.$img_name.".".$extn, 0777);
			                return $img_name.".".$extn;
	        	    	}else{
	        	    		return false;
	        	    	}
        	    	}else{
        	    		return false;
        	    	}
        	    }else{
        	    	return false;	
        	    }
        }else{
            return false;
        }
    } 

      /*function sendEmail($email_template=null,$from=null,$to=null,$subject=null){
		$Email = new CakeEmail();
		// prx($email_template);
		$Email->template('common_template')
		    ->emailFormat('html')
		    ->to($to)
		    ->from($from)
		    ->viewVars(array('content' => $email_template))
		    ->subject($subject)
		    ->send();

     	}*/
     	
     function sendEmail($email_template=null,$from=null,$to=null,$subject=null,$sender=null){

     	$this->Email->to = $to;
		$this->Email->subject=$subject;
		$this->Email->from =$from;
	        $this->Email->sender =$sender;
                $this->Email->template = 'default';
		$this->Email->sendAs = 'both'; 
		/*$this->Email->smtpOptions = array(
		     'port'=>'587',
		     'host' => 'smtp.mandrillapp.com',
		     'username'=>'braiderbooking@wollen.com',
		     'password'=>'C1INt0SeNL1oep1_iKZliQ'
		);
 		$this->Email->delivery = 'smtp';*/
 		
 		$this->Email->send($email_template);
     }
public function sendSMS($content=null,$to=null){
	      require_once(APP . 'Vendor' . DS  . 'sms' . DS. 'sms.php');
	      $this->sendMultipleSMS($content,$to);
     }
/*Send Sms : use in bulk include sms vendor file in function*/
public function sendMultipleSMS($content=null,$to=null){
	      $expertTexting= new experttexting_sms(); // Create SMS object.
	      $expertTexting->from= admin_phone; // Sender of the SMS – PreRegistered through the Customer Area.
	      $expertTexting->to= '1'.$to; // The full International mobile number of the without + or 00
	      $expertTexting->msgtext= $content; // The SMS content.*/
	      $expertTexting->send();
     }
}
