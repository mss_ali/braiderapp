<?php
ob_start();
 class CmsPageController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  var $helpers=array("Html","Form","Session");

  
  function admin_view_cms_page()
  {
     $this->layout="admin";
    $this->set('cms', $this->CmsPage->find('all'));
  
   }  
  

  function admin_edit_cms_page($id = null)
  { 

   $this->layout="admin";
 
        if (!$id) { 
        throw new NotFoundException(__('Invalid post'));
    }

    $cms = $this->CmsPage->findById($id);
    $this->set('cms', $cms);
    if (!$cms) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->CmsPage->id = $id;
        if ($this->CmsPage->save($this->request->data)) {
             return $this->redirect(array('action' => 'admin_view_cms_page'));
        }
    }

    if (!$this->request->data) {
        $this->request->data = $cms;
    }

  }

   public function termConditions(){
      Configure::write('debug',0);
        $this->RequestHandler->respondAs('Json');
        $this->response->type('json');
        $this->autoRender= false;
        $this->loadModel('CmsPage');
        $result=$this->CmsPage->find('first',array('conditions'=>array('CmsPage.identifier'=>'term_conditions')));
        $response['status']='success';
        $response['message']=$result['CmsPage']['content'];
        $this->response->body(json_encode($response));
  }
  public function faq(){
      Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->response->type('json');
        $this->autoRender= false;
        $this->loadModel('CmsPage');
        $result=$this->CmsPage->find('first',array('conditions'=>array('CmsPage.identifier'=>'FAQ')));
        $response['status']='success';
        $response['message']=$result;
        $this->response->body(json_encode($response));
  }


}