<?php
ob_start();
 class TemplatesController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  var $helpers=array("Html","Form","Session");

 
 
  function admin_view_template()
  {
     $this->layout="admin";
    $this->set('templates', $this->Template->find('all'));
  
   }  
  

  function admin_edit_template($id = null)
  { 

     $this->layout="admin";
 
        if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $templates = $this->Template->findById($id);
    $this->set('templates', $templates);
        if (!$templates) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Template->id = $id;
        if ($this->Template->save($this->request->data)) {
             return $this->redirect(array('action' => 'admin_view_template'));
        }
    }

    if (!$this->request->data) {
        $this->request->data = $templates;
    }

}




 }

?>
