<?php
App::uses('AppModel', 'Model');
class User extends AppModel {
public $actsAs = array('Containable');
public $hasMany= array(
    'Horse' => array(
      'className' => 'Horse',
      'foreignKey' => 'competitor_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        ),
    'Reservation' => array(
      'className' => 'Reservation',
      'foreignKey' => 'competitor_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
       ),
    'Reservation' => array(
      'className' => 'Reservation',
      'foreignKey' => 'braider_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
       ),
    'BraiderWorkpicture' => array(
      'className' => 'BraiderWorkpicture',
      'foreignKey' => 'braider_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
       )

    );
    
}
