<?php
App::uses('AppModel', 'Model');
class Horse extends AppModel {
public $actsAs = array('Containable');
public $belongsTo= array(
    'User' => array(
      'className' => 'User',
      'foreignKey' => 'competitor_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        )
    );
}
