<?php
App::uses('AppModel', 'Model');
class BraiderAvialablity extends AppModel {
public $actsAs = array('Containable');
public $belongsTo= array(
    'User' => array(
      'className' => 'User',
      'foreignKey' => 'braider_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        ),
  
  'Show' => array(
      'className' => 'Show',
      'foreignKey' => 'show_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
      
    ),
  'State' => array(
      'className' => 'State',
      'foreignKey' => 'state_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        )
   
    );
    
 


    public function afterFind($results, $primary = false) {
    if(!empty($results)){
      foreach ($results as $key => $value) {
        if(isset($value['BraiderAvialablity']['dates_available']) && !empty($value['BraiderAvialablity']['dates_available'])){
          $results[$key]['BraiderAvialablity']['dates_available']=unserialize($value['BraiderAvialablity']['dates_available']);
        }else{
          continue;
        }
      }
    }
    return $results;
  }

}