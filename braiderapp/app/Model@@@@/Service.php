<?php
App::uses('AppModel', 'Model');
class Service extends AppModel {
public $actsAs = array('Containable');
var $virtualFields = array('braidername'=>"SELECT name FROM users usr WHERE usr.id = Service.braider_id"); 
public $belongsTo= array(
    'User' => array(
      'className' => 'User',
      'foreignKey' => 'braider_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        )
    );

    /*public function beforeSave($options = array()) {
       if(isset($this->data['Service']['otherServicesname']) && isset($this->data['Service']['otherServicesprice']) && !empty($this->data['Service']['otherServicesname']) && !empty($this->data['Service']['otherServicesprice'])){
          $this->data['Service']['otherServices']=serialize(array_combine($this->data['Service']['otherServicesprice'],$this->data['Service']['otherServicesname']));
       }
  }*/
}
