<?php
App::uses('AppModel', 'Model');
class Show extends AppModel {
public $actsAs = array('Containable');
//var $virtualFields = array('statename'=>"SELECT name FROM states st WHERE st.id = Show.state_id"); 

public $belongsTo= array(
    'State' => array(
      'className' => 'State',
      'foreignKey' => 'state_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        ),
    'Division' => array(
      'className' => 'Division',
      'foreignKey' => 'divisions',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        )
    );
public $hasMany= array(
    'Reservation' => array(
      'className' => 'Reservation',
      'foreignKey' => 'state_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        )
    );
}
