<?php
App::uses('AppModel', 'Model');
class Reservation extends AppModel {
public $actsAs = array('Containable');
var $virtualFields =array('showname'=>"SELECT showName FROM shows sn WHERE sn.id =
Reservation.show_id",'statename'=>"SELECT name FROM states st WHERE st.id =
Reservation.state_id",'competitor'=>"SELECT name FROM users usr WHERE usr.id =
Reservation.competitor_id",'competitoremail'=>"SELECT email FROM users usr WHERE usr.id =
Reservation.competitor_id",'competitorphone'=>"SELECT phone FROM users usr WHERE usr.id =
Reservation.competitor_id",'braider'=>"SELECT name FROM users usr WHERE usr.id
= Reservation.braider_id",'service'=>"SELECT name FROM services ser WHERE ser.id
= Reservation.braider_id"); 
 
public $belongsTo= array(
    'Horse' => array(
      'className' => 'Horse',
      'foreignKey' => 'horse_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        
    ),
   'State' => array(
      'className' => 'State',
      'foreignKey' => 'state_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
        ),
    
   
    'Show' => array(
      'className' => 'Show',
      'foreignKey' => 'show_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
     
    ),
    'User' => array(
      'className' => 'User',
      'foreignKey' => 'competitor_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
     
    ),
    'User' => array(
      'className' => 'User',
      'foreignKey' => 'braider_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
     
    ),
    'Service' => array(
      'className' => 'Service',
      'foreignKey' => 'braider_id',
      'dependent' => true,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => '',
    )
     
    
  );

public function afterFind($results, $primary = false) {
    if(!empty($results)){
      foreach ($results as $key => $value) {
        if(isset($value['Reservation']['service_ids']) && !empty($value['Reservation']['service_ids'])){
          $results[$key]['Reservation']['service_ids']=@unserialize($value['Reservation']['service_ids']);
        }else{
          continue;
        }
      }
    }
    //prx($results);
    return $results;
  }

}
