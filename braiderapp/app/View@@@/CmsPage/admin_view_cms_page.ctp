<body>
    <div id="wrapper">
        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Terms and Conditions</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        	Content
                         </div>
                             <div class="panel-body">
                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                           <th align="center">S No</th> 
                                           <th align="center">Heading</th>
                                           <th align="center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                            <?php
                            if(!empty($cms))
                                { 
                                    $i=1;
                                foreach( $cms as $cmss )
                                        {
   
           
                                    echo "<tr>"; ?>
                                        <td><?php echo $i; ?></td> <?php
                                        echo "<td>{$cmss['CmsPage']['heading']}</td>";
                                        echo "<td class='actions'>";
                                        echo $this->Html->link( 'Edit', array('action' => 'admin_edit_cms_page', $cmss['CmsPage']['id']) );
                                        echo "</td>";
                                        echo "</tr>";
                               
                                
                                $i++; 
                                         } 
                                } 
                              else 
                                { 
                                    echo "<tr>";
                                    echo "<td colspan='7'>No Record Found.</td>";
                                    echo "</tr>";
                                }
                                 ?>          
                             </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
