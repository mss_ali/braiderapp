 <?php $this->Paginator->options(array('url' => array('controller'=>'Users','action'=>'admin_customer'))); ?> 
  <div id="page-content">
    <div id="page-content-wrapper" class="no-bg-image wrapper-full">
      <div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
      <div class="inner-page-title">
        <h2>Products Details</h2>
      </div>
       <?php if($this->Session->check('success')){ ?>
				<div class="response-msg success ui-corner-all">
					<span></span>
					<?php echo $this->Session->read('success');?>
				</div>
				<?php $this->Session->delete('success'); ?>
			<?php } ?>
      <div class="content-box content-box-header" style="border:none;" id='check'>
          <div id="page-content">
              <div id="page-content-wrapper" style="padding:0; margin:0; background:0; box-shadow:0 0 0 0 #fff;">
                <div class="hastable">
                  <table id="sort-table">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Customer ID</th>
                        <th>firstName</th>
                        <th>lastName</th>
                        <th>address</th>
                        <th>product_id</th>
                        <th>description</th>
                        <th>email</th>
                        <th>quantity</th>
                        <th>created</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                      <tbody>
                      <?php
						
						if(!empty($customerList))
						{	$i=1;
							foreach($customerList as $info)
							{ 
						?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $info['Customer']['id'] ; ?></td>
                        <td><?php echo $info['Customer']['firstName'] ; ?></td>
                        <td><?php echo $info['Customer']['lastName']; ?></td>
                        <td><?php echo $info['Customer']['address']; ?></td>
                        <td><?php echo $info['Customer']['product_id']; ?></td>
                         <td><?php echo $this->Text->truncate(strip_tags($info['Customer']['description']),50,array('ending'=>'...','exact'=>false,'html'=>true));?></td>
                          <td><?php echo $info['Customer']['email']; ?></td>
                        <td><?php echo $info['Customer']['quantity']; ?></td>
                        <td><?php echo $info['Customer']['created']; ?></td>
                          <td><a title="Edit" href="<?php echo HTTP_ROOT."admin/users/editProduct/".base64_encode($info['Customer']['id']); ?>" class="btn_no_text btn ui-state-default ui-corner-all tooltip"> <span class="ui-icon ui-icon-pencil"></span> </a>
                            <a title="Delete" href="<?php echo HTTP_ROOT."admin/users/deleteProduct/".base64_encode($info['Customer']['id']); ?>" onclick="return confirm('Are you sure to delete this product')" class="btn_no_text btn ui-state-default ui-corner-all tooltip"> <span class="ui-icon ui-icon-remove"></span> </a>
                          </td>
                      </tr>
                      <?php	
								$i++;	
							}
						} else {
					?>
                      <tr>
                        <td colspan="7">No Record Found.</td>
                      </tr>
                      <?php		
						}
					?>
                    </tbody>
                  </table></br>
              <div id="pager" style="float:right">          
               <?php echo $this->element('adminElements/table_head'); ?>
               </div> 
                  <div class="clear"></div>
             
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
           
       
       </div>
      <div class="clearfix"></div>
     
      <div class="clear"></div>
    </div>
   </div> 
    <div class="clear"></div>
  </div>