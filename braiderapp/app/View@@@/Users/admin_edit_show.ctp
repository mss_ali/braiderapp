<body>

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Show</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
                
                    <div class="panel panel-default">
                        <?php echo $this->Session->flash(); ?>
                        <div class="panel-heading">
                           Add Show Table
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                             <?php echo $this->Form->create('Show',array('id'=>'addshow')); ?>
                               <!-- <form name = "Show" method = "post" action="./viewShow" id = "addshow">--> 
                <table class="table table-striped table-bordered table-hover" id="dataTables-example"> 
                    <tr><th>State_id</th>
                    <td class="center"><select name = "data[Show][state_id]" id="state_id">
                      <?php foreach ($data as $datas): ?>
                       <option value="<?php echo $datas['State']['id']; ?>">
                            <?php echo $datas['State']['name']; ?></option>
                      <?php endforeach; ?>
                             </select> 
                    </td>
                    </tr>
                    <tr><th>Show Name</th>
                    <td>
                   <?php echo $this->Form->input('showName',array('class' => 'form-control','placeholder'=> 'Show Name','label'=>false)); ?> 
                    </td></tr>
                     </td></tr>
                    <tr><th>Start Date</th>
                    <td><?php echo $this->Form->input('startDate ',array('class' => 'datepicker' ,'placeholder'=> 'Start Date','label'=>false)); ?></td></tr>
                    <tr><th>End Date</th>
                    <td> <?php echo $this->Form->input('endDate ',array('class' => 'datepicker' ,'placeholder'=> 'End Date','label'=>false)); ?></td></tr>
                    <tr><th>Divisions</th>
                    <td><?php echo $this->Form->input('divisions', array('type' => 'select','options' => array('Hunter' => 'hunter', 'Jumper' => 'jumper','Dressage'=> 'dressage'))); ?>
                    </td></tr>
                     <?php echo  $this->Form->input('startMonth', array('type' => 'hidden')); ?>
                    <?php echo  $this->Form->input('endMonth', array('type' => 'hidden')); ?>
                   
                    
                    
                    </table>
                        <?php   echo $this->Form->submit('Edit Show',array('class' => 'btn btn-lg btn-success btn-blocks'));  ?>
               <!-- <input type ="submit" name = "submit" value = "submt" class ="btn btn-lg btn-success btn-blocks"> -->
                    
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
           
           
        </div>
        <!-- /#page-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
    $('#addshow').validate({
           onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
              "data[Show][state_id]":
                {
                    required:true
                    
                },
              "data[Show][showName]":
               
                {
                    required:true
                    
                },
                "data[Show][divisions]":
                {
                    required:true
                    
                }


            },
            messages:
            {
               "data[Show][state_id]":
                {
                    required:"Please enter the state id"
                    
                },
             
                "data[Show][showName]":
                {
                     required:"Please enter the Show Name"
                  
                },
                "data[Show][divisions]":
                {
                   required:"Please enter Divisions"
                  
                }
             
            }
        
        
        });
        
    
    });
</script>
    
  <script>
  $(function() {
    $( ".datepicker" ).datepicker({ dateFormat : 'yy-mm-dd'});
  });
  </script>



    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>

