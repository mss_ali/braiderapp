        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php //echo $this->element('admin/row'); ?>
            <div class="row">
               
                <div class="panel panel-default">
                        <div class="panel-heading">
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div class="row"><div class="col-sm-12">
                                <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dataTables-example_info">
                                    <thead>
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php   echo "<th>" . $paginator->sort('Show.state_id', 'State') . "</th>";
                                    echo "<th>" . $paginator->sort('Show.city', 'City Name') . "</th>";
                                    echo "<th>" . $paginator->sort('Show.showName', 'Show Name') . "</th>";
                                    echo "<th>" . $paginator->sort('Show.startDate', 'Start Date') . "</th>";
                                    echo "<th>" . $paginator->sort('Show.endDate', 'End Date') . "</th>";
                                    echo "<th>" . $paginator->sort('Show.divisions', 'Divisions') . "</th>";
                                     echo "<th>" . $paginator->sort( 'Action') . "</th>";
                                        ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                     <tr class="odd gradeX">
                                <?php
                                foreach( $shows as $show ){
    
        echo "<tr>";
             $sdate=new DateTime($show['Show']['startDate']);
             $edate=new DateTime($show['Show']['endDate']);
            echo "<td>{$show['State']['name']}</td>";
            echo "<td>{$show['Show']['city']}</td>";
            echo "<td>{$show['Show']['showName']}</td>";
            echo "<td>{$sdate->format("j, F, Y")}</td>";
            echo "<td>{$edate->format("j, F, Y")}</td>";
            echo "<td>{$show['Show']['divisions']}</td>";
                      
            //here are the links to edit and delete actions
            echo "<td class='actions'>";
                echo $this->Html->link( 'Edit', array('action' => 'admin_editShow', $show['Show']['id']) );
                 
                //in cakephp 2.0, we won't use get request for deleting records
                //we use post request (for security purposes)
                echo $this->Form->postLink( 'Delete', array(
                        'action' => 'admin_deleteShow', 
                        $show['Show']['id']), array(
                            'confirm'=>'Are you sure you want to delete that user?' ) );
            echo "</td>";
        echo "</tr>";
    }
    ?>
                            
                                        </tr></tbody>
                                </table>
<div class="records-p">
 <?php
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                          </div>
                    
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>

                            </div></div>
                                        <!-- Pagination -->

                            </div>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>
            <!-- /.row -->
