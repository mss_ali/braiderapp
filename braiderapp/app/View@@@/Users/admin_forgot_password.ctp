<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">
    .error {
      color: red;
    }
    </style>
    <?php 
      echo $this->Html->css('admin/bootstrap/bootstrap.min.css');
      echo $this->Html->css('admin/menu/dist/metisMenu.min.css');
      echo $this->Html->css('admin/sb-admin-2.css');
        echo $this->Html->css('admin/font-awesome/css/font-awesome.min.css');
    ?>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default" id="login-screen">
                      <div class="m-user"><img alt="login-user" src="http://mastersoftwaretechnologies.com/braider/braiderapp/img/forgot-password.png"></div>
                      <p>Forget Password</p>
                       <?php echo $this->Session->flash(); ?>
                   <!--  <div class="panel-heading">

                        <h3 class="panel-title">Forget Password</h3>
                    </div> -->
                    <div class="panel-body">
                      
                             <?php echo $this->Form->create('User', array('action' => 'admin_forgotPassword')); ?>
                            <fieldset class="fielset">
                              <div class="form-group">
                                
                                         <?php echo $this->Form->input('email',array('class' => 'form-control','placeholder'=> 'email','label'=>false)); ?>
                             </div>
                              
                                </fieldset>
                               
                                    <?php echo $this->Form->submit('Reset Password',array('class' =>'btn btn-lg btn-success btn-block'));  ?>
                               
                                                   
                          
                    </div>
                </div>
            </div>
        </div>
    </div>  
  <?php  
          echo $this->Html->script('admin/jquery/jquery.min.js');
          echo $this->Html->script('admin/bootstrap/bootstrap.min.js');
          echo $this->Html->script('admin/menu/metisMenu.min.js');
          echo $this->Html->script('admin/sb-admin-2.js');
          echo $this->Html->script('jquery.validate.js');
    ?>  
    
</body>

</html>
