<body>
<?php  //prx($shows);?>
        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage shows</h1>
                </div>
              
            </div>
            
                    <div class="panel panel-default">
                      
                        <div class="panel-heading">
                           Edit Show
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                             <?php echo $this->Form->create('Show',array('id'=>'addshow')); ?>
                            
                            <form role="form">
                                 <table class="" id="dataTables-example"> 
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">State</label>
                                            <select name = "data[Show][state_id]" id="state_id" class="form-control">
                      <?php foreach ($data as $datas): ?>
                       <option value="<?php echo $datas['State']['id']; ?>">
                            <?php echo $datas['State']['name']; ?></option>
                      <?php endforeach; ?>
                             </select> 
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">City Name</label>
                                        <?php echo $this->Form->input('city',array('class'=>"form-control",'placeholder'=> 'City Name','id'=>"inputSuccess", 'label'=>false)); ?>    
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">Show Name</label>
                                        <?php echo $this->Form->input('showName',array('class'=>"form-control",'placeholder'=> 'Show Name','id'=>"inputSuccess", 'label'=>false)); ?>    
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">Start Date</label>
                                            <input type="text" name = "data[Show][startDate]" class="datepicker form-control"  id="dt1"  placeholder = 'Start Date' value="<?=  date("Y-m-d",strtotime($shows['Show']['startDate'])) ?>">
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">End Date</label>
                                            <input type="text" name = "data[Show][endDate]" class="datepicker form-control" id="dt2" placeholder = 'End Date'
                                             value =" <?php echo   date("Y-m-d",strtotime($shows['Show']['endDate'])) ?>" >
                                        </div>
                                         <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">Division</label>
                                            <select name = "data[Show][divisions]" class="form-control">
                        <?php foreach ($divisions as $division): ?>
                       <option value="<?php echo $division['Division']['name']; ?>">
                            <?php echo $division['Division']['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                                        </div>
                                       
                                   
                    <?php  echo $this->Form->submit('Apply Edits',array('class' => 'btn btn-primary'));  ?> 
                       </table> 
                    </form>
               
                    
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
           
           
        </div>
        <!-- /#page-wrapper -->
<script type="text/javascript">

    $(document).ready(function () {

        $("#dt1").datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0,
            onSelect: function (date) {
                var dt2 = $('#dt2');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                dt2.datepicker('setDate', minDate);
                startDate.setDate(startDate.getDate() + 30);
                dt2.datepicker('option', 'maxDate', startDate);
                dt2.datepicker('option', 'minDate', minDate);
                $(this).datepicker('option', 'minDate', minDate);
            }
        });
        $('#dt2').datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
   
    
    </script>

<script type="text/javascript">
$(document).ready(function(){
    $('#addshow').validate({
           onBlur: function (element) {
             $(element).valid();
            },
            rules:
            {
              "data[Show][state_id]":
                {
                    required:true
                    
                },
              "data[Show][city]":
               
                {
                    required:true,
                     maxlength: 60
                    
                },
              "data[Show][showName]":
               
                {
                    required:true,
                     maxlength: 60
                    
                },
                "data[Show][startDate]":
               
                {
                    required:true
                                         
                },
                "data[Show][endDate]":
               
                {
                    required:true
                                         
                }

            },
            messages:
            {
               "data[Show][state_id]":
                {
                    required:"Please enter the state id"
                    
                },
                "data[Show][city]":
                {
                     required:"Please enter the City Name",
                     maxlength:"Please enter less then 60 character"
                  
                },
             
                "data[Show][showName]":
                {
                     required:"Please enter the Show Name",
                     maxlength:"Please enter less then 60 character"
                  
                },
                "data[Show][startDate]":
                {
                     required:"Please enter the Start Date"
                     
                  
                },
                "data[Show][endDate]":
                {
                     required:"Please enter the End Date"
                     
                  
                }
             
            }
        
        
        });
        
    
    });
</script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>

