<body>

    <div id="wrapper">

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Division</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <div class='upper-right-opt'>
    <?php echo $this->Html->link( 'Add Division', array( 'action' => 'add_division' ) ); ?>
</div>
 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                           <th align="center">Divisions</th>
                                            <th align="center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                                <?php
                                foreach( $divisions as $division ){
   
           
        echo "<tr>";
            echo "<td>{$division['Division']['name']}</td>";
                      
            //here are the links to edit and delete actions
             echo "<td class='actions'>";
                echo $this->Html->link( 'Edit', array('action' => 'admin_edit_division', $division['Division']['id']) );
                //in cakephp 2.0, we won't use get request for deleting records
                //we use post request (for security purposes)
                echo $this->Form->postLink( 'Delete', array(
                        'action' => 'admin_deleteDivision', 
                        $division['Division']['id']), array(
                            'confirm'=>'Are you sure you want to delete that division?' ) );
            echo "</td>";
        echo "</tr>";
    }
    ?>
                            
                                        </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
