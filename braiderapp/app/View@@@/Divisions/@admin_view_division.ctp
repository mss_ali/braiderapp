<!DOCTYPE html>
<html lang="en">

  <body>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                      <?php echo $this->Session->flash(); ?>
                    <div class="panel-heading">
                        <h3 class="panel-title">Divisions</h3>
                    </div>
                    <div class="panel-body">
 
<!-- link to add new users page -->
<div class='upper-right-opt'>
    <?php echo $this->Html->link( ' New Divisions', array( 'action' => 'add_division' ) ); ?>
</div>
 
<table style='padding:5px;'>
    <!-- table heading -->
    <tr >
       
        <th>Divisions</th>
        </tr>
     
<?php
 
    foreach( $divisions as $division ){
               
        echo "<tr>";
           
            echo "<td>{$division['Division']['name']}</td>";
                      
            //here are the links to edit and delete actions
            echo "<td class='actions'>";
                echo $this->Html->link( 'Edit', array('action' => 'admin_edit_division', $division['Division']['id']) );
                 
                //in cakephp 2.0, we won't use get request for deleting records
                //we use post request (for security purposes)
                echo $this->Form->postLink( 'Delete', array(
                        'action' => 'admin_deleteDivision', 
                        $division['Division']['id']), array(
                            'confirm'=>'Are you sure you want to delete that user?' ) );
            echo "</td>";
        echo "</tr>";
    }
?>
     
</table>
       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
      <?= $this->Html->script('jquery.min.js') ?>

    <!-- Bootstrap Core JavaScript -->
      <?= $this->Html->script('bootstrap.min.js') ?>
    
    <!-- Metis Menu Plugin JavaScript -->
     <?= $this->Html->script('metisMenu.min.js') ?>

    <!-- Custom Theme JavaScript -->
     <?= $this->Html->script('sb-admin-2.js') ?>
      <?= $this->Html->script('jquery-1.10.2.js') ?>
      <?= $this->Html->script('jquery-ui.js') ?>
    
</body>

</html>
