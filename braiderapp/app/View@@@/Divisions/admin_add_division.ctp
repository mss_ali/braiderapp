<body>

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Division</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
                
                    <div class="panel panel-default">
                        <?php echo $this->Session->flash(); ?>
                        <div class="panel-heading">
                           Add Division
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <?php echo $this->Form->create('Division',array('id'=>'division')); ?>
                                 
                            <form role="form">
                                 <table class="" id="dataTables-example"> 
                                        
                                     <div class="form-group has-successs">
                                         <label class="control-label" for="inputSuccess">Division Name</label>
                                            <input type="text" name="data[Division][name]" class="form-control" id="inputSuccess" placeholder = 'Division Name'>
                                     </div>
                                       
                                 </table>           
                            <?php   echo $this->Form->submit('Add Division',array('class' => 'btn btn-primary'));  ?>
                     
                    </form>
               

             
                            </div>
                          
                           
                        </div>
                     
                    </div>
                  
              
           
           
        </div>
  <script type="text/javascript">
$(document).ready(function(){
   $('#division').validate({
      onfocusout: function (element) {
             $(element).valid();
            },
         rules:
         {
            "data[Division][name]":
            {
              
               required:true,
               maxlength: 30
              
           }
         
         },
         messages:
         {
             "data[Division][name]":
            {
               required:'Please enter Division.',
               maxlength:"Please enter less then 30 character"
                
            }
           
         }
      });
   
   });
</script>


    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>


</body>

</html>