<?php $templates ?>
            <div class="row">
                <div class="col-lg-12">
                      <?php   if($templates['Template']['type'] == 'notify') 
                          {  
                             ?> 
                    <h1 class="page-header">Manage SMS</h1>
                    <?php }
                    else
                    { ?>
                        <h1 class="page-header">Manage Email</h1>
                    <?php  } ?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           <?php echo $this->Html->script('ckeditor/ckeditor');?>
                
                 <div class="panel panel-default">
                        <?php echo $this->Session->flash(); ?>
                     <?php   if($templates['Template']['type'] == 'notify') 
                          {  
                             ?>  
                     <div class="panel-heading">
                          Manage SMS
                     </div>
                     <?php }
                     else
                     { ?>
                        <div class="panel-heading">
                          Manage Email
                     </div>
                     <?php } ?>
                        <!-- /.panel-heading -->
                    <div class="panel-body edit-templte">
                            <div class="dataTable_wrapper">
                                <?php echo $this->Form->create('Template'); ?>
                                 
                                  <table  id="dataTables-example"> 
                                        
                                     <div class="form-group has-successs">
                                         <label class="control-label" for="inputSuccess">Email Subject</label>
                                            <?php echo $this->Form->input('subject',array('class' => 'form-control','placeholder'=> 'Template Subject','label'=>false)); ?>
                                     </div>
                                      <div class="">
                    <?php if($templates['Template']['variable']){?>                  
                        <div class="form-group has-successs"><label for="inputSuccess" class="control-label">Variables</label> 
                        <input type="text" id="variables" value="<?php echo $templates['Template']['variable']?>" maxlength="255" placeholder="Template Subject" class="form-control" name="data[Template][subject]" disabled>
                        </div>  
                    <?php }?>    
                    <?php   if($templates['Template']['type'] == 'notify') 
                          {  
                             ?>              
					<label for="inputSuccess" class="control-label">Email Content</label>
					<textarea  name="data[Template][content]" style="width: 1080px; height: 310px;"><?php echo $templates['Template']['content']?></textarea>
                        <?php   }
                           else
                           { ?>
                           <label for="inputSuccess" class="control-label">Email Content</label>
                    <textarea id="TemplateContent" class="ckeditor" name="data[Template][content]" style="width: 1080px; height: 310px;"><?php echo $templates['Template']['content']?></textarea>
                       <?php } ?>
                                     </div>
                                       
                                 </table>  
                             <?php   echo $this->Form->submit('Update Template',array('class' => 'btn btn-primary'));  ?>

                            </div>
                    </div>
                 </div>
                          
      <style type="text/css">
       .cke_inner {
    background: none repeat scroll 0 0 #fff;
    display: block;
    padding: 0;
    width: 930px;
}
      </style>   
       
