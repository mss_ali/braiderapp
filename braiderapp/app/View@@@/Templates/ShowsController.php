<?php
ob_start();
 class ShowsController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  
  function beforeFilter(){
    $this->disableCache();
    parent::beforeFilter();
        if(!$this->CheckAdminSession() && $this->request->prefix=='admin' && !in_array($this->request->action,array('admin_login','admin_forgotPassword','admin_reset_password','admin_chkadminemail','admin_viewReservation'))){
        $this->redirect(array('action' => 'login','admin' => true));
        exit();
    }
  }
    
    public function index(){
        //die;
      //$shows=$this->Show->find('all');
        $aDays = array('2015-07-05', '2015-07-08');
      prx(serialize($aDays));
    }

    /*Start:Api to fetch shows list*/
   public function showsList( $m = null,$stateId = null){ 
      Configure::write('debug',0);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
        $date=$this->getMonthDate($m);
         
        if(!empty($date)){
             $shows=$this->Show->find('all',array('contain'=>false));
             if(!empty($shows)){
                $results=array();
                foreach ($shows as $key => $value) {
                    if(($value['Show']['startDate']>=$date[0] && $value['Show']['endDate']<=$date[1]) || ($value['Show']['startDate']<=$date[1] && $value['Show']['endDate']>$date[1]) || ($value['Show']['endDate']>=$date[0] && $value['Show']['startDate']<$date[0])){
                      if($value['Show']['state_id'] == $stateId){
                       $results[]= $this->getCombinedShowName($value['Show']);
                       }
                    }
                 }
                if(!empty($results)){
                    $response['status']='success';
                    $response['shows']=$results;
                    $this->response->body(json_encode($response));
                }else{
                    $response['status']='success';
                    $response['shows']=array();
                    $response['message']='Shows not found';
                    $this->response->body(json_encode($response));
                }
             }else{
                $response['status']='success';
                $response['shows']=array();
                $response['message']='Shows not found';
                $this->response->body(json_encode($response));
             } 
        }else{
            $shows=$this->Show->find('all',array('contain'=>false));
            if(!empty($shows)){
                $results=array();
                foreach ($shows as $key => $value) { 
                $results[]=$this->getCombinedShowName($value['Show']);
                } 
                $response['status']='success';
                $response['shows']=@$results;
                $this->response->body(json_encode($response));
            }else{
                $response['status']='success';
                $response['shows']=array();
                $response['message']='Shows not found';
                $this->response->body(json_encode($response));
             }
        }
    }
 
    function getCombinedShowName($data = null){
 
     $date = strtotime($data['startDate']);
     $startDate = date('Y-m-d', $date);
     $startMonth = explode('-',$startDate);
     $startday = $startMonth[2];
     $startMonth = $startMonth[1];
     $date1 = strtotime($data['endDate']);
     $endDate = date('Y-m-d', $date1);
     $endMonth = explode('-',$endDate);
     $endday = $endMonth[2];
     $endMonth = $endMonth[1];
     $monthNum  = $startMonth;
     $dateObj   = DateTime::createFromFormat('!m', $monthNum);
     $startMonthName = $dateObj->format('F');
     $monthNum  = $endMonth;
     $dateObj   = DateTime::createFromFormat('!m', $monthNum);
     $endMonthName = $dateObj->format('F');
     $completeShowName = $data['showName'].','.$data['divisions'].','.$startMonthName.''.$startday.'-'.$endMonthName.''.$endday;
          
      $data['showName'] = $completeShowName;
      return $data;


    }
 

    function getMonthDate($monthStr=null) {
        $m = trim($monthStr);
        $y=date("Y");
        $date=array();
        switch ($m) {
            case "1":
                $date[0] = "$y-01-01";
                $date[1] = date("$y-01-t");
                break;
            case "2":
                $date[0] = "$y-02-01";
                $date[1] = date("$y-02-t");
                break;
            case "3":
                $date[0] = "$y-03-01";
                $date[1] = date("$y-03-t");
                break;
            case "4":
                $date[0] = "$y-04-01";
                $date[1] = date("$y-04-t");
                break;
            case "5":
                $date[0] = "$y-05-01";
                $date[1] = date("$y-05-t");
                break;
            case "6":
                $date[0] = "$y-06-01";
                $date[1] = date("$y-06-t");
                break;
            case "7":
                $date[0] = "$y-07-01";
                $date[1] = date("$y-07-t");
                break;
            case "8":
                $date[0] = "$y-08-01";
                $date[1] = date("$y-08-t");
                break;
            case "9":
                $date[0] = "$y-09-01";
                $date[1] = date("$y-09-t");
                break;
            case "10":
                $date[0] = "$y-10-01";
                $date[1] = date("$y-10-t");
                break;
            case "11":
                $date[0] = "$y-11-01";
                $date[1] = date("$y-11-t");
                break;
            case "12":
                $date[0] = "$y-12-01";
                $date[1] = date("$y-12-t");
                break;
            default:
                break;
        }
        return $date;
        }

    function GetDays($sStartDate, $sEndDate){  
        Configure::write('debug',0);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $sCurrentDate=$sStartDate;
        $aDays=array($sStartDate);
      while($sStartDate < $sEndDate){  
        $sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
        $aDays[] = $sCurrentDate;  
        $sStartDate=$sCurrentDate;
      }
     if(!empty($aDays)){
        $response['status']='success';
        $response['dates']=$aDays;
        $this->response->body(json_encode($response));
     }else{
        $response['status']='error';
        $response['message']='dates not found';
        $this->response->body(json_encode($response));
     }   
     
    }  
     
    /*Start:Api to Add show by braider*/
    public function addShow(){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $json=$this->request->input('json_decode');
           /*
           *here we convert date from milisecond to date because 
           *request come from with date in milisecond format 

           */

          $dateArray = $json->addshowbraiderdata->dates_available;
          for($i = 0;$i<count($dateArray);$i++)
          {
           $mil = $dateArray[$i];
           $seconds = $mil / 1000;
           $data['dates_available'][] = date("Y-m-d", $seconds);
           }
           $data['state_id']=@$json->addshowbraiderdata->state_id;
           $data['show_id']=@$json->addshowbraiderdata->show_id;
           $data['braider_id']=@$json->addshowbraiderdata->braider_id;
           $data['month_id']=@$json->addshowbraiderdata->month_id;
           $data['status']=@$json->addshowbraiderdata->status;
           $data['access_token']=@$json->addshowbraiderdata->access_token;
           $fields=array('state_id','show_id','dates_available','status','access_token');
           $errors=$this->validateFields($data,$fields);
           if(empty($errors)){
                $this->loadModel('User');
               $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>trim($data['access_token']),'User.role'=>'braider')));
               if(!empty($user)){
                    $this->loadModel('BraiderAvialablity');
                    if(!empty($data['dates_available'])){
                        $data['dates_available']=serialize($data['dates_available']);
                    }
                    $data['braider_id']=$user['User']['id'];
                    if($this->BraiderAvialablity->save($data)){
                        $response['status']='success';
                        $response['message']='show has been added successfully.';
                        $this->response->body(json_encode($response));
                    }else{
                        $response['status']='error';
                        $response['message']='Opps! there is something wrong to add show.';
                        $this->response->body(json_encode($response));
                    }
               }else{
                    $response['status']='error';
                    $response['message']='User is not valid to add show.';
                    $this->response->body(json_encode($response));
               }
            }else{
                $response['status']='error';
                $response['message']=$errors;
                $this->response->body(json_encode($response));
           }  
        }else{
                $response['status']='error';
                $response['message']='Invalid post request.';
                $this->response->body(json_encode($response));
        } 
    }
    /*End*/
    
    /*Start: Api ti get braider show*/
        public function getShow($id=null){
           Configure::write('debug',0);
           $this->RequestHandler->respondAs('Json');
           $this->autoRender= false;
           $this->loadModel('BraiderAvialablity');
           $show=$this->BraiderAvialablity->find('all',array('conditions'=>array('BraiderAvialablity.braider_id'=>$id),'BraiderAvialablity.id DESC'));
           $results = array();
           $result = array();
           if(!empty($show) && count($show) > 0){
            foreach ($show as $key => $value) {
                  $result['BraiderAvialablity']=$value['BraiderAvialablity'];
                  $result['BraiderAvialablity']['statename']=$value['State']['name'];
                  $result['BraiderAvialablity']['showName']=$value['Show']['showName'];
                  $response['BraiderAvialablity'][] = $result['BraiderAvialablity'];
                  }
                  $response['status']='success';
                  $this->response->body(json_encode($response));
            }else{
                  $response['status']='success';
                  $response['message']='Show not found';
                  $response['BraiderAvialablity']=array();
                  $this->response->body(json_encode($response));
           
         }
      }
      /* public function getSingleShow($showid=null){
           Configure::write('debug',2);
           $this->RequestHandler->respondAs('Json');
           $this->autoRender= false;
           $this->loadModel('BraiderAvialablity');
           $show=$this->BraiderAvialablity->find('all',array('conditions'=>array('BraiderAvialablity.id'=>$showid)));

           $results = array();
           $result = array();
           if(!empty($show) && count($show) > 0){
            foreach ($show as $key => $value) {
                  $result['BraiderAvialablity']=$value['BraiderAvialablity'];
                  $result['BraiderAvialablity']['statename']=$value['State']['name'];
                  $result['BraiderAvialablity']['showName']=$value['Show']['showName'];
                  $response['BraiderAvialablity'][] = $result['BraiderAvialablity'];
                  }
                  $response['status']='success';
                  $this->response->body(json_encode($response));
            }else{
                  $response['status']='success';
                  $response['message']='Show not found';
                  $response['BraiderAvialablity']=array();
                  $this->response->body(json_encode($response));
           
         }
      }
   */
   
    /*End*/

    public function getSingleShow($showid=null){
           Configure::write('debug',2);
           $this->RequestHandler->respondAs('Json');
           $this->autoRender= false;
           $this->loadModel('BraiderAvialablity');
           $show=$this->BraiderAvialablity->find('all',array('conditions'=>array('BraiderAvialablity.id'=>$showid)));
           $results = array();
           $result = array();
           if(!empty($show) && count($show) > 0){
            $dates = array();
           for($i=0;$i<count($show[0]['BraiderAvialablity']['dates_available']);$i++){
            $dates[] = 1000 * strtotime($show[0]['BraiderAvialablity']['dates_available'][$i]);
            } 
            foreach ($show as $key => $value) {
                  $result['BraiderAvialablity']=$value['BraiderAvialablity'];
                  $result['BraiderAvialablity']['statename']=$value['State']['name'];
                  $result['BraiderAvialablity']['showName']=$value['Show']['showName'];
                  $response['BraiderAvialablity']['id'] = $result['BraiderAvialablity']['id'];
                  $response['BraiderAvialablity']['state_id'] = $result['BraiderAvialablity']['state_id'];
                  $response['BraiderAvialablity']['show_id'] = $result['BraiderAvialablity']['show_id'];
                  $response['BraiderAvialablity']['braider_id'] = $result['BraiderAvialablity']['braider_id'];
                  $response['BraiderAvialablity']['month_id'] = $result['BraiderAvialablity']['month_id'];
                  $response['BraiderAvialablity']['status'] = $result['BraiderAvialablity']['status'];
                  $response['BraiderAvialablity']['created'] = $result['BraiderAvialablity']['created'];
                  $response['BraiderAvialablity']['updated'] = $result['BraiderAvialablity']['updated'];
                  $response['BraiderAvialablity']['statename'] = $result['BraiderAvialablity']['statename'];
                  $response['BraiderAvialablity']['showName'] = $result['BraiderAvialablity']['showName'];
                  $response['BraiderAvialablity']['dates_available'] = $dates;

                }
                  $response['status']='success';
                  $this->response->body(json_encode($response));
            }else{
                  $response['status']='success';
                  $response['message']='Show not found';
                  $response['BraiderAvialablity']=array();
                  $this->response->body(json_encode($response));
           
         }
      }

    /*Start:Api to fetch braider shows(showSchedule)*/
      public function getshowSchedule($access_token=null){
         Configure::write('debug',2);
         $this->RequestHandler->respondAs('Json');
         $this->autoRender= false;
         $this->loadModel('BraiderAvialablity');
         $this->loadModel('User');
         $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>$access_token,'User.role'=>'braider')));
         if(!empty($user)){
            $shows=$this->BraiderAvialablity->find('all',array('conditions'=>array('BraiderAvialablity.braider_id'=>$user['User']['id'])));
            if(!empty($shows)){
                 
                 $results=array(); 
                 foreach ($shows as $key => $value) {
                      $results[$key]['id']=$value['BraiderAvialablity']['id'];
                      $results[$key]['dates_available']=$value['BraiderAvialablity']['dates_available'];
                      $results[$key]['status']=$value['BraiderAvialablity']['status'];
                      $results[$key]['status']=$value['BraiderAvialablity']['status'];
                      $results[$key]['braidername']=$value['User']['name'];
                      $results[$key]['email']=$value['User']['email'];
                      $results[$key]['phone']=$value['User']['phone'];
                      $results[$key]['showname']=$value['Show']['showName'];
                      $results[$key]['startDate']=date('Y-m-d',strtotime($value['Show']['startDate']));
                      $results[$key]['endDate']=date('Y-m-d',strtotime($value['Show']['endDate']));
                      $results[$key]['divisions']=$value['Show']['divisions'];
                      $results[$key]['State']=$value['State']['name'];
                  } 
                 $response['status']='success';
                 $response['scheduleShow']=$results;
                 $this->response->body(json_encode($response));
            }else{
                $response['status']='success';
                $response['message']='No show schedule yet.';
                $this->response->body(json_encode($response));
            }
         }else{
            $response['status']='error';
            $response['message']='User is not valid to fetch show.';
            $this->response->body(json_encode($response));
         }
      }
    /*End*/

    /*Start: Api to edit brader shows*/
    public function editBraiderShow(){
        Configure::write('debug',0);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $json=$this->request->input('json_decode');
           /*
           *here we convert date from milisecond to date because 
           *request come from with date in milisecond format 

           */

          $dateArray = $json->dates_available;
          for($i = 0;$i<count($dateArray);$i++)
          {
           $mil = $dateArray[$i];
           $seconds = $mil / 1000;
           $data['dates_available'][] = date("Y-m-d", $seconds);
           }
           $data['id']=@$json->id;
           $data['state_id']=@$json->state_id;
           $data['show_id']=@$json->show_id;
           $data['month_id']=@$json->addshowbraiderdata->month_id;
           /*$data['status']=@$json->status;*/
           $data['access_token']=@$json->access_token;
           $fields=array('id','state_id','show_id','dates_available','access_token');
           $errors=$this->validateFields($data,$fields);
           if(empty($errors)){
                $this->loadModel('User');
               $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>trim($data['access_token']),'User.role'=>'braider')));
               if(!empty($user)){
                    $this->loadModel('BraiderAvialablity');
                    $checkshow=$this->BraiderAvialablity->find('first',array('conditions'=>array('BraiderAvialablity.id'=>$data['id'],'BraiderAvialablity.braider_id'=>$user['User']['id'])));
                    if(!empty($checkshow)){
                         if(!empty($data['dates_available'])){
                            $data['dates_available']=serialize($data['dates_available']);
                        }
                        $data['braider_id']=$user['User']['id'];
                        if($this->BraiderAvialablity->save($data)){
                            $response['status']='success';
                            $response['message']='show has been updated successfully.';
                            $this->response->body(json_encode($response));
                        }else{
                            $response['status']='error';
                            $response['message']='Opps! there is something wrong to update show.';
                            $this->response->body(json_encode($response));
                        }
                    }else{
                        $response['status']='error';
                        $response['message']='Show does not exist.';
                        $this->response->body(json_encode($response));
                    }
                       
               }else{
                    $response['status']='error';
                    $response['message']='User is not valid to add show.';
                    $this->response->body(json_encode($response));
               }
            
            }else{
                $response['status']='error';
                $response['message']=$errors;
                $this->response->body(json_encode($response));
           }  
        }else{
            $response['status']='error';
            $response['message']='Invalid request';
            $this->response->body(json_encode($response));
        }   
    }
    /*End*/


  function admin_addShow()
  {
    $this->layout="admin";
    $this->loadModel('Show');
    $this->loadModel('Division');
    $data = $this->Division->find('all',array('fields'=>array('id','name')));  
    $this->set('divisions',$data);
    $this->loadModel('State');
    $data = $this->State->find('all',array('fields'=>array('id','name')));  
    $this->set('data',$data);
     if ($this->request->is('post')) {
       
    $data = $this->data;
    
    $startMonth = $data['Show']['startDate'];
    $startMonths =  date('M', strtotime($startMonth));
    $data['Show']['startMonth'] = $startMonths;
    $endMonth = $data['Show']['endDate'];
    $endMonths =  date('M', strtotime($endMonth));
    $data['Show']['endMonth'] = $endMonths;
    if($this->Show->save($data)){
     return $this->redirect(array('action' => 'admin_viewShow'));
     }      
    }
  }
   
   function admin_viewShow()
  {
    $this->layout="admin";
    $this->paginate = array("limit"=> 10,'order' => array('Show.startDate' => 'DESC'));
    $data = $this->paginate('Show');
    $this->set('shows',$data);
   }  
   

  function admin_editShow($id = null)
  {
     $this->layout="admin";

    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $shows = $this->Show->findById($id);
   $this->set('shows',$shows);
    if (!$shows) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Show->id = $id;

        if ($this->Show->save($this->request->data)) {
             return $this->redirect(array('action' => 'admin_viewShow'));
        }
    }

    if (!$this->request->data) {
        $this->request->data = $shows;
        
    }


 $this->loadModel('State');
   $data = $this->State->find('all',array('fields'=>array('id','name')));  
      $this->set('data',$data);

 $this->loadModel('Division');
 $data = $this->Division->find('all',array('fields'=>array('id','name')));
 $this->set('divisions',$data);

 
 
}



public function admin_deleteShow($id) {
   if ($this->request->is('post')) {
    if ($this->Show->delete($id)) {
     
      return $this->redirect(array('action' => 'admin_viewShow'));
    }
  }
}
       
function admin_viewReservation($id = null) 
{
      $this->layout="admin";
  $this->paginate = array('limit'=>10);
  $this->loadModel('Reservation');
  $data = $this->paginate('Reservation');
  $this->set('reservation',$data);

                 

          $this->layout='admin';
          $this->loadModel('Reservation');
          $this->loadModel('User');
          $this->loadModel('State');
          $this->loadModel('Show');
          if(!empty($id)){ 

            $this->redirect(array('controller'=>'shows','action'=>'viewReservation/id:'.$id,'admin'=>true));
             
          } 
          else{
          if($this->request->is('post'))
          {  
            $data = $this->data;
            $name = $data['filter']['value'];
            $type = $data['filter']['bytype'];
            if(isset($type)){

          $this->redirect(array('controller'=>'shows','action'=>'viewReservation/'.$type.':'.$name.'/type:'.$type,'admin'=>true));

        }
          }
      }
            
      if(isset($this->request->params['named']['State'])){
              $name = $this->request->params['named']['State'];
              $type=  $this->request->params['named']['type'];
              $this->paginate = array('limit'=>20,
                      'conditions' => array(
                      'State.name LIKE' => "%$name%")); 
            $this->set('reservation',$this->paginate('Reservation'));
           // pr($this->paginate('State'));die;
            
            $this->set('name',$name);
            $this->set('type',$type);
              }
   else if(isset($this->request->params['named']['Competitor'])){
          $name = $this->request->params['named']['Competitor'];
          $type=  $this->request->params['named']['type'];
          $this->paginate = array('limit'=>20,
                      'conditions' => array(
                      'User.role  LIKE' => "%$name%"));  
            $this->set('reservation',$this->paginate('Reservation'));
            $this->set('name',$name);
            $this->set('type',$type);
              }
    else if(isset($this->request->params['named']['Braider'])){
          $name = $this->request->params['named']['Braider'];
          $type=  $this->request->params['named']['type'];
          $this->paginate = array('limit'=>20,
                      'conditions' => array(
                      'User.name   LIKE' => "%$name%"));  
            $this->set('reservation',$this->paginate('Reservation'));
            $this->set('name',$name);
            $this->set('type',$type);
              }


        
}
 function admin_export_report()
{  
   $this->autoRender = false;
  
         $this->loadModel('User');
          $data = '';   
                          
              
              ini_set("memory_limit",-1);
                
$result =  $this->User->find('all',array('fields'=>array('name','email','role','phone','created'),'recursive'=>'-1'));
                        
           
              foreach ($result as $rslt) {
                        $data .= $rslt['User']['name'].",";
                        $data .= $rslt['User']['email'].",";
                        $data .= $rslt['User']['role'].",";
                        $data .= $rslt['User']['phone'].",";
                        $data .= $rslt['User']['created'].",";
                        $data .="\n";
                     } 
                                                     
                    header("Content-Type: application/csv");
                    header("Content-type: application/octet-stream");
                    $csv_filename = 'Reporting_Status'."_".date('M').date('dy').".csv";
                    header("Content-Disposition:attachment;filename=$csv_filename");
                    $fd = fopen ($csv_filename, "w");
                    fputs($fd,$data);
                    fclose($fd);
                    echo $data;
                    die();
                    $this->Session->setFlash('CSV file record has been downloaded please check your browser or folder..','success');
                      //$this->redirect($this->referer());

           
        }


    function admin_export_reservation()
        {  
           $this->autoRender = false;
          
                 $this->loadModel('Reservation');
                  $data = '';   
                                  
                        
                      ini_set("memory_limit",-1);
                        
        $result =  $this->Reservation->find('all'); 
                              $data .= "State Name".",";
                              $data .= "Show Name".",";
                              $data .= "Braider Name".",";
                              $data .= "Competitor Name".",";
                              $data .= "Services".",";
                              $data .= "Amount".",";
                              $data .= "Date".",";
                              $data .= "Division".","; 
                              $data .="\n";   
                      foreach ($result as $rslt) {
				                        $data .= $rslt['State']['name'].",";
				                        $data .= $rslt['Show']['showName'].",";
				                        $data .= $rslt['Reservation']['braider'].",";
						        $data .= $rslt['Reservation']['competitor'].",";
						        $data .= $rslt['Service']['name'].",";
						        $data .= $rslt['Service']['price'].",";
						        $data .= $rslt['Service']['created'].",";
						        $data .= $rslt['Show']['divisions'].","; 
						        $data .="\n";
                             } 
                                                             
                            header("Content-Type: application/csv");
                            header("Content-type: application/octet-stream");
                            $csv_filename = 'Reporting_Status'."_".date('M').date('dy').".csv";
                            header("Content-Disposition:attachment;filename=$csv_filename");
                            $fd = fopen ($csv_filename, "w");
                            fputs($fd,$data);
                            fclose($fd);
                            echo $data;
                            die();

                            $this->Session->setFlash('CSV file record has been downloaded please check your browser or folder..','success');
                              //$this->redirect($this->referer());

                   
                }

    /*Start:function to import show*/
      public function admin_importShow(){
        $this->layout='admin';
        if($this->request->is('post')){
          if (is_uploaded_file($this->request->data['Show']['file']['tmp_name'])) {
             $delimiter = ',';
             $ext = pathinfo($this->request->data['Show']['file']['name'], PATHINFO_EXTENSION);
            if($ext=='csv'){
                if (($handle = fopen($this->request->data['Show']['file']['tmp_name'], "r")) !== FALSE) { 
                    $i = 0; 
                    while (($lineArray= fgetcsv($handle, 4000, $delimiter))!== FALSE){ 
                        for ($j=0; $j<count($lineArray); $j++) { 
                                  $data2DArray[$i][$j] = $lineArray[$j];
                        } 
                        $i++; 
                    } 
                    fclose($handle); 
                }
                unset($data2DArray[0]);
                $dataArray=array_values($data2DArray);
                $i = 0;
                foreach($dataArray as $csv){
                  $this->loadModel('State');
                  $state=$this->State->find('first',array('conditions'=>array('State.code'=>@$csv[5])));
                  if(!empty($state)){
                    $data[$i]['Show']['state_id']=$state['State']['id'];
                  }else{
                     if(isset($csv[5]) && $csv[5]){
                       $statedata['State']['code']=$csv[5];
                       $statedata['State']['name']=$csv[5];
                       $this->State->create();
                       if($st=$this->State->save($statedata)){
                         $data[$i]['Show']['state_id']=$st['State']['id'];
                       }else{
                        $data[$i]['Show']['state_id']=@$csv[5];
                       }
                     }else{
                       $data[$i]['Show']['state_id']=@$csv[5];
                     }
                  }
                  if(isset($data[$i]['Show']['state_id']) && $data[$i]['Show']['state_id']){
                     $this->loadModel('City');
                     $city=$this->City->find('first',array('conditions'=>array('City.state_id'=>$data[$i]['Show']['state_id'],'City.name'=>@$csv[4])));
                     if(!empty($city)){
                       $data[$i]['Show']['city_id']=$city['City']['id'];
                     }else{
                        if(isset($csv[4]) && $csv[4]){
                       $citydata['City']['state_id']=$data[$i]['Show']['state_id'];
                       $citydata['City']['name']=$csv[4];
                       $this->City->create();
                       if($ct=$this->City->save($citydata)){
                         $data[$i]['Show']['city_id']=$ct['City']['id'];
                       }else{
                        $data[$i]['Show']['city_id']=@$csv[4];
                       }
                     }else{
                       $data[$i]['Show']['city_id']=@$csv[4];
                     }
                     }
                  }
                  $sdate=new DateTime(@$csv[0]);
                  $edate=new DateTime(@$csv[1]);
                  $data[$i]['Show']['startDate']=$sdate->format("Y-m-d");
                  $data[$i]['Show']['endDate']=$edate->format("Y-m-d");
                  $data[$i]['Show']['showName']=@$csv[2];
                  $data[$i]['Show']['divisions']=@$csv[3];
                  $i++;
                }
                $this->Show->create();
                if ($this->Show->saveAll($data)) {
                  $this->Session->write('success','Records have been imported successfully.');
                 // $this->redirect(array('controller'=>'Shows','action'=>'viewShow'));
                } else {
                 $this->Session->write('error','Opps! There is something wrong to import file.');
                }
            }else{
              $this->Session->write('error','Invalid file formate! please upload csv file.');
            } 
          }else{
             $this->Session->write('error','file is not uploaded successfully.');
          }
        }
      }
    /*End*/
 }
?>
