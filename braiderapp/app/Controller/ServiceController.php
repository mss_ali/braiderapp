<?php
ob_start();
 class ServiceController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  
  function beforeFilter(){
		$this->disableCache();
		parent::beforeFilter();
        if(!$this->CheckAdminSession() && $this->request->prefix=='admin' && !in_array($this->request->action,array('admin_login','admin_forgotPassword','admin_reset_password','admin_chkadminemail'))){
				$this->redirect(array('action' => 'login','admin' => true));
				exit();
		}
	}
    
    public function index(){
    	$service=$this->Service->find('all');
    	prx($service);
    }

    /*Start: Api to add new service*/
        public function addService(){
          Configure::write('debug',2);
          $this->RequestHandler->respondAs('Json');
          $this->autoRender= false;
          if($this->request->is('post')){
             $data=$_POST;
             $response=array();
             $json=$this->request->input('json_decode');
            // prx($json);
             $data['access_token']=@$json->access_token;
             $data['braider_id']=@$json->braider_id;
             $data['name']=@$json->name;
             $data['price']=@$json->price;
             $data['description']=@$json->description;
            //prx($data);
             $fields=array('braider_id','name','price');
             $errors=$this->validateFields($data,$fields);
             if(empty($errors)){
                $this->loadMOdel('User');
                $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.id'=>trim($data['braider_id']),'User.access_token'=>trim($data['access_token']),'User.role'=>'braider')));
               if(!empty($user)){
                  if($service=$this->Service->save($data)){
                      $response['status']='success';
                      $response['message']='Service has been added successfully.';
                      $response['reservation']=$service['Service'];
                      $this->response->body(json_encode($response));
                  }else{
                      $response['status']='error';
                      $response['message']='Opps! There is something wrong to schedule the braider.';
                      $this->response->body(json_encode($response));
                  }
               }else{
                  $response['status']='error';
                  $response['message']='User is not valid to add service.';
                  $this->response->body(json_encode($response));
               }
             }else{
                  $response['status']='error';
                  $response['message']=$errors;
                  $this->response->body(json_encode($response));
             }
          }else{
              $response['status']='error';
              $response['errors'] = 'invalid post request';
              $this->response->body(json_encode($response));
          }   
        }
    /*End*/

    /*Start: get Braider services*/
        public function getBraiderServices($id=null){
            Configure::write('debug',2);
            $this->RequestHandler->respondAs('Json');
            $this->autoRender= false;
            $services=$this->Service->find('all',array('contain'=>array('User'=>array('fields'=>array('User.name','User.email'))),'conditions'=>array('Service.braider_id'=>$id),'order'=>'Service.id DESC'));
            if(!empty($services)){
                $results=array();
                foreach ($services as $key => $value) {
                    $results['services'][$key]=$value['Service'];
                    //$results['braider']=$value['User'];
                }
                $response['status']='success';
                $response['services'] = $results['services'];
                $this->response->body(json_encode($response));
            }else{
                $response['status']='success';
                $response['message'] = 'No service is available.';
                $response['service'] = array();
                $this->response->body(json_encode($response));
            }
        }
    /*End*/
    //==== Api to fetch list of reservations by competetior/Braider updated by vipin ===========//


       public function getbraiderReservations($access_token = null,$reservationId = null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $this->loadMOdel('User');
        $this->loadMOdel('Reservation');
        $this->loadMOdel('Notification');
        if(isset($access_token) && !empty($access_token)){
          $compId=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.access_token'=>$access_token,'User.role'=>'braider'),'fields'=>array('name','id'))); 
          $competitor = @$compId['User']['id'];
        
       $competitorReservation = array();
       if(isset($competitor) && count($competitor) > 0){
        if(isset($reservationId) && !empty($reservationId)){
        $competitorReservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.id'=>$reservationId)));
        }else{
        $competitorReservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.braider_id'=>$competitor),'order'=>'Reservation.date ASC'));
         }
       }  
     // prx($competitorReservation);
       
       if(!empty($competitorReservation)){
            $results=array();
            foreach ($competitorReservation as $key => $value) { 
            $fetch_msgs = $this->Notification->find('all',array('contain'=>false,'conditions'=>array('Notification.to_user'=>$value['Reservation']['braider_id'])));
              // prx($fetch_msgs);
          foreach ($fetch_msgs as $key => $values) {
                 $eachcompititorreservations['messages'][$key] = $values['Notification']['msg'];                 }
          $services=$this->Service->find('all',array('contain'=>false,'conditions'=>array('Service.id'=>$value['Reservation']['service_ids'])));
         // prx($services);
          if(!empty($services)){
            foreach ($services as $k => $v) {
              $eachcompititorreservations['services'][$k]=$v['Service'];
            }
          }else{
            $eachcompititorreservations['services']=array();
          }

          $eachcompititorreservations['compitetor_id'] = $value['Reservation']['competitor_id'];
          $eachcompititorreservations['braider_id'] = $value['Reservation']['braider_id'];
          $eachcompititorreservations['status'] = $value['Reservation']['status'];
          $eachcompititorreservations['reservation_id'] = $value['Reservation']['id'];  
          $eachcompititorreservations['showDivision'] = $value['Reservation']['showDivision'];
          $eachcompititorreservations['res_number'] = $value['Reservation']['res_number'];
          $eachcompititorreservations['barnName'] = $value['Reservation']['barnName']; 
          $eachcompititorreservations['barnNumber'] = $value['Reservation']['barnNumber'];
          $eachcompititorreservations['stallNumber'] = $value['Reservation']['stallNumber']; 
          $eachcompititorreservations['date'] = date("Y-m-d", strtotime($value['Reservation']['date']));
          $eachcompititorreservations['horsename'] = $value['Horse']['name']; 
          $eachcompititorreservations['horsegender'] = $value['Horse']['gender'];
          $eachcompititorreservations['horsemarkings'] = $value['Horse']['markings'];
          $eachcompititorreservations['horsequirks'] = $value['Horse']['quirks'];
          $eachcompititorreservations['horsecolor'] = $value['Horse']['color'];
          $eachcompititorreservations['horseimage'] = $value['Horse']['image'];
          $eachcompititorreservations['horseheight'] = $value['Horse']['height'];
          $eachcompititorreservations['horseimage'] = $value['Horse']['image'];
          $eachcompititorreservations['showname'] = $value['Show']['showName']; 
          $eachcompititorreservations['showdivisions'] = $value['Show']['divisions']; 
          $eachcompititorreservations['statename'] = @$value['State']['name']; 
          $eachcompititorreservations['total_price'] = @$value['Reservation']['total_price'];
          $eachcompititorreservations['competitor'] = @$value['Reservation']['competitor'];

          $results[] = $eachcompititorreservations;   
                      } 
            $response['status']='success';
            $response['braidereservations'] = $results;
            $this->response->body(json_encode($response));
        }else{
            $response['status']='success';
            $response['message'] = 'No Reservation is available.';
            $response['braidereservations'] = array();
            $this->response->body(json_encode($response));
        }
        }else{
              $response['status']='error';
              $response['message'] = 'invalid access_token';
              $this->response->body(json_encode($response));
          }
    }

   

    /*Api to check reservation of braider on the base of date registration*/
    /*Api to check reservation of braider on the base of date registration*/
    public function getbraiderreservationsDate($access_token = null ,$current_date = null, $type = null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $this->loadMOdel('User');
        $this->loadMOdel('Reservation');
        $this->loadMOdel('Notification');
        if(isset($access_token) && !empty($access_token)){
          $compId=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.access_token'=>$access_token,'User.role'=>'braider'),'fields'=>array('name','id'))); 
            $competitor = @$compId['User']['id'];
           
       $competitorReservation = array();
       if(isset($competitor) && count($competitor) > 0 ){
        if(!$type){
             $competitorReservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.date'=>$current_date,'Reservation.braider_id'=>$competitor),'order'=>'Reservation.orders ASC'));
       
          }else{
              if($type=='previous'){
                $competitorReservation = $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('Reservation.braider_id'=>$competitor,'Reservation.date <'=>$current_date),'order'=>'Reservation.date DESC','fields'=>array('Reservation.date')));
              }
              if($type=='next'){
                 $competitorReservation = $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('Reservation.braider_id'=>$competitor,'Reservation.date >'=>$current_date),'order'=>'Reservation.date ASC','fields'=>array('Reservation.date')));
              }
              
              if(!empty($competitorReservation) && isset($competitorReservation[0]['Reservation']['date'])){
                 $res_date=$competitorReservation[0]['Reservation']['date'];
                 $competitorReservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.date'=>$res_date,'Reservation.braider_id'=>$competitor),'order'=>'Reservation.orders ASC'));
              }

         }


       }  
       
     if(!empty($competitorReservation)){
            $results=array();            
          foreach ($competitorReservation as $key => $value) { 
                $fetch_msgs = $this->Notification->find('all',array('contain'=>false,'conditions'=>array('Notification.reservation_id'=>$value['Reservation']['id'])));
                if($fetch_msgs){
            foreach ($fetch_msgs as $key => $values) {
                $eachcompititorreservations['messages'][$key] = $values['Notification']['msg'];
                $eachcompititorreservations['mark_as_read'] = $values['Notification']['mark_as_read']; 
                if($values['Notification']['mark_as_read']){
                  break;
                }
            }
                }else{
                  $eachcompititorreservations['mark_as_read'] = false;
                }
            $services=$this->Service->find('all',array('contain'=>false,'conditions'=>array('Service.id'=>$value['Reservation']['service_ids'])));
           // prx($services);
            $svices=array();
            if(!empty($services)){
              foreach ($services as $k => $v) {
                  array_push($svices,$v['Service']['name']);
               }
               $eachcompititorreservations['services']=implode(',',$svices);
            }else{
               $eachcompititorreservations['services']= "No Service";
             }
              $eachcompititorreservations['compitetor_id'] = $value['Reservation']['competitor_id'];
              $eachcompititorreservations['braider_id'] = $value['Reservation']['braider_id'];
              $eachcompititorreservations['status'] = $value['Reservation']['status'];
              $eachcompititorreservations['reservation_id'] = $value['Reservation']['id'];  
              $eachcompititorreservations['res_number'] = $value['Reservation']['res_number'];
              $eachcompititorreservations['orders'] = $value['Reservation']['orders'];
              $eachcompititorreservations['barnName'] = $value['Reservation']['barnName'];
              $eachcompititorreservations['barnNumber'] = $value['Reservation']['barnNumber'];
              $eachcompititorreservations['stallNumber'] = $value['Reservation']['stallNumber'];
              $eachcompititorreservations['date'] = date("Y-m-d", strtotime($value['Reservation']['date']));
              $eachcompititorreservations['horsename'] = $value['Horse']['name']; 
              $eachcompititorreservations['showname'] = $value['Show']['showName']; 
           
              $results[] = $eachcompititorreservations;
          } 
            $response['status']='success';
            $response['braidereservations'] = $results;
            $this->response->body(json_encode($response));
      
      }else{
            $response['status']='success';
            $response['statusCode']='512';
            $response['message'] = 'No Reservation is available on this date.';
            $response['braidereservations'] = array();
            $this->response->body(json_encode($response));
        }
    }else{
        $response['status']='error';
        $response['message'] = 'invalid access_token';
        $this->response->body(json_encode($response));
    }
  }
    /*End*/
    /*End*/

    public function getcompeteterReservations($access_token = null,$reservationId = null){ 
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $this->loadMOdel('User');
        $this->loadMOdel('Reservation');
        $this->loadMOdel('Notification');
        if(isset($access_token) && !empty($access_token)){
          $compId=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.access_token'=>$access_token,'User.role'=>'competitor'),'fields'=>array('name','id'))); 
          $competitor = @$compId['User']['id'];
        
       $competitorReservation = array();
       if(isset($competitor) && count($competitor) > 0){
        if(isset($reservationId) && !empty($reservationId)){
        $competitorReservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.id'=>$reservationId)));
        }else{

        $competitorReservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.competitor_id'=>$competitor),'order'=>'Reservation.date ASC'));

         }
       }  
         
       if(!empty($competitorReservation)){
            $results=array();
          foreach ($competitorReservation as $key => $value) {  //prx($competitorReservation);
          $services = array(); 
           $fetch_msg = $this->Notification->find('all',array('contain'=>false,'conditions'=>array('Notification.reservation_id'=>$value['Reservation']['id'])));  //prx($fetch_msg);
           if($fetch_msg){
          foreach ($fetch_msg as $key => $values) {
             $eachcompititorreservations['messages'][$key] = $values['Notification']['msg'];
             $eachcompititorreservations['mark_as_read']= $values['Notification']['mark_as_read'];
               
              }}
              else{
                $eachcompititorreservations['mark_as_read'] = false;
              }
             $services=$this->Service->find('all',array('contain'=>false,'conditions'=>array('Service.id'=>$value['Reservation']['service_ids'])));
           if(!empty($services)){
            foreach ($services as $k => $v) {
              $services['services'][$k]=$v['Service'];
            }
            }else{
              
              $eachcompititorreservations['services']=array();
            } 
          $eachcompititorreservations['compid'] = $value['Reservation']['competitor_id'];
          $eachcompititorreservations['services'] = @$services['services']; 
          $eachcompititorreservations['braider_id'] = $value['Reservation']['braider_id'];
          $eachcompititorreservations['reservation_id'] = $value['Reservation']['id'];
          $eachcompititorreservations['showDivision'] = $value['Reservation']['showDivision'];
 
          $eachcompititorreservations['barnName'] = $value['Reservation']['barnName'];
          $eachcompititorreservations['barnNumber'] = $value['Reservation']['barnNumber']; 
 
          $eachcompititorreservations['stallNumber'] = $value['Reservation']['stallNumber']; 
          $eachcompititorreservations['res_number'] = $value['Reservation']['res_number']; 
          $eachcompititorreservations['date'] = date("Y-m-d", strtotime($value['Reservation']['date']));
          $eachcompititorreservations['horsename'] = $value['Horse']['name']; 
          $eachcompititorreservations['horsegender'] = $value['Horse']['gender'];
          $eachcompititorreservations['horsemarkings'] = $value['Horse']['markings'];
          $eachcompititorreservations['horsequirks'] = $value['Horse']['quirks'];
          $eachcompititorreservations['horsecolor'] = $value['Horse']['color'];
          $eachcompititorreservations['status'] = $value['Reservation']['status'];
          $eachcompititorreservations['horseheight'] = $value['Horse']['height'];
          $eachcompititorreservations['horsequirks'] = $value['Horse']['quirks'];
          $eachcompititorreservations['horsemarkings'] = $value['Horse']['markings'];
          $eachcompititorreservations['horseimage'] = $value['Horse']['image'];
          $eachcompititorreservations['showname'] = $value['Show']['showName']; 
          $eachcompititorreservations['showdivisions'] = $value['Show']['divisions']; 
          $eachcompititorreservations['statename'] = @$value['State']['name']; 
          $eachcompititorreservations['total_price'] = @$value['Reservation']['total_price'];
          $results[] = $eachcompititorreservations;   
        
            }  
            $response['status']='success';
            $response['competeterreservations'] = $results;
            $this->response->body(json_encode($response));
        }else{
            $response['status']='success';
            $response['message'] = 'No Reservation is available.';
            $response['competeterreservations'] = array();
            $this->response->body(json_encode($response));
        }
        }else{
              $response['status']='error';
              $response['message'] = 'invalid access_token';
              $this->response->body(json_encode($response));
          }
            
      }

      //Api to fetch state
      function getstate(){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $this->loadMOdel('State');
        $state = $this->State->find('all',array('fields'=>array('id','name')));
        if(count($state) > 0){
            $result = array();
            $results = array();
            foreach($state as $key=>$value){
             $result['id'] = $value['State']['id'];
             $result['name'] = $value['State']['name'];  
             $results['State'][] = $result;
            }
          $response['status']='success';
          $response= $results;
          $this->response->body(json_encode($response));
       
        }else{
          $response['status']='success';
          $response['message'] = 'No State is available.';
          $response['state'] = array();
          $this->response->body(json_encode($response));
        } 
      }

//=============================Api to fetch next 3 reservaion by competetior/Braider===============


        public function getnextcompeteterReservations($compId = null){
        
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $this->loadMOdel('User');
        $this->loadMOdel('Reservation');
        if(isset($compId) && !empty($compId)){
          $competitor[] = $compId;
        }else{
        $competeterID=$this->User->find('all',array('contain'=>false,'conditions'=>array('User.role'=>'competitor'),'fields'=>array('id')));

       $competitor = array();
       foreach ($competeterID as $key => $value) {
         $competitor[] = $value['User']['id'];
       }
     } 
     $competitorReservation = array();
       if(isset($competitor) && count($competitor) > 0){
         for ($i=0; $i < count($competitor) ; $i++) { 
           $competitorReservation[] = $this->Reservation->find('all',array('conditions'=>array('Reservation.competitor_id'=>$competitor[$i],'Reservation.status'=>'Confirmed'),'order'=>'Reservation.id ASC','limit'=>3,
           ));
           
         }
       }  
       if(count($competitorReservation) > 0){ 
        $eachcompititorreservations = array();
        $allcompititorsreservations = array();
        $results = array();
        foreach ($competitorReservation as $key => $value) {
          for($j=0;$j<count($value);$j++){
          $eachcompititorreservations['compid'] = $value[$j]['Reservation']['competitor_id'];  
          $eachcompititorreservations['showDivision'] = $value[$j]['Reservation']['showDivision'];
          $eachcompititorreservations['barnName'] = $value[$j]['Reservation']['barnName']; 
          $eachcompititorreservations['stallNumber'] = $value[$j]['Reservation']['stallNumber']; 
          $eachcompititorreservations['created'] = $value[$j]['Reservation']['created'];
          $eachcompititorreservations['horsename'] = $value[$j]['Horse']['name']; 
          $eachcompititorreservations['horsegender'] = $value[$j]['Horse']['gender'];
          $eachcompititorreservations['horsecolor'] = $value[$j]['Horse']['color'];
          $eachcompititorreservations['horseheight'] = $value[$j]['Horse']['height'];
          $eachcompititorreservations['horseimage'] = $value[$j]['Horse']['image'];
          $eachcompititorreservations['showname'] = $value[$j]['Show']['showName']; 
          $eachcompititorreservations['showdivisions'] = $value[$j]['Show']['divisions']; 
          $eachcompititorreservations['statename'] = $value[$j]['Show']['statename']; 
          $results[$value[0]['Reservation']['competitor']][] = $eachcompititorreservations;    
          }  
        }
           if(count($results) > 0){ 
            $response['status']='success';
            $response['competeterreservations'] = $results; 
            $this->response->body(json_encode($response));
          }else{
            $response['status']='success';
            $response['message'] = 'No Reservation is available.';
            $response['competeterreservations'] = array(); 
            $this->response->body(json_encode($response));
          }
          
       }else{

          $response['status']='success';
          $response['message'] = 'No Reservation is available.';
          $response['competeterreservations'] = array();
          $this->response->body(json_encode($response));
     }   
    
     
          
      }

      public function getnextbraiderReservations($compId = null){
        
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $this->loadMOdel('User');
        $this->loadMOdel('Reservation');
        if(isset($compId) && !empty($compId)){
          $competitor[] = $compId;
        }else{
        $competeterID=$this->User->find('all',array('contain'=>false,'conditions'=>array('User.role'=>'braider'),'fields'=>array('id')));
       
       $competitor = array();
       foreach ($competeterID as $key => $value) {
         $competitor[] = $value['User']['id'];
       }
     } 
     $competitorReservation = array();
       if(isset($competitor) && count($competitor) > 0){
         for ($i=0; $i < count($competitor) ; $i++) { 
           $competitorReservation[] = $this->Reservation->find('all',array('conditions'=>array('Reservation.braider_id'=>$competitor[$i],'Reservation.status'=>'Confirmed'),'order'=>'Reservation.id ASC','limit'=>3,
           ));
           
         }
       } 
       if(count($competitorReservation) > 0){ 
        $eachcompititorreservations = array();
        $allcompititorsreservations = array();
        $results = array();
        foreach ($competitorReservation as $key => $value) {
          for($j=0;$j<count($value);$j++){
          $eachcompititorreservations['braiderid'] = $value[$j]['Reservation']['braider_id'];  
          $eachcompititorreservations['showDivision'] = $value[$j]['Reservation']['showDivision'];
          $eachcompititorreservations['barnName'] = $value[$j]['Reservation']['barnName']; 
          $eachcompititorreservations['stallNumber'] = $value[$j]['Reservation']['stallNumber']; 
          $eachcompititorreservations['created'] = $value[$j]['Reservation']['created'];
          $eachcompititorreservations['horsename'] = $value[$j]['Horse']['name']; 
          $eachcompititorreservations['horsegender'] = $value[$j]['Horse']['gender'];
          $eachcompititorreservations['horsecolor'] = $value[$j]['Horse']['color'];
          $eachcompititorreservations['horseheight'] = $value[$j]['Horse']['height'];
          $eachcompititorreservations['horseimage'] = $value[$j]['Horse']['image'];
          $eachcompititorreservations['showname'] = $value[$j]['Show']['showName']; 
          $eachcompititorreservations['showdivisions'] = $value[$j]['Show']['divisions']; 
          $eachcompititorreservations['statename'] = $value[$j]['Show']['statename']; 
          $results[$value[0]['Reservation']['braider']][] = $eachcompititorreservations;    
          }  
        }
           if(count($results) > 0){ 
            $response['status']='success';
            $response['braiderreservations'] = $results; 
            $this->response->body(json_encode($response));
          }else{
            $response['status']='success';
            $response['message'] = 'No Reservation is available.';
            $response['braiderreservations'] = array(); 
            $this->response->body(json_encode($response));
          }
          
       }else{

          $response['status']='success';
          $response['message'] = 'No Reservation is available.';
          $response['braiderreservations'] = array();
          $this->response->body(json_encode($response));
     }   
    
     
          
      }
     
 public function cancel_reservation_by_competetior($reservationId = null,$competitorId = null,$cancelled = null){
  Configure::write('debug',2);
  $this->RequestHandler->respondAs('Json');
  $this->autoRender= false;
  $this->loadModel('User');
  $this->loadModel('Reservation');
   
  $response = array();
  $competitorId = trim($competitorId);
  $reservationId = trim($reservationId);
  if(isset($competitorId) && isset($reservationId)){ 
       $iscompititor = $this->User->find('first',array('conditions'=>array('User.id'=>$competitorId,'User.role'=>'competitor')));
      $isreservationexist = $this->Reservation->find('first',array('conditions'=>array('Reservation.id'=>$reservationId)));
          
      if( (isset($iscompititor) && count($iscompititor) > 0) && (isset($isreservationexist) && count($isreservationexist) > 0)  ){  
         if(!empty($cancelled)){ 

            $cancelreservation = $this->Reservation->updateAll(array('Reservation.status'=>'"cancelled by Competitor"'),array('Reservation.id'=>$reservationId));


        if($cancelreservation){
           $finds = $this->Reservation->find('all',array('conditions'=>array('Reservation.id'=>$reservationId)));
          foreach ($finds as $key => $value) {
                 // $braider_name = $value['Reservation']['braider'];
                  //$competitor_name = $value['Reservation']['competitor'];
                  $horse_name = $value['Horse']['name'];

        
          /* Notification code goes here */
          $this->loadModel('Template');
         // require(APP . 'Vendor' . DS  . 'sms' . DS. 'sms.php');
           /* Msg goes to Braider Start */
          $notification_template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_compitetor','Template.type'=>'notify')));
          $fields=array('{competitor}','{show}','{date}','{name}');
          $start_date=new DateTime(@$isreservationexist['Show']['startDate']);
          $replaced=array(@$isreservationexist['Reservation']['competitor'],@$isreservationexist['Show']['showName'],$start_date->format("j, F, Y"),$horse_name);
          $content=str_replace($fields,$replaced, @$notification_template['Template']['content']);
         //$this->sendSMS($content,@$isreservationexist['User']['phone']);
         if(@$isreservationexist['User']['device_type']=='android'){
                  $deviceid=@$isreservationexist['User']['device_id'];
                  $this->sendNotification($deviceid,$content);  
                }
                if(@$isreservationexist['User']['device_type']=='iphone'){
                  $deviceid=@$isreservationexist['User']['device_id'];
                  $this->sendISONotification($deviceid,$content);  
                }
          /* Msg goes to Braider End */       
  


         /* Msg goes to competitor Start */
          
          $notification_template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_compitetor','Template.type'=>'notify')));
          $fields=array('{competitor}','{show}','{date}','{name}');
          $start_date=new DateTime(@$isreservationexist['Show']['startDate']);
          $replaced=array(@$isreservationexist['Reservation']['competitor'],@$isreservationexist['Show']['showName'],$start_date->format("j, F, Y"),$horse_name);
          $content=str_replace($fields,$replaced, @$notification_template['Template']['content']);
          // $this->sendSMS($content,@$isreservationexist['Reservation']['competitorphone']);
          if(@$isreservationexist['User']['device_type']=='android'){
                  $deviceid=@$isreservationexist['User']['device_id'];
                  $this->sendNotification($deviceid,$content);  
                }
                if(@$isreservationexist['User']['device_type']=='iphone'){
                  $deviceid=@$isreservationexist['User']['device_id'];
                  $this->sendISONotification($deviceid,$content);  
                }
        
         /* msg goes to competitior End */
         

         /* Email goes to  Braider Start*/
         
         $email_template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_compitetor','Template.type'=>'email')));

          $fields=array('{competitor}','{show}','{date}','{name}');
          $start_date=new DateTime(@$isreservationexist['Show']['startDate']);
          $replaced=array(@$isreservationexist['Reservation']['competitor'],@$isreservationexist['Show']['showName'],$start_date->format("j, F, Y"),$horse_name);
          $content=str_replace($fields,$replaced, @$email_template['Template']['content']);
          $from = @$isreservationexist['User']['email'];
          $subject = @$email_template['Template']['subject'] ;
          $this->sendEmail($content,SITE_EMAIL,$from,$subject);

         /* Email goes to  Braider End */

          /* Email goes to  Competitor Start*/
         
         $email_template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_compitetor','Template.type'=>'email')));

          $fields=array('{competitor}','{show}','{date}','{name}');
          $start_date=new DateTime(@$isreservationexist['Show']['startDate']);
          $replaced=array(@$isreservationexist['Reservation']['competitor'],@$isreservationexist['Show']['showName'],$start_date->format("j, F, Y"),$horse_name);
          $content=str_replace($fields,$replaced, @$email_template['Template']['content']);
          $from =  @$isreservationexist['Reservation']['competitoremail'];
          $subject= @$email_template['Template']['subject'];
          $this->sendEmail($content,SITE_EMAIL,$from,$subject);

         /* Email goes to  Competitor End */ 
 
           $response['status']='success';
          $response['message'] = 'Reservation has been cancelled!';
          $this->response->body(json_encode($response));
          
       }    
      }
        else{
         $response['status']='error';
         $response['message'] = 'Some problem occuer during cancilaton!';
         $this->response->body(json_encode($response));
        }

    }
    }else{
         $response['status']='error';
         $response['message'] = 'wrong id provided';
         $this->response->body(json_encode($response));   
       }

  }else{
     $response['status']='error';
     $response['message'] = 'wrong id provided';
     $this->response->body(json_encode($response));   
  }

} 
         public function cancel_reservation_by_braider($reservationId = null,$braiderId = null){
           Configure::write('debug',2);
          $this->RequestHandler->respondAs('Json');
          $this->autoRender= false;
          $this->loadMOdel('User');
          $this->loadMOdel('Reservation');
          $response = array();
          $braiderId = trim($braiderId);
          $reservationId = trim($reservationId);
          if(isset($braiderId) && !empty($braiderId)){
               $isbraider = $this->User->find('first',array('conditions'=>array('User.id'=>$braiderId,'User.role'=>'braider')));
               $isreservationexist = $this->Reservation->find('first',array('conditions'=>array('Reservation.id'=>$reservationId)));
              if( (isset($isbraider) && count($isbraider) > 0) && (isset($isreservationexist) && count($isreservationexist) > 0)  ){
                $cancelreservation = $this->Reservation->updateAll(array('Reservation.status'=>'"Declined"'),array('Reservation.id'=>$reservationId));
               if($cancelreservation){
                 /* Notification code goes here */ 
               $finds = $this->Reservation->find('all',array('conditions'=>array('Reservation.id'=>$reservationId)));
               foreach ($finds as $key => $value) {
                  $braider_name = $value['Reservation']['braider'];
                  $horse_name = $value['Horse']['name'];
              
                 $this->loadModel('Template');
                  $notification_template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_braider','Template.type'=>'notify')));

                  $fields=array('{braider}','{show}','{date}','{name}');
                  $start_date=new DateTime(@$isreservationexist['Show']['startDate']);
                  $replaced=array(@$isreservationexist['User']['name'],@$isreservationexist['Show']['showName'],$start_date->format("j, F, Y"),@$horse_name);
                  $content=str_replace($fields,$replaced, @$notification_template['Template']['content']);

                 //$this->sendSMS($content,@$isreservationexist['User']['phone']);
                 if(@$isreservationexist['User']['device_type']=='android'){
                  $deviceid=@$isreservationexist['User']['device_id'];
                  $this->sendNotification($deviceid,$content);  
                }
                if(@$isreservationexist['User']['device_type']=='iphone'){
                  $deviceid=@$isreservationexist['User']['device_id'];
                  $this->sendISONotification($deviceid,$content);  
                }

                 /*Send email*/
                  $email_template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_braider','Template.type'=>'email')));

                  $fields=array('{braider}','{show}','{date}','{name}');
                  $start_date=new DateTime(@$isreservationexist['Show']['startDate']);
                  $replaced=array(@$isreservationexist['User']['name'],@$isreservationexist['Show']['showName'],$start_date->format("j, F, Y"),@$horse_name);
                  $content=str_replace($fields,$replaced, @$email_template['Template']['content']);
                  $from = @$isreservationexist['User']['email'];
                  $subject = @$email_template['Template']['subject'];
                  $this->sendEmail($content,SITE_EMAIL,$from,$subject);
                 $response['status']='success';
                 $response['message'] = 'Reservation has been cancelled!';
                 $this->response->body(json_encode($response));
               }    }else{
                 $response['status']='error';
                 $response['message'] = 'Some problem occuer during cancilaton!';
                 $this->response->body(json_encode($response));
                }

               }else{
                 $response['status']='error';
                 $response['message'] = 'wrong id provided';
                 $this->response->body(json_encode($response));   
               }

          }else{
             $response['status']='error';
             $response['message'] = 'wrong id provided';
             $this->response->body(json_encode($response));   
          }
   }
// ========Api to fetch all the services by braider===============
       
  public function all_services_by_braider($id = null){
            Configure::write('debug',2);
            $this->RequestHandler->respondAs('Json');
            $this->autoRender= false;
            $services=$this->Service->find('all',array('contain'=>array('User'=>array('fields'=>array('User.name','User.email'))),'conditions'=>array('Service.braider_id'=>$id)));

            if(!empty($services)){
                $results=array();
                foreach ($services as $key => $value) {
                    $results['services'][$key]=$value['Service'];
                    //$results['braider']=$value['User'];
                }
                $response['status']='success';
                $response['services'] = $results['services'];
                $this->response->body(json_encode($response));
            }else{
                $response['status']='success';
                $response['message'] = 'No service is available.';
                $response['service'] = array();
                $this->response->body(json_encode($response));
            }
   }

//======================Api to fetch list of "My show schedule" by braider===============
   public function braiderShow_schdule($braiderId = null){
   
            Configure::write('debug',2);
            $this->RequestHandler->respondAs('Json');
            $this->loadModel('BraiderAvialablity');
            $this->autoRender= false;
            $braiderId = trim($braiderId);
            if(isset($braiderId) && !empty($braiderId)){
            $conditions = array('BraiderAvialablity.braider_id'=>$braiderId,'BraiderAvialablity.status'=>1);
            $braiderschudle = $this->BraiderAvialablity->find('all',array('conditions'=>$conditions));
           /* pr($braiderschudle);die;*/
            if(isset($braiderschudle) && count($braiderschudle) > 0){
             $results = array();
             $result = array(); 
             foreach ($braiderschudle as $key => $value) {
               foreach ($value['BraiderAvialablity']['dates_available'] as $k => $v) {
                 $results['braiderschudle']['dateavailabe'][] = $v;  
               }

               $results['braiderschudle']['name']= $value['User']['name'];
               $results['braiderschudle']['showName'] = $value['Show']['showName'];
               $results['braiderschudle']['divisions'] = $value['Show']['divisions'];
               $results['braiderschudle']['statename'] = $value['Show']['statename'];
               $result['braiderschudle'][] = $results['braiderschudle'];
             }
                $response['braidershow_schdule'] = $result['braiderschudle'];
                $response['status']='success';
                $this->response->body(json_encode($response));

            }else{
                $response['status']='success';
                $response['message'] = 'No any schudle found for this braider!';
                $response['braidershow_schdule'] = array();
                $this->response->body(json_encode($response));
            }
          }else{
                $response['status']='success';
                $response['message'] = 'Wrong id provided!';
                $response['braidershow_schdule'] = array();
                $this->response->body(json_encode($response));
          }
            

   }
//==============Api accept or reject the reservation by braider=================
  public function accept_reject_reservation_braider($reservationId = null,$braiderId = null,$value = null){
          
          Configure::write('debug',2);
          $this->RequestHandler->respondAs('Json');
          $this->autoRender= false;
          $this->loadMOdel('User');
          $this->loadMOdel('Reservation');
          $response = array();
          $braiderId = trim($braiderId);
          $reservationId = trim($reservationId);
          $update = trim($value);
          if($value ==='0'||$value === '1'){
            $status = array('0'=>'cancelled by Braider','1'=>'Confirmed');
            $update = $status[$value];
          if(isset($braiderId) && !empty($braiderId)){
               $isbraider = $this->User->find('first',array('conditions'=>array('User.id'=>$braiderId,'User.role'=>'braider')));

               $isreservationexist = $this->Reservation->find('first',array('conditions'=>array('Reservation.id'=>$reservationId)));
                 // prx($isreservationexist);
                  $role = $isreservationexist['Reservation']['competitor'];
                  $name = $isreservationexist['Horse']['name'];
              if( (isset($isbraider) && count($isbraider) > 0) && (isset($isreservationexist) && count($isreservationexist) > 0)){ 
                $cancelreservation = $this->Reservation->updateAll(array('Reservation.status'=>"'$update'"),array('Reservation.id'=>$reservationId));
                if($cancelreservation){
                 /* Notification code goes here */
                 
                   
                   $this->notificationReservation($isreservationexist,$update,$role,$name);
                
                 /*Notification*/
                 $response['status']='success';
                 $response['message'] = 'Reservation has been updated!';
                 $this->response->body(json_encode($response));
                }else{
                 $response['status']='error';
                 $response['message'] = 'Some problem occurr during updataion!';
                 $this->response->body(json_encode($response));
                }

               }else{
                 $response['status']='error';
                 $response['message'] = 'wrong id provided';
                 $this->response->body(json_encode($response));   
               }

          

        }
        else{
                 $response['status']='error';
                 $response['message'] = 'Accept-Reject value is missing !';
                 $this->response->body(json_encode($response)); 

        }
      }
        else{
             $response['status']='error';
             $response['message'] = 'wrong id provided';
             $this->response->body(json_encode($response));   
          }
   }
 
public function notificationReservation($data = null,$update,$role,$name){
  $this->loadModel('Template');
   if($update == 'Confirmed'){
     $template = $this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_request_accepted','Template.type'=>'email')));
    
   
    $fields=array('{braider}','{show}','{date}','{role}','{name}');
    $start_date=new DateTime(@$data['Show']['startDate']);
    $replaced=array($data['User']['name'],@$data['Show']['showName'],$start_date->format("j, F, Y"),$role,$name);
    $content=str_replace($fields,$replaced, @$template['Template']['content']);
    $from = $data['Reservation']['competitoremail'];
    $subject= @$template['Template']['subject'];
    $this->sendEmail($content,SITE_EMAIL,$from,$subject);

 }else if($update == 'cancelled by Braider'){
      $template = $this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_cancelled_braider','type'=>'email')));
    $fields=array('{braider}','{show}','{date}','{braider}','{name}');
    $start_date=new DateTime(@$data['Show']['startDate']);
    $replaced=array($data['User']['name'],@$data['Show']['showName'],$start_date->format("j, F, Y"),$role,$name);
    $content=str_replace($fields,$replaced, @$template['Template']['content']);
    $from = $data['Reservation']['competitoremail'];
    $subject= @$template['Template']['subject'];
    $this->sendEmail($content,SITE_EMAIL,$from,$subject);
   }else{
      return true;
   }
 return true;
 }
/*Start: APi to update service by braider*/
  public function updateService(){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $json=$this->request->input('json_decode');
           $data['id']=@$json->tabldedata->id;
           $data['name']=@$json->tabldedata->name;
           $data['price']=@$json->tabldedata->price;
           $data['description']=@$json->tabldedata->description;
           $data['access_token']=@$json->access_token;
           $fields=array('id','name','price','access_token');
           $errors=$this->validateFields($data,$fields);
           if(empty($errors)){
             $this->loadModel('User');
             $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>trim($data['access_token']),'User.role'=>'braider')));
             if(!empty($user)){
                $service=$this->Service->find('first',array('conditions'=>array('Service.id'=>$data['id'],'Service.braider_id'=>$user['User']['id'])));
                if(!empty($service)){
                    if($this->Service->save($data)){
                        $response['status']='success';
                        $response['message'] = 'Service has been updated successfully.';
                        $this->response->body(json_encode($response));
                    }else{
                        $response['status']='error';
                        $response['message'] = 'Opps{ there is something wrong to update service.';
                        $this->response->body(json_encode($response));
                    }
                }else{
                  $response['status']='error';
                  $response['message'] = 'Service does not exist.';
                  $this->response->body(json_encode($response));
                }
             }else{
              $response['status']='error';
              $response['message'] = 'Invalid user to update service.';
              $this->response->body(json_encode($response));
             }
           }else{
            $response['status']='error';
            $response['message'] = $errors;
            $this->response->body(json_encode($response));
           }
        }else{
          $response['status']='error';
          $response['message'] = 'Invalid request.';
          $this->response->body(json_encode($response));
        }   
  }
/*End*/
 /* public function braiderDetail($braider_id= null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $response=array();
        $this->loadModel('User');
        $this->loadModel('Reservation');
        $this->loadModel('BraiderAvialablity');
        $braiderReservation = array();
        $resultmain = array();
        if(isset($braider_id) && (!empty($braider_id))){
      
       $braiderReservation= $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('braider_id'=>$braider_id))); 
       if(!empty($braiderReservation)){
          foreach ($braiderReservation as $key => $value) {  
            $date = $value['Reservation']['date'];  
            $b_id = $value['Reservation']['braider_id'];  
            $res_id = $value['Reservation']['id'];

            $msg = $this->_isMsgAvailable($date,$res_id);
            $avialablity = $this->_isAvailablity($date,$b_id);
            $reservation = $this->_isReservationAvailable($date,$b_id);
            $temparray = array();
            $temparray['msg'] = $msg;
            $temparray['avialablity'] = $avialablity;
            $temparray['reservation'] = $reservation;
            $dates = date('Y-m-d',strtotime($date));
            $temparray['date'] = $dates;
            $resultmain[] = $temparray;
          }
             $resultmain = array_unique($resultmain,SORT_REGULAR);
             $response['status']='success';
             $this->response->body(json_encode($resultmain));
           }else{

               $resultmains = $this->BraiderAvialablity->find('all',array('conditions'=>array('BraiderAvialablity.braider_id'=>$braider_id)));
              // prx($resultmains);
               $returnArray = array();
               foreach ($resultmains as $key => $value) {
                        $val = $value['BraiderAvialablity']['dates_available'];  
                        $key = array_search($val,$value['BraiderAvialablity']['dates_available']);
                       // pr($key);
                        if (false !== $key) {
                        unset($value['BraiderAvialablity']['dates_available'][$key]);
                       }
               if(is_array($val)){   
                  foreach($val as $k=>$v){
                  if (in_array($v, $returnArray)) {
                  continue;
                  }else{
                  array_push($returnArray,$v);
                  }
                }  
              }
              $temparray['avialablity'] =  $returnArray;
              $resultmain[] = $temparray;
              $response['status']='success';
              $this->response->body(json_encode($resultmain));

               }
             }
           }
            else{
              $response['status']='error';
              $response['message'] = 'braider id cannot empty';
              $this->response->body(json_encode($response));
          }
        } */
        
        public function braiderDetail($braider_id= null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $response=array();
        $this->loadModel('User');
        $this->loadModel('Reservation');
        $this->loadModel('BraiderAvialablity');
        $braiderReservation = array();
        $resultmain = array();
        if(isset($braider_id) && (!empty($braider_id))){
        /*$braiderReservation = $this->Reservation->query("SELECT DISTINCT braider_id,date FROM reservations WHERE braider_id = ".$braider_id.""); //prx($braiderReservation);*/
         $braiderReservation= $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('braider_id'=>$braider_id))); //prx($braiderReservation);
          // prx($braiderReservation);
          if(!empty($braiderReservation)){
            foreach ($braiderReservation as $key => $value) {
                      $date = $value['Reservation']['date'];  
                      $b_id = $value['Reservation']['braider_id'];  
                      $res_id = $value['Reservation']['id'];
                      $msg = $this->_isMsgAvailable($date,$res_id);
                      $avialablity = $this->_isAvailablity($date,$b_id);
                      $reservation = $this->_isReservationAvailable($date,$b_id);
                      $temparray = array();
                      $temparray['msg'] = $msg;
                      $temparray['avialablity'] = $avialablity;
                      $temparray['reservation'] = $reservation;
                      $dates = date('Y-m-d',strtotime($date));
                      $temparray['date'] = $dates;
                      $resultmain[] = $temparray;
                  }  
                  $resultmain=array_values($resultmain);
                  $resultmain[] = sort($resultmain);
                 // $resultmain = array_unique($resultmain,SORT_STRING);
                  $response['status']='success';
                  $this->response->body(json_encode($resultmain));
          }else{
                    $date = '';  
                    $b_id = $braider_id; 
                    $msg = 0;
                    $avialablity = $this->_isAvailablity($date,$b_id);
                    $reservation = 0;
                    $temparray = array();
                    $temparray['msg'] = $msg;
                    $temparray['avialablity'] = $avialablity;
                    $temparray['reservation'] = $reservation;
                    //$dates = date('Y-m-d',strtotime($date));
                    $temparray['date'] = '';
                    $resultmain[] = $temparray;
                    $resultmain = array_unique($resultmain,SORT_REGULAR);
                    $response['status']='success';
                    $this->response->body(json_encode($resultmain));
          }    
        } else{
              $response['status']='error';
              $response['message'] = 'braider id cannot empty';
              $this->response->body(json_encode($response));
          }


  }

          function _isMsgAvailable($date,$res_id){  
           $this->loadModel('Notification');
           $this->loadModel('Reservation');
           $msg = $this->Notification->find('first',array('conditions'=>array('Notification.status'=>1,'Notification.reservation_id'=>$res_id,'Notification.mark_as_read'=>1)));
          // prx($msg);
           if(isset($msg['Notification']['status']) && $msg['Notification']['status'] == 1 && $msg['Notification']['mark_as_read']== 1 ){
            return 1;
          } else{
            return 0;
          }
       } 

     /*  function _isAvailablity($date,$b_id){
          $this->loadModel('BraiderAvialablity');
          $msg = $this->BraiderAvialablity->find('all',array('fields'=>array('dates_available','braider_id','status'),'conditions'=>array('BraiderAvialablity.braider_id'=>$b_id,'BraiderAvialablity.status'=>1))); ///pr($msg);
           $dates = date("Y-m-d",strtotime($date));
           $returnArray = array();
           foreach ($msg as $key => $value) {
           $key = array_search($dates,$value['BraiderAvialablity']['dates_available']);
           if (false !== $key) {
            unset($value['BraiderAvialablity']['dates_available'][$key]);
           }
                 $new_date = $value['BraiderAvialablity']['dates_available'];
                  if(is_array($new_date)){
                          foreach($new_date as $k=>$v){
                                  if (in_array($v, $returnArray)) {
                                        continue;
                                    }else{
                                    array_push($returnArray,$v);
                                    }
                          } 
                  }
          } 
         return $returnArray;
  }*/

   function _isAvailablity($date,$b_id){
          $this->loadModel('BraiderAvialablity');
          $msg = $this->BraiderAvialablity->find('all',array('fields'=>array('dates_available','braider_id','status'),'conditions'=>array('BraiderAvialablity.braider_id'=>$b_id,'BraiderAvialablity.status'=>1))); 
         // prx($msg);///pr($msg);
           $dates = date("Y-m-d",strtotime($date));
           $returnArray = array();
           foreach ($msg as $key => $value) {
            if($dates){
               $key = array_search($dates,$value['BraiderAvialablity']['dates_available']);
               if (false !== $key) {
                unset($value['BraiderAvialablity']['dates_available'][$key]);
               }
            }
            $new_date = $value['BraiderAvialablity']['dates_available'];
            if(is_array($new_date)){
                      foreach($new_date as $k=>$v){
                              if (in_array($v, $returnArray)) {
                                    continue;
                                }else{
                                array_push($returnArray,$v);
                                }
                      } 
              }
          } 
         return $returnArray;
  }
    function _isReservationAvailable($date,$b_id){
           $this->loadModel('Reservation');
           $msgs = $this->Reservation->find('first',array('fields'=>array('date','braider_id','res_status'),'conditions'=>array('Reservation.res_status'=>1,'Reservation.braider_id'=>$b_id,'Reservation.date'=>$date)));
            if(isset($msgs['Reservation']['res_status']) && $msgs['Reservation']['res_status'] == 1){
            return 1;
          } else{
            return 0;
          }
       
      }
    public function competitorMessage(){ 
         Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      if($this->request->is('post')){
         $data=$_POST;
         $response=array();
         $result = array();
         $json=$this->request->input('json_decode');
         $data['reservation_id']=$json->reservation_id;
         $fields=array('reservation_id');
         $errors=$this->validateFields($data,$fields);
         if(isset($errors)){
            $this->loadModel('Notification');
            $this->loadModel('Reservation');
            $this->loadModel('User');
            $notification = $this->Notification->find('all',array('conditions'=>array('Notification.reservation_id'=>$data['reservation_id'])));
            foreach ($notification as $key => $value) {   
                   $to_user = $value['Notification']['to_user'];
                   $from_user = $value['Notification']['from_user'];
                   $msg = $value['Notification']['msg'];
                   $msg_id = $value['Notification']['id'];
                   $mark_as_read = $value['Notification']['mark_as_read']; 

            $to_users = $this->User->find('all',array('conditions'=>array('User.id'=>$to_user))); 
            foreach ($to_users as $key => $value) {
                 $to_id = $value['User']['id'];
                 $to_name = $value['User']['name']; 
            }
            $from_users = $this->User->find('all',array('conditions'=>array('User.id'=>$from_user)));
                 foreach ($from_users as $key => $value) {
                 $to_ids = $value['User']['id'];
                 $to_names = $value['User']['name'];  
            }   
      
            $reservation = $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('Reservation.id'=>$data['reservation_id'])));
            
            foreach ($reservation as $key => $value) {
                 $date =  date("Y-m-d", strtotime($value['Reservation']['date']));
            }      
           //   echo "<pre>";echo $msg;  echo $date;   echo $to_name;echo  $to_names;
                $response['to_user'] =$to_name;    
                $response['from_user']= $to_names;
                $response['msg']= $msg; 
                $response['msgid']= $msg_id;
                $response['mark_as_read']= $mark_as_read;
                $response['date']= $date;
                $result[]= $response;
                $response['status']='success';
                $this->response->body(json_encode($result));
         }        
      }         
    } 
  } 
 /*Api to updated reorder clender reservation listing*/
   public function reorder_reservationList(){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $json=$this->request->input('json_decode');
      $this->loadModel('Reservation');
      foreach ($json as $key => $value){
        $key=$key+1;
        $data['Reservation']['id']=$value;
        $data['Reservation']['orders']=$key;
        $this->Reservation->save($data);
      }
      $result['statusCode']=111;
      $result['msg']='Order has been updated successfully.';
      $this->response->body(json_encode($result));
    }


 public function fetch_reservationList($competitor_id=null){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $this->loadModel('Reservation');
      $this->loadModel('Notification');
      $this->loadModel('Service');
      $reservationlist = $this->Reservation->find('all',array('conditions'=>array      ('Reservation.status'=>'Completed','Reservation.competitor_id'=>$competitor_id)));  
      if(!empty($reservationlist)){ 

        foreach ($reservationlist as $key => $value) {  
        $notification = $this->Notification->find('all',array('conditions'=>array('reservation_id'=>$value['Reservation']['id']))); 
        foreach ($notification as $key => $values) { 
            $res['mark_as_read']= $values['Notification']['mark_as_read'];
        }
        $services=$this->Service->find('all',array('contain'=>false,'conditions'=>array('Service.id'=>$value['Reservation']['service_ids'])));
          if(!empty($services)){
            foreach ($services as $k => $v) {
              $res['service']=$v['Service']['name'];
            }
          }
        $date = date('Y-m-d',strtotime($value['Reservation']['date']));
        $res['id']= $value['Reservation']['id'] ; 
        $res['state_id']= $value['Reservation']['state_id'] ;
        $res['show_id']= $value['Reservation']['show_id'] ;
        $res['braider_id']= $value['Reservation']['braider_id'] ;
        $res['date']= $date ;
        $res['competitor_id']= $value['Reservation']['competitor_id'] ;
        $res['horse_id']= $value['Reservation']['horse_id'] ;
        $res['service_ids']= $value['Reservation']['service_ids'] ;
        $res['total_price']= $value['Reservation']['total_price'] ;
        $res['showDivision']= $value['Reservation']['showDivision'] ;
        $res['barnName']= $value['Reservation']['barnName'] ;
        $res['barnNumber']= $value['Reservation']['barnNumber'] ;
        $res['stallNumber']= $value['Reservation']['stallNumber'] ;
        $res['res_number']= $value['Reservation']['res_number'] ;
        $res['status']= $value['Reservation']['status'] ;
        $res['statename']= $value['Reservation']['statename'] ;
        $res['competitor']= $value['Reservation']['competitor'] ;
        $res['braider']= $value['Reservation']['braider'] ;
        $res['status']= $value['Reservation']['status'] ;
        $res['horse'] = $value['Horse']['name'];
        $result[] = $res;
       }
        $response['status'] = "success";
        $response['reservationlist'] = $result;
        $response['msg'] =  "Fetch reservation on the base of status successfully.";
        $this->response->body(json_encode($response));
       
      }else{
        $response['status'] = "error";
        $response['msg'] =  "No reservation has  status completed.";
        $this->response->body(json_encode($response));
      }
   }
  public function reservation_Bydate(){
      $this->loadModel('Reservation');
      $this->autoRender = false;
      $current_date = date("Y-m-d");
      $actual_date = strtotime ( '-1 day' , strtotime($current_date)); 
      $actual_date = date ( 'Y-m-d' , $actual_date );    
   
      $allreservation = $this->Reservation->find('all',array('conditions'=>array('Reservation.date ='=>$actual_date,'Reservation.status'=>'Confirmed')));  
      if(!empty($allreservation)){ 
      foreach ($allreservation as $key => $value) { 
              $res_id = $value['Reservation']['id']; 
      $update  = $this->Reservation->updateAll(array('Reservation.status'=>'"Completed"'),array('Reservation.id'=>$res_id));  
      }die('Status Updated Successfully');
      }else{ die('No Reservation Found');}
    }
    public function tests(){                    
        echo $this->sendISONotification('3f082a0453d2d8ab8c368787a0465b59fc96745451127a4364553e4c9d345758','Notification testing');
        die;
      }
 }
?>
