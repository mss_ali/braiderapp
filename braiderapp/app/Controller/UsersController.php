<?php
ob_start();
 class UsersController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  
   function beforeFilter(){
    $this->disableCache();
    parent::beforeFilter();
        if(!$this->CheckAdminSession() && $this->request->prefix=='admin' && !in_array($this->request->action,array('admin_login','admin_forgotPassword','admin_reset_password','admin_chkadminemail'))){
        $this->redirect(array('action' => 'login','admin' => true));
        exit();
    }
  }
 public function index(){
  /*require(APP . 'Vendor' . DS  . 'sms' . DS. 'sms.php');
  $expertTexting= new experttexting_sms(); // Create SMS object.
  $expertTexting->from= '19132697254'; // Sender of the SMS – PreRegistered through the Customer Area.
  $expertTexting->to= '919988570425'; // The full International mobile number of the without + or 00
  $expertTexting->msgtext= 'Demo Test Message'; // The SMS content.*/
  //echo $expertTexting->send();
  //$this->sendEmail('test msg','mohdali@mastersoftwaresolutions.com','mohdali@mastersoftwaresolutions.com','test email');
      //die('email has been sent.');
 
 }

 
 function admin_login(){
    $this->layout="";
    if($this->CheckAdminSession()){
      $this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true));
    }
    if (!empty($this->data)){  
      //prx($this->data);
      if($this->data['User']['email']!="" && $this->data['User']['password']!=""){      
        $email = $this->data['User']['email'];
        $password = md5($this->data['User']['password']);
        $getAdminData = $this->User->find('first',array('conditions' => array('email'=>$email,'password'=>$password)));
        if(($getAdminData && $getAdminData['User']['role'] == 'admin') && ( $getAdminData['User']['email'] == $email)){
          $data['User']['id'] = $getAdminData['User']['id'];
          $data['User']['updated'] = date('Y-m-d H:i:s');
          $this->User->save($data);
          $this->Session->write('User.email', $getAdminData['User']['email']);
          $this->Session->write('User.id', $getAdminData['User']['id']);          
          $this->Session->write('User.usertype', $getAdminData['User']['role']);
          $this->Session->write('User.username', $getAdminData['User']['name']);
          $this->redirect(array('controller'=>'users', 'action' =>'dashboard','admin' => true));
        }
        $this->Session->write('error','Username or Password is invalid.');
      }
      $this->Session->write('error','Username or Password is invalid.');    
    }
  }

  function admin_logout(){
    $this->Session->delete('User');
    $this->redirect(array('action' => 'login','admin' => true));
  }
  
  /**************** Dashboard ***************/
  function admin_dashboard(){  
     $this->layout="admin";
   $this->loadModel('Show');
     $this->paginate = array("limit"=> 10,'order' => array('Show.startDate' => 'DESC'));
          $this->set('shows',$this->paginate('Show'));
  }

 
         
  function admin_chkadminemail(){
  }

   /*Start:Login API*/
   public function login(){
      Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->response->type('json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $json=$this->request->input('json_decode');
           $data['email']=@$json->email;
           $data['password']=@$json->password;
           $data['device_id']=@$json->device_id;
           $data['device_type']=@$json->device_type;
           $response=array();
           $fields=array('email','password');
           $errors=$this->validateFields($data,$fields);
           if(empty($errors)){
            $email = trim($data['email']);
            $password = trim(md5($data['password']));
            $user = $this->User->find('first',array('conditions' => array('email'=>$email,'password'=>$password)));
          if(empty($data['device_id'] && $data['device_type'])){
            foreach ($user as $key => $value)  {
              $data['id'] = @$value['id'];  
              $data['password'] = @trim(($password));
              $this->User->updateAll(array('User.device_id'=>$data['device_id'],'User.device_type'=>$data['device_type']),array('User.id'=>$data['id']));
            }
          }
            if(!empty($user)){
              if($user['User']['status']){
                if(isset($user['User']['last_login']) && $user['User']['last_login']){
                  $start_date = new DateTime(@$user['User']['last_login']);
                  $since_start = $start_date->diff(new DateTime(date('Y-m-d H:i:s')));
                  $days=$since_start->d;
                }else{
                  $days=0;
                }
               
                if($days>7 || empty($user['User']['last_login'])){
                  $apikey=$this->UniqueKey();
                  $user['User']['access_token']=$apikey;
                }
                $user['User']['last_login']=date('Y-m-d H:I:S');
                $this->User->save($user);
                $user = $this->User->find('first',array('conditions' => array('User.id'=>@$user['User']['id'])));
                $response['status']='success';
                $response['User']=$user['User'];
                $this->response->header('Access-Control-Allow-Origin', '*');
                $this->response->body(json_encode($response));
              }else{
                $response['status']='error';
                $response['message']='suspended';
                $this->response->header('Access-Control-Allow-Origin', '*');
                $this->response->body(json_encode($response));
              }
            }else{
               $response['status']='error';
               $response['message']='Invalid username or password';
               $this->response->header('Access-Control-Allow-Origin', '*');
               $this->response->body(json_encode($response));
            }
           }else{
              $response['status']='error';
              $response['message']=$errors;
              $this->response->header('Access-Control-Allow-Origin', '*');
              $this->response->body(json_encode($response));
          }
        }else{
          $this->response->statusCode(403);
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->header('Access-Control-Allow-Origin', '*');
          $this->response->body(json_encode($response));
        }
    }
   /*End*/

   /*start:Function to create account*/
      public function register(){
      Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $data=$_POST;
           $response=array();
           $json=$this->request->input('json_decode');
           $data['password']=@$json->password;
           $data['name']=@$json->name;
           $data['email']=@$json->email;
           $data['phone']=@$json->phone;
           $data['role']=@$json->role;
           $data['agree']=@$json->agree;
           $data['cpassword']=@$json->cpassword;
           $data['device_id'] =@$json->device_id;
           $data['device_type']=@$json->device_type;
          // prx($data);
           $fields=array('name','password','email','phone','role','agree','cpassword');
           $errors=$this->validateFields($data,$fields);
           if(empty($errors)){
              $errors=$this->validateaccount($data);
              if(empty($errors)){
                    $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.email'=>trim($data['email']))));
                    if(empty($user)){
                      $apikey=$this->UniqueKey();
                     // $phoneKey = $this->UniqueKey();
                      $user['User']['access_token']=$apikey;
                      $user['User']['name']=$data['name'];
                      $user['User']['password']=md5(trim($data['password']));
                      $user['User']['email']=$data['email'];
                      $user['User']['phone']=$data['phone'];
                      $user['User']['role']=$data['role'];
                      $user['User']['accept_term']=$data['agree'];
                      $user['User']['device_id']=$data['device_id'];
                      $user['User']['device_type']=$data['device_type'];
                      $this->User->save($user);
                      /*Start:Send Welcome Email*/
                       $this->loadModel('Template');
                       $template=$user['User']['role']=='braider'?$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'welcome_braider','Template.type'=>'email'))):$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'welcome_competitor','Template.type'=>'email')));
                       $fields=array('{user}');
                       $replaced=array($user['User']['name']);
                       $content=str_replace($fields,$replaced, $template['Template']['content']);
                       $subject=$template['Template']['subject'];
                       $from=SITE_EMAIL;
                       $to=$user['User']['email'];
                       $response['status']='success';
                       $response['User']=$user['User'];
                       $response['message']='Account has been created successfully.';
                       $this->response->body(json_encode($response));
                       }else{
                       $this->response->statusCode(200);
                       $response['status']='error';
                       $response['errors'] = 'The email address you have entered is already registered with us. Please log into your existing account. Or, if you think this is an error, please contact us';
                       $this->response->body(json_encode($response));  
                    }
              }else{
                    $this->response->statusCode(200);
                    $response['status']='error';
                    $response['errors']=$errors;
                    $this->response->body(json_encode($response));
              }   
           }else{
            $this->response->statusCode(200);
             $response['status']='error';
            $response['message']=$errors;
            $this->response->body(json_encode($response));
           }
        }else{
          $this->response->statusCode(403);
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
        }
    }
   /*End*/
   /*Start: validate account*/ 
    public function validateaccount($data=null){
        $error = array();
        if(!empty($data)){
                $reg='/^[A-Za-z0-9 _]+$/';
                
                if(empty($data['email'])){
                 $error = 'email is required.';
                 return $error;
                }
                if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                 $error = "Invalid email format.";
                 return $error;
                }
                $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
                if (!preg_match($regex, $data['email'])) {
                  $error = "Invalid email format.";
                  return $error;
                }
                if(empty($data['phone'])){
                  $error = 'Please enter the  phone.';
                  return $error;
                }
                if(!(is_numeric($data['phone']))){
                     $error = 'Please enter the  valid phone.';  
                     return $error;
                }
                if(strlen(trim($data['password']))<6){
                     $error = 'Sorry, your password needs to be a minimum of 6 characters';  
                     return $error;
                }
                if(trim($data['password'])!=trim($data['cpassword'])){
                     $error = 'passwords do not match';  
                     return $error;
                }
                if(!in_array($data['role'], array('braider','competitor'))){
                    $error = 'Invalid user type';
                    return $error;
                }
                return $error;
        }else{
            return $error='invalid request.';
        }
            return $error;
    }
   /*End*/
   /*Start:function to get profile info*/
    public function getProfile($token=null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $response=array();
        if($token){
          $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.access_token'=>trim($token))));
          if(!empty($user)){
            $response['status']='success';
            $response['User']=$user['User'];
            $this->response->body(json_encode($response));
          }else{
            $response['status']='error';
            $response['errors'] = 'invalid access token';
            $this->response->body(json_encode($response));
          }
        }else{
          $response['status']='error';
          $response['errors'] = 'invalid access token';
          $this->response->body(json_encode($response));
        }
    }
  /*End*/

  /*Start:function to Reset password*/
   public function resetpassword(){
    Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $data=$_POST;
           $response=array();
           $json=$this->request->input('json_decode');
           $data['id']=@$json->id;
           $data['password']=@$json->password;
           $data['cpassword']=@$json->cpassword;
           $data['access_token']=@$json->access_token;
           $errors=$this->validatereset($data);
           if(empty($errors) && count($errors) ==0){
            $this->loadModel('users');
            $data['password']=md5($data['password']);
 
            if($this->User->save($data)){
 
              $response['status']='true';
              $response['result'] = "Password has been updated successfully.";
              $this->response->body(json_encode($response)); 
            }
           }else{
            $response['status']='error';
            $response['errors'] = 'Password and confirm Password does not match!!';
            $this->response->body(json_encode($response)); 

 
           }
         }else{
          $this->response->statusCode(403);
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
         
         }
         

  }
 
  function validatereset($data=null){
    $error = array();
    if(!empty($data)){
     if(empty($data['password']))
      {
        $error['password'] = 'password is required'; 
      }
       if(empty($data['cpassword']))
      {
        $error['password'] = 'cpassword is required'; 
      }
      if( trim($data['password'] ) != trim( $data['cpassword'] ) ) 
                {
                 $error['cpass'] = "Password and confirm password does not match!!";
                }
      return $error;
    }else{
      return $error;
    }


  }
   
   /*Start:function to update user profile*/
    public function updateProfile(){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($this->request->is('post')){
           $data=$_POST;
           $response=array();
           $json=$this->request->input('json_decode');
           $data['access_token']=@$json->access_token;
           $data['id']=@$json->id;
           $data['email']=@$json->email;
           $data['name']=@$json->name;
           $data['phone']=@$json->phone;
           $fields=array('access_token','id','phone','email','name');
           $errors=$this->validateFields($data,$fields);
           if(empty($errors)){
           $errors=$this->validateprofile($data);
        if(empty($errors)){
          $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.id <>'=>trim($data['id']),'User.email'=>trim($data['email']))));
          if(empty($user)){
              $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.id'=>trim($data['id']),'User.access_token'=>trim($data['access_token']))));
              if(!empty($user)){
                $user['User']['id']=$data['id'];
                $user['User']['name']=$data['name'];
                $user['User']['email']=$data['email'];
                $user['User']['phone']=$data['phone'];
                $this->User->save($user);
                $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.access_token'=>$data['access_token'])));
                $response['status']='success';
                $response['User']=$user['User'];
                $response['message']='Profile has been updated successfully.';
                $this->response->body(json_encode($response));
              }else{
                $response['status']='error';
                $response['errors'] = 'User does not exist.';
                $this->response->body(json_encode($response));  
              }
            }else{
                $response['status']='error';
                $response['errors'] = 'Email is already exist.';
                $this->response->body(json_encode($response)); 
            }
        }else{
          $this->response->statusCode(200);
          $response['status']='error';
          $response['message']=$errors;
          $this->response->body(json_encode($response));
        }
           }else{
            $this->response->statusCode(200);
             $response['status']='error';
            $response['message']=$errors;
            $this->response->body(json_encode($response));
           }
        }else{
          $this->response->statusCode(403);
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
        }
    }
   /*End*/
   /*Start: validate account*/ 
    public function validateprofile($data=null){
        $error = array();
        if(!empty($data)){
                if(empty($data['email'])){
                  $error = 'Email is required.';
                  return $error;
                }
                if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                 $error = "Invalid email format.";
                 return $error;
                }
                $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
                if (!preg_match($regex, $data['email'])) {
                  $error = "Invalid email format.";
                  return $error;
                }
                if(empty($data['phone'])){
                  $error = 'Phone is required.';
                  return $error;
                }
                if(!is_numeric($data['phone'])){
                  $error = 'Please enter the  valid phone number.';
                  return $error;
                }
                return $error;
        }else{
            return $error='Invalid request.';
        }
            return $error;
    }
 
   /*End*/

   /*Start:get braider list*/
    public function getBraiders($limit=null,$page=null){
       Configure::write('debug',2);
       $this->RequestHandler->respondAs('Json');
       $this->autoRender= false;
       $result=array();
       if($limit && $page){
        $currentpage=$page;
        $nextpage=$currentpage+1;
        $page=($page-1)*$limit;
        $braiders=$this->User->find('all',array('contain'=>false,'limit'=>$limit, 'offset'=>$page,'conditions'=>array('User.role'=>'braider','User.status'=>1),'fields'=>array('User.id','User.name','User.email','User.phone','User.role')));
        $braiderscount=$this->User->find('all',array('contain'=>false,'conditions'=>array('User.role'=>'braider','User.status'=>1),'fields'=>array('User.id','User.name','User.email','User.phone','User.role')));
        $total=count($braiderscount);
        $q=intval($total/$limit);
        $r=($total%$limit)>0?1:0;
        $totalpages=$q+$r;
       foreach ($braiders as $key => $value) {
         $result['User'][$key]=$value['User'];
       }
        if(!empty($result)){
              $response['status']='success';
              $response['limit']=$limit;
              $response['currentpage']=$currentpage;
              $response['nextpage']=$nextpage>$totalpages?'':$nextpage;
              $response['pagecount']=$totalpages;
              $response['braiders']=$result;
              $this->response->body(json_encode($response));
           }else{
             $response['limit']=$limit;
             $response['braiders']=array();
             $this->response->body(json_encode($response));
           }
       }else{
        $braiders=$this->User->find('all',array('contain'=>false,'conditions'=>array('User.role'=>'braider','User.status'=>1),'fields'=>array('User.id','User.name','User.email','User.phone','User.role')));
          foreach ($braiders as $key => $value) {
           $result['User'][$key]=$value['User'];
         }
          //prx(count($result['User']));
         if(!empty($braiders)){
            $response['status']='success';
            $response['count']=count($result['User']);
            $response['braiders']=$result;
            $this->response->body(json_encode($response));
         }else{
            $response['status']='success';
            $response['count']=count($result['User']);
            $response['braiders']=array();
            $this->response->body(json_encode($response));
         }
       }
    }
   /*End:*/

   /*Start:Api to fetch braider profile*/
    public function getBraiderProfile($id=null){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $response=array();
      $braider=$this->User->find('first',array('conditions'=>array('User.id'=>$id,'User.role'=>'braider','User.status'=>1)));
      if(!empty($braider)){
          $response['status']='success';
          $response['braider']=$braider;
          $this->response->body(json_encode($response));
      }else{
        $response['status']='success';
        $response['message']='Braider not found.';
        $this->response->body(json_encode($response));
      }
    }

public function  reservationNumber(){
  $this->autoRender=false;
  $this->loadModel('Reservation');
  $fetch = $this->Reservation->find('all',array('order'=>array('Reservation.id'=>'DESC')));
  foreach ($fetch as $key => $value) { 
  if(!empty($value)){  
      $nos= ($value['Reservation']['res_number']);  
      $nos = str_pad(++$nos,4,'0',STR_PAD_RIGHT);   
    }
    return $nos;
    }
}  
   /*End*/
 public function reservations(){
   Configure::write('debug',2);
   $this->RequestHandler->respondAs('Json');
   $this->autoRender= false;
   if($this->request->is('post')){
   $data=$_POST;
   $response=array();
   $json=$this->request->input('json_decode');
   $data['access_token']=@$json->access_token;
   $data['competitor_id']=@$json->competitor_id;
   $data['show_id']=@$json->show_id;
   $data['state_id']=@$json->state_id;
   $data['braider_id']=@$json->braider_id;
   $data['date']=@$json->date;
   $data['horse_id']=@$json->horse_id;
   $data['service_ids']=@$json->service_ids;
   $data['showDivision']=@$json->showDivision;
   $data['barnName']=@$json->barnName;
   $data['barnNumber']=@$json->barnNumber;
   $data['stallNumber']=@$json->stallNumber;
   $data['total_price']=@$json->total_price;
   $data['res_number']=$this->reservationNumber();
   /*Update reservation order*/
   $this->loadModel("Reservation");
   $max=$this->Reservation->find('first', array('contain'=>false,'fields' => array('MAX(Reservation.orders) as max')));
   $data['orders']=$max[0]['max']+1;
   //prx($data);
   $fields=array('show_id','state_id','braider_id','date','horse_id','service_ids');
   $errors=$this->validateFields($data,$fields);
     if(empty($errors)){
      $user=$this->User->find('first',array('conditions'=>array('User.id'=>trim($data['competitor_id']),'User.access_token'=>trim($data['access_token']),'User.role'=>'competitor')));
     // print_r($user);

      if(!empty($user)){
       $braider=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.id'=>trim($data['braider_id']),'User.role'=>'braider'),'fields'=>array('User.email','User.phone')));

       //print_r($user);die;
       if(!empty($braider)){
          if($data['service_ids']){
            $data['service_ids']=serialize($data['service_ids']);
          }
           $data['status']='Pending';
           $this->loadModel('Reservation');
           $res = 0;
            if($this->Reservation->save($data)) {
           $this->Reservation->updateAll(array('Reservation.res_status'=>1),array('Reservation.braider_id'=>$data['braider_id']));
            /*Start:Send Email & SMS*/
             
            $this->loadModel('Horse');
           $horse_id = $this->Horse->find('first',array('contain'=>false,'conditions'=>array('Horse.id'=>$data['horse_id'])));
           $horse_name = $horse_id['Horse']['name'];
           $competitor_name = $user['User']['name'];
       

              if(!empty($braider)){
                 $this->loadModel('Template');
                $template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'request_to_braider','Type'=>'email')));
                $fields = array('{role}','{name}');
                $replaced = array($competitor_name ,$horse_name);  
                $content=str_replace($fields,$replaced,$template['Template']['content']);
                $subject=$template['Template']['subject'];
                $from=$braider['User']['email'];
              //  $this->sendEmail($content,SITE_EMAIL,$from,$subject);
                /*SMS*/
                 $template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'request_to_braider','Type'=>'notify')));
                require_once(APP . 'Vendor' . DS  . 'sms' . DS. 'sms.php');
                $fields = array('{role}','{name}');
                $replaced = array($competitor_name ,$horse_name);  
                $content=str_replace($fields,$replaced,$template['Template']['content']);
                $expertTexting= new experttexting_sms(); 
                $expertTexting->from= admin_phone; 
                $expertTexting->to='1'. $braider['User']['phone']; 
                $expertTexting->msgtext= $content; 
             //   $expertTexting->send();
              }
          //  }
             /*End*/ 
              $response['status']='success';
              $response['message']='Braider has been successfully scheduled.';
               
              
              $this->response->body(json_encode($response));
          }else{
              $response['status']='error';
              $response['message']='Opps! There is something wrong to schedule the braider.';
              $this->response->body(json_encode($response));
          }
         
        }else{
          $response['status']='error';
          $response['message']='Braider does not exist.';
          $this->response->body(json_encode($response));
        }  
     }else{
        $response['status']='error';
        $response['message']='User is not valid to schedule the braider.';
        $this->response->body(json_encode($response));
     }
   }else{
        $response['status']='error';
        $response['message']=$errors;
        $this->response->body(json_encode($response));
   }
}else{
    $response['status']='error';
    $response['errors'] = 'invalid post request';
    $this->response->body(json_encode($response));
}   
} 
    


   public function showBraiders($showId=null,$date=null){ 
      Configure::write('debug',0);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $date=trim($date);
      $this->loadModel('BraiderAvialablity');
      $this->loadModel('BraiderWorkpicture');
      $this->loadModel('Service');
      $braiders = $this->BraiderAvialablity->find('all',array('contain'=>array('User.id','User.name','User.email','User.role'),'conditions'=>array('BraiderAvialablity.show_id'=>trim($showId),'BraiderAvialablity.status'=>1)));
      if(!empty($braiders)){
          $results=array();
          foreach ($braiders as $key => $value) {
              if(in_array($date, $value['BraiderAvialablity']['dates_available'])){
                $services=$this->Service->find('all',array('contain'=>false,'conditions'=>array('Service.braider_id'=>$value['User']['id'])));
                $images = array();
                $ThreeworkImage = array();
                $images = $this->BraiderWorkpicture->find('all',array('conditions'=>array('BraiderWorkpicture.braider_id'=>$value['BraiderAvialablity']['braider_id']),'fields'=>array('BraiderWorkpicture.image'),'order'=>'BraiderWorkpicture.id DESC','limit'=>3));
                
                foreach ($images as $k => $v) {
                  $ThreeworkImage[] = HTTP_ROOT.$v['BraiderWorkpicture']['image'];
                }

                $servs=array();
                foreach ($services as $key1 => $value1) {
                  $servs[$key1] = $value1['Service'];
                }
                
                $results['Braider'][$key]=$value['User'];
                $results['Braider'][$key]['services']=$servs;
                $results['Braider'][$key]['workpicture']=$ThreeworkImage;
                $results['Braider'][$key]['BraiderAvialablity_id'] = @$value['BraiderAvialablity']['id'];
              }
          }  
         if(!empty($results)){
            $response['status']='success';
            $response['braiders'] = $results['Braider'];
            $this->response->body(json_encode($response));
         }else{
            $response['status']='error';
            $response['errors'] = 'No braider is available.';
            $this->response->body(json_encode($response));
         }
      }else{
         $response['status']='error';
         $response['errors'] = 'No braider is available.';
         $this->response->body(json_encode($response));
      }
    }


   /*End*/
   /*Start: API to Update Reservation By Braider*/
    public function updateReservation(){
      Configure::write('debug',0);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      if($this->request->is('post')){
         $data=$_POST;
         $response=array();
         $json=$this->request->input('json_decode');
         $data['reservation_id']=@$json->reservation_id;
         $data['access_token']=@$json->access_token;
         $data['competitor_id']=@$json->competitor_id;
         // $data['show_id']=@$json->show_id;
         // $data['state_id']=@$json->state_id;
         // $data['braider_id']=@$json->braider_id;
         // $data['date']=@$json->date;
         // $data['horse_id']=@$json->horse_id;
         // $data['service_ids']=@$json->service_ids;
         // $data['showDivision']=@$json->showDivision;
         $data['barnName']=@$json->barnName;
         $data['barnNumber']=@$json->barnNumber;
         $data['stallNumber']=@$json->stallNumber;
         // $data['status']=@$json->status;
         // $data['total_price']=@$json->total_price;
         $fields=array('reservation_id');
         $errors=$this->validateFields($data,$fields);
         if(empty($errors)){
            $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.id'=>trim($data['competitor_id']),'User.access_token'=>trim($data['access_token']),'User.role'=>'competitor')));
           if(!empty($user)){
              if($data['service_ids']){
                $data['service_ids']=serialize($data['service_ids']);
              }
               $this->loadModel('Reservation');
               $reservation=$this->Reservation->find('first',array('contain'=>false,'conditions'=>array('Reservation.id'=>trim($data['reservation_id']))));
               if(!empty($reservation)){
                    $data['id']=$data['reservation_id'];
                    if($res=$this->Reservation->save($data)){
                  /*$braider=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.id'=>trim($data['braider_id']),'User.role'=>'braider'),'fields'=>array('User.email','User.phone')));
                  if(!empty($braider)){
                    $this->loadModel('Template');
                    $content=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'appointment_request','Template.type'=>'email')));
                    $this->sendEmail($content['Template']['content'],SITE_EMAIL,$braider['User']['email'],$content['Template']['subject']);
                  }*/
                  $response['status']='success';
                  $response['message']='Reservation has been updated successfully.';
                  $this->response->body(json_encode($response));
                  }else{
                      $response['status']='error';
                      $response['message']='Opps! There is something wrong to update the reservation.';
                      $this->response->body(json_encode($response));
                  }
               }else{
                       $response['status']='error';
                       $response['message']='Reservation not found.';
                      $this->response->body(json_encode($response));
               }
           }else{
              $response['status']='error';
              $response['message']='User is not valid to update reservation.';
              $this->response->body(json_encode($response));
           }
         }else{
              $response['status']='error';
              $response['message']=$errors;
              $this->response->body(json_encode($response));
         }
      }else{
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
      }   
    }
   /*End*/

   /*Start:Api to upload work picture*/
    public function addWorkPicture(){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      if($this->request->is('post')){
         $data=$_POST;
         $response=array();
         $json=$this->request->input('json_decode');
         $data['access_token']=@$json->access_token;
         $data['image']=@$json->image->image;
         $data['image_caption']=@$json->image->caption;
         $fields=array('image','access_token','image_caption');
         $errors=$this->validateFields($data,$fields);
         if(empty($errors)){
            $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>trim($data['access_token']),'User.role'=>'braider')));
            if(!empty($user)){
              if($data['image']){
              $data['image']=$this->upload_image($data['image'],$user['User']['name'],'../../app/webroot/img/work/');
              
              if(!empty($data['image'])){
                 $data['image']='/img/work/'.$data['image'];
              }else{
                 $response['status']='error';
                 $response['errors'] = 'Image is not valid.';
                 echo json_encode($response);die;
              }
              }else{
                $data['image']="";
              }
              $data['braider_id']=$user['User']['id'];
              $this->loadModel('BraiderWorkpicture');
              if($this->BraiderWorkpicture->save($data)){
                $response['status']='success';
                $response['message'] = 'Image has been added successfully.';
                $this->response->body(json_encode($response));
              }else{
                $response['status']='error';
                $response['errors'] = 'Opps! there is something wrong to upload image.';
                $this->response->body(json_encode($response));
              }
            }else{
              $response['status']='error';
              $response['errors'] = 'User is not valid to upload picture.';
              $this->response->body(json_encode($response));
            }
         }else{
          $response['status']='error';
          $response['errors'] = $errors;
          $this->response->body(json_encode($response));
         }
       }else{
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
       }
    }
   /*End*/

   /*Start:Api to fetch braidier portfolio*/
    public function getPortfolio($access_token=null){
      Configure::write('debug',0);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>trim($access_token),'User.role'=>'braider')));
      if(!empty($user)){
         $this->loadModel('BraiderWorkpicture');
         $portfolio=$this->BraiderWorkpicture->find('all',array('conditions'=>array('BraiderWorkpicture.braider_id'=>$user['User']['id']),'order'=>'BraiderWorkpicture.id DESC'));
         if(!empty($portfolio)){
            // prx($portfolio);
            $results=array();
            foreach ($portfolio as $key => $value) {
              $value['BraiderWorkpicture']['image']=HTTP_ROOT.$value['BraiderWorkpicture']['image'];
              $results['portfolio'][$key]=$value['BraiderWorkpicture'];
            }
            $response['status']='success';
            $response['portfolio'] = $results['portfolio'];
            $this->response->body(json_encode($response));
         }else{
          $response['status']='success';
          $response['message'] = 'No image added yet.';
          $this->response->body(json_encode($response));
         }
      }else{
          $response['status']='error';
          $response['message'] = 'invalid user type.';
          $this->response->body(json_encode($response));
      }
    }
    /*public function getPortfolioworkpicture($braider_id=null){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $user=$this->User->find('first',array('conditions'=>array('User.id'=>trim($braider_id),'User.role'=>'braider')));
      if(!empty($user)){
         $this->loadModel('BraiderWorkpicture');
         $portfolio=$this->BraiderWorkpicture->find('all',array('conditions'=>array('BraiderWorkpicture.braider_id'=>$user['User']['id']),'order'=>'BraiderWorkpicture.id DESC','limit'=>3));
        if(!empty($portfolio)){
            // prx($portfolio);
            $results=array();
            $result = array();
            foreach ($portfolio as $key => $value) {
              $results['BraiderWorkpicture']['image']=HTTP_ROOT.$value['BraiderWorkpicture']['image'];
              $result[] = $results['BraiderWorkpicture']['image'];
            }
            $response['status']='success';
            $response['portfolioworkpicture'] = $result;
            $this->response->body(json_encode($response));
         }else{
          $response['status']='success';
          $response['message'] = 'No image added yet.';
          $this->response->body(json_encode($response));
         }
      }else{
          $response['status']='error';
          $response['message'] = 'invalid user type.';
          $this->response->body(json_encode($response));
      }
    }*/
   /*End*/

   public function getPortfolioworkpicture($braider_id=null){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      $user=$this->User->find('first',array('conditions'=>array('User.id'=>trim($braider_id),'User.role'=>'braider')));
      if(!empty($user)){
         $this->loadModel('BraiderWorkpicture');
         $portfolio=$this->BraiderWorkpicture->find('all',array('conditions'=>array('BraiderWorkpicture.braider_id'=>$user['User']['id']),'order'=>'BraiderWorkpicture.id DESC','limit'=>3));
        if(!empty($portfolio)){
            // prx($portfolio);
            $results=array();
            $result = array();
            foreach ($portfolio as $key => $value) {
              $results['BraiderWorkpicture']['image']=HTTP_ROOT.$value['BraiderWorkpicture']['image'];
              $result[] = $results['BraiderWorkpicture']['image'];
            }
            $response['status']='success';
            $response['portfolioworkpicture'] = $result;
            $this->response->body(json_encode($response));
         }else{
          $response['status']='success';
          $response['message'] = 'No image added yet.';
          $this->response->body(json_encode($response));
         }
      }else{
          $response['status']='error';
          $response['message'] = 'invalid user type.';
          $this->response->body(json_encode($response));
      }
    }

   /*Start:Api to edit work picture*/
     public function editWorkPicture(){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      if($this->request->is('post')){
         $data=$_POST;
         $response=array();
         $json=$this->request->input('json_decode');
         $data['access_token']=@$json->access_token;
         $data['image']=@$json->image;
         $data['image_caption']=@$json->image_caption;
         $data['id']=@$json->id;
         $fields=array('image','access_token','image_caption','id');
         $errors=$this->validateFields($data,$fields);
         $this->loadModel('BraiderWorkpicture');
         if(empty($errors)){
            $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>trim($data['access_token']),'User.role'=>'braider')));
            if(!empty($user)){
                $chek=$this->BraiderWorkpicture->find('first',array('conditions'=>array('BraiderWorkpicture.id'=>$data['id'],'BraiderWorkpicture.braider_id'=>$user['User']['id'])));
                if(!empty($chek)){
                  if($data['image']){
                  $data1['image']=$this->upload_image($data['image'],$user['User']['name'],'../../app/webroot/img/work/');
                  
                  if(!empty($data1['image'])){
                     $data['image']='/img/work/'.$data1['image'];
                  }
                  }
                  $data['braider_id']=$user['User']['id'];
                  if($this->BraiderWorkpicture->save($data)){
                    $response['status']='success';
                    $response['message'] = 'Image has been updated successfully.';
                    $this->response->body(json_encode($response));
                  }else{
                    $response['status']='error';
                    $response['message'] = 'Opps! there is something wrong to upload image.';
                    $this->response->body(json_encode($response));
                  }
              }else{
                $response['status']='error';
                $response['message'] = 'image does not exist.';
                $this->response->body(json_encode($response));
              }    
            }else{
              $response['status']='error';
              $response['message'] = 'User is not valid to upload picture.';
              $this->response->body(json_encode($response));
            }
         }else{
          $response['status']='error';
          $response['message'] = $errors;
          $this->response->body(json_encode($response));
         }
       }else{
          $response['status']='error';
          $response['message'] = 'invalid post request';
          $this->response->body(json_encode($response));
       }
    }
   /*End*/

   /*Start: Api to delete workpicture*/
     public function deleteWorkPicture($access_token=null,$id=null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>$access_token,'User.role'=>'braider')));
        if(!empty($user)){
            $this->loadModel('BraiderWorkpicture');
            $chek=$this->BraiderWorkpicture->find('first',array('conditions'=>array('BraiderWorkpicture.id'=>$id,'BraiderWorkpicture.braider_id'=>$user['User']['id'])));
            if(!empty($chek)){
              if($this->BraiderWorkpicture->delete($id)){
                  $dest=realpath('../../app/webroot');
                  unlink($dest.$chek['BraiderWorkpicture']['image']);
                  $response['status']='success';
                  $response['message'] = 'Image has been deleted successfully.';
                  $this->response->body(json_encode($response));  
              }else{
                $response['status']='error';
                $response['error'] = 'Opps! there is something wrong to delete image.';
                $this->response->body(json_encode($response));  
              }
            }else{
              $response['status']='error';
              $response['errors'] = 'Image does not exist.';
              $this->response->body(json_encode($response));
            }
        }else{
          $response['status']='error';
          $response['errors'] = 'invalid user';
          $this->response->body(json_encode($response));
        }
     }
   /*End*/

   /*----------------------------Show Listing for CSV------- */
   function admin_viewList()
  {
    $this->layout="admin";
    $this->paginate = array(
                        "limit"=> 10);
    $this->set('users',$this->paginate('User'));
    
   } 

public function admin_delete($id = null) {
    if (!$this->request->is('post')) {
        throw new MethodNotAllowedException();
    }
    $this->User->id = $id;
    if (!$this->User->exists()) {
        throw new NotFoundException(__('Invalid user'));
    }
    if ($this->User->delete()) {
        $this->Session->setFlash(__('User deleted'));
        $this->redirect(array('action' => 'viewSuspended'));
    }
    $this->Session->setFlash(__('User was not deleted'));
    $this->redirect(array('action' => 'viewSuspended'));
}



function admin_viewActive($id = null)
  {
   $this->layout="admin";
    $this->paginate = array("limit"=> 10,'conditions'=>array('User.status'=>'1'),'contain'=>false);
    $this->set('users',$this->paginate('User'));
      
 $this->layout='admin';
         $this->loadModel('User');
         
          if(!empty($id)){ 

            $this->redirect(array('controller'=>'users','action'=>'viewActive/id:'.$id,'admin'=>true));
             
          } 
          else{
          if($this->request->is('post'))
          {  
            $data = $this->data;
            $name = $data['filter']['value'];
            $type = $data['filter']['bytype'];
            if(isset($type)){

          $this->redirect(array('controller'=>'users','action'=>'viewActive/'.$type.':'.$name.'/type:'.$type,'admin'=>true));

        }
          }
      }
            
      if(isset($this->request->params['named']['Name'])){
              $name = $this->request->params['named']['Name'];
              $type=  $this->request->params['named']['type'];
              $this->paginate = array('limit'=>20,
                      'conditions' => array(
                      'User.name LIKE' => "%$name%")); 
            $this->set('users',$this->paginate('User'));
           // pr($this->paginate('State'));die;
            
            $this->set('name',$name);
            $this->set('type',$type);
              }
   else if(isset($this->request->params['named']['Email'])){
          $name = $this->request->params['named']['Email'];
          $type=  $this->request->params['named']['type'];
          $this->paginate = array('limit'=>20,
                      'conditions' => array(
                      'User.email  LIKE' => "%$name%"));  
          $this->set('users',$this->paginate('User'));
            $this->set('name',$name);
            $this->set('type',$type);
              }

   } 



   function admin_viewActives($id=null)
  {  


      $this->layout="admin";
      $this->loadModel('User');
     if($id)
     {           
           $users = $this->User->find('first', array('conditions' => array('User.id' => $id)));
              $user = $users['User']['status'];
           if($user == 1)
                {  
                  $this->User->updateAll(array("status"=>0),array("id"=>$id));
                }
                else
                {
                $this->User->updateAll(array("status"=>1),array("id"=>$id));
                }
      }
       $this->redirect(array('controller'=>'users','action' => 'viewActive','admin' => true)); 
          
                      
}

   function admin_viewSuspended()
  {  
     $this->layout="admin";
     $this->paginate = array('conditions' => array('User.status' => '0'),'limit' => 10);
     $data = $this->paginate('User');
     $this->set('users',$data);
 
}
 
 public function admin_deleteSuspended($id) {
   $this->layout="admin";
      $this->loadModel('User');
     if($id)
     {           
           $users = $this->User->find('first', array('conditions' => array('User.id' => $id)));
              $user = $users['User']['status'];
           if($user == 1)
                {  
                  $this->User->updateAll(array("status"=>0),array("id"=>$id));
                }
                else
                {
                $this->User->updateAll(array("status"=>1),array("id"=>$id));
                }
      }
  
      return $this->redirect(array('action' => 'admin_viewSuspended'));

 
}


function admin_export_report()

{  
   $this->autoRender = false;
  
         $this->loadModel('User');
          $data = '';

                          
                
              ini_set("memory_limit",-1);
                        // $data .= 'Name'.",";
                        // $data .= 'Email'.",";
                        // $data .= 'Role'.",";
                        // $data .= 'Phone'.",";
                        // $data .= 'Created'.",";
                        // $data .="\n";
        
$result =  $this->User->find('all',array('fields'=>array('name','email','role','phone','created'),'recursive'=>'-1'));

              foreach ($result as $rslt) {

                        $data .= $rslt['User']['name'].",";
                        $data .= $rslt['User']['email'].",";
                        $data .= $rslt['User']['role'].",";
                        $data .= $rslt['User']['phone'].",";
                        $data .= $rslt['User']['created'].",";
                        $data .="\n";
                     }                 
                    header("Content-Type: application/csv");
                    header("Content-type: application/octet-stream");
                    $csv_filename = 'Reporting_Status'."_".date('M').date('dy').".csv";
                    header("Content-Disposition:attachment;filename=$csv_filename");
                    $fd = fopen ($csv_filename, "w");
                    fputs($fd,$data);
                    fclose($fd);
                    echo $data;
                    die();
                    $this->Session->setFlash('CSV file record has been downloaded please check your browser or folder..','success');
                    //$this->redirect($this->referer());
        }

               
          
 


  
  function admin_editShow($id = null)
  {
     $this->layout="admin";
      $this->loadModel('Show');
$this->loadModel('State');
   $data = $this->State->find('all',array('fields'=>array('id','name')));  
      $this->set('data',$data);
 
    
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $show = $this->Show->findById($id);
    if (!$show) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Show->id = $id;
        if ($this->Show->save($this->request->data)) {
            //$this->Session->setFlash(__('Your post has been updated.'));
            return $this->redirect(array('action' => 'admin_dashboard'));
        }
        //$this->Session->setFlash(__('Unable to update your post.'));
    }

    if (!$this->request->data) {
        $this->request->data = $show;
    }

}

public function admin_deleteShow($id) {
           $this->loadModel('Show');
   if ($this->request->is('post')) {
    if ($this->Show->delete($id)) {
      //$this->Session->setFlash(__('The post number %s has been deleted.', h($id)));
      return $this->redirect(array('action' => 'admin_dashboard'));
    }
  }
}


  function admin_forgotPassword(){
  
 $this->User->recursive=-1;
 if(!empty($this->data))
 {
 if(empty($this->data['User']['email']))
 {
 $this->Session->setFlash('Please Provide Your Email Address that You used to Register with Us');
 }
 else
 {
 $email=$this->data['User']['email'];

 $fu=$this->User->find('first',array('conditions'=>array('User.email'=>$email)));

 if($fu)
 {
  if($fu['User']['access_token'])
 {
 $key = Security::hash(String::uuid(),'sha512',true);
 $hash=sha1($fu['User']['name'].rand(0,100));
 $url = Router::url( array('controller'=>'users','action'=>'admin_reset'), true ).'/'.$key.'#'.$hash;
 $ms=$url;
 $ms=wordwrap($ms,1000);
 //debug($url);
 $fu['User']['tokenhash']=$key;
 $this->User->id=$fu['User']['id'];
 if($this->User->saveField('tokenhash',$fu['User']['tokenhash'])){
 
 //============Email================//
 /* SMTP Options */
 //$this->Email->smtpOptions = array(
 //'port'=>'25',
 //'timeout'=>'30',
 //'host' => 'mail.example.com',
 //'username'=>'accounts+example.com',
 //'password'=>'your password'
 //  );
   $this->Email->template = 'resetpw';

 $this->Email->from    = 'mss.mastersoftwaresolutions.com';
 $this->Email->to      = $fu['User']['name'].'<'.$fu['User']['email'].'>';
 $this->Email->subject = 'Reset Your Password';
 $this->Email->sendAs = 'both';
 
 $this->Email->delivery = 'smtp';
  $this->set('ms', $ms);
 //$this->Email->send();
  $this->set('smtp_errors', $this->Email->smtpError);
 $this->Session->setFlash(__('Check Your Email To Reset your password', true));

 //============EndEmail=============//
 }
 else{
 $this->Session->setFlash("Error Generating Reset link");
 }
 }
 else
 {
 $this->Session->setFlash('This Account is not Active yet.Check Your mail to activate it');
 }
 }
 else
 {
 $this->Session->setFlash('Email does Not Exist');
 }
 }
 }
 }



function admin_reset($token=null){
    //$this->layout="admin";
 //$this->layout="Login";
 $this->User->recursive=-1;
 if(!empty($token)){
 $u=$this->User->findBytokenhash($token);
 if($u){
 $this->User->id=$u['User']['id'];
 if(!empty($this->data)){
 $this->User->data=$this->data;
 $this->User->data['User']['name']=$u['User']['name'];
 $new_hash=sha1($u['User']['name'].rand(0,100));//created token
 $this->User->data['User']['tokenhash']=$new_hash;
 if($this->User->validates(array('fieldList'=>array('password','password_confirm')))){
 if($this->User->save($this->User->data))
 {
 $this->Session->setFlash('Password Has been Updated');
 $this->redirect(array('controller'=>'users','action'=>'admin_login'));
 }
 
 }
 else{
 
 $this->set('errors',$this->User->invalidFields());
 }
 }
 }
 else
 {
 $this->Session->setFlash('Token Corrupted,,Please Retry.the reset link work only for once.');
 }
 }
 
 else{
 $this->redirect('/');
 }
 }

public function admin_sendNotification() 
 { 

  $this->layout="admin";
  if($this->request->is('post')){
  $data =  $this->request->data;  
  $textarea =  $this->request->data('textarea');
$this->sendISONotification('5a3f2a1ab2a9034bf8e57d692641a78b3e381b95123db01aa226595490cc6773',$textarea); die("Notification Sent");  
  if($data['type']== '') {
  $sql =$this->User->find('all',array('contain'=>false,'fields'=>array('User.email','User.device_id','User.device_type')));  
 
  foreach ($sql as $key => $value) { 
  $ms=$textarea;  
  $from = SITE_EMAIL;   
  $to = $value['User']['email'];
  $subject = 'Notification';     
  $this->sendEmail($ms,$from,$to,$subject);
 
         /* Notification code goes here */ 
                           if($value['User']['device_type']=='android'){
                                 $deviceid=$value['User']['device_id'];
                              $this->sendNotification($deviceid,$ms);  
                            }
                           if($value['User']['device_type']=='iphone'){
                              $deviceid=$value['User']['device_id'];
                              $this->sendISONotification($deviceid,$ms);  
                            } 
     }
}
  $role = $this->User->find('all',array('contain'=>false,'fields'=>array('User.email','User.device_id','User.device_type'),'conditions'=> array('User.role'=>$data['type'])));  
  foreach ($role as $key => $value) {  
  $ms=$textarea;
  $from = SITE_EMAIL;
  $to = $value['User']['email']; 
  $subject = 'Notification';    
  $ss= $this->sendEmail($ms,$from,$to,$subject); 
   
         /* Notification code goes here */ 
                           if($user['User']['device_type']=='android'){
                                 $deviceid=$user['User']['device_id'];
                              $this->sendNotification($deviceid,$ms);  
                            }
                           if($user['User']['device_type']=='iphone'){  
                              $deviceid=$user['User']['device_id'];
                              $this->sendISONotification($deviceid,$ms);  
                            }
   } 
   $this->redirect(array('controller'=>'users','action' => 'dashboard','admin' => true)); 
   }
 }

/* Start:Generate the unique Key*/
function random_string() {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < 7; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}
/* End*/

 /*Start:Api to sent password link*/
    public function passwordLink($email=null){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      if($email){
         $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.email'=>trim($email))));
         if(!empty($user)){
 
            // $data['reset_password']=$this->UniqueKey();
 
             $data['reset_password']=$this->random_string();
 
             $data['id']=$user['User']['id'];
              //$uid=$data['reset_password'];
             //$url=$_SERVER['HTTP_HOST'].'/braider/braider/#/reset_password?token='.$uid;
             $pass=$this->random_string();
             $data['password']=md5($pass);
             $content="Your New Password is $pass";
             //$content="<a href=$url>Click here to reset your password.</a>";
             $subject="Your New Password";
             //prx($html);
             $from=ALERT_SENDER_EMAIL;
             $to=$email;
             $this->sendEmail($content,$from,$to,$subject);
             $this->User->save($data);
             $response['error']=false;
             $response['message']='New Password has been sent on your email id. Please check your email.';
             $this->response->header('Access-Control-Allow-Origin', '*');
             $this->response->body(json_encode($response));
         }else{
            $response['error']=true;
            $response['message']='Invalid email';
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->body(json_encode($response));
         }
      }else{
           $response['error']=true;
           $response['message']='Email is required.';
           $this->response->header('Access-Control-Allow-Origin', '*');
           $this->response->body(json_encode($response));
      }
    }

    /*End*/

    /*Start:Function to update password*/
     public function reset_password($token=null){
        Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        if($token){
           if($this->request->is('post')){
              $user=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.reset_password'=>trim($token))));
              if(!empty($user)){
                $data['password']=@$_POST['password'];
                $data['cpassword']=@$_POST['cpassword'];
                $data['id']=$user['User']['id'];
                $data['reset_password']='';
                $fields = array('password','cpassword');
                $errors=$this->validateFields($data,$fields);
                if(empty($errors)){
                  if($data['password']==$data['cpassword']){
                    $data['password']=md5($data['password']);
                    if($this->User->save($data)){
                      $response['error']=false;
                      $response['message']='Password has been updated successfully.';
                      $this->response->header('Access-Control-Allow-Origin', '*');
                      $this->response->body(json_encode($response));
                    }else{
                        $response['error']=true;
                       $response['message']='Opps! there is something wrong to update password.';
                       $this->response->header('Access-Control-Allow-Origin', '*');
                       $this->response->body(json_encode($response));
                    }
                    }else{
                      $response['error']=true;
                      $response['message']='Password and Confirm Password do not match.';
                      $this->response->header('Access-Control-Allow-Origin', '*');
                      $this->response->body(json_encode($response));
                    }
                 
                }else{
                  $response['error']=false;
                  $response['message']=$errors;
                  $this->response->header('Access-Control-Allow-Origin', '*');
                  $this->response->body(json_encode($response));
                }
              }else{
                $response['error']=false;
                $response['message']='Password link has been expired.';
                $this->response->header('Access-Control-Allow-Origin', '*');
                $this->response->body(json_encode($response));
              }
                
           }else{
              $response['error']=true;
              $response['message']='Invalid post request.';
              $this->response->header('Access-Control-Allow-Origin', '*');
              $this->response->body(json_encode($response));
           }
        }else{
           $response['error']=true;
           $response['message']='Invalid reset password link.';
           $this->response->header('Access-Control-Allow-Origin', '*');
           $this->response->body(json_encode($response));
        }
     }
    /*End*/

  /*   public function testmail(){
    
        $this->sendEmail('This is test email ','mss.yogendra@gmail.com',"mss.birisingh@gmail.com","test",'YOgendra');
die("email sent.");
     } */

  }
 
?>
