<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	 var $components=array('Session','Email','RequestHandler','Paginator');
	function checkAdminSession(){
		$adminSession = $this->Session->read('User');
		if(isset($adminSession['id']) && isset($adminSession['email']) && ($adminSession['usertype'] == 'admin')){
			return true;
		}
		else{
			return false;
		}
	}

	function UniqueKey() {
        return md5(uniqid(rand(), true));
        }

    public function validateFields($data,$fields){
	    $error = array();
	    foreach($fields as $key=>$value){
	      if (!isset($data[$value]) || !$data[$value]) {
	              $error[]=$value." "."is required.";
	             }
	    }

	    return $error;

	}

	public function upload_image($content=null,$imgname=null,$dest=null,$extn=null){
        if($content){
        	    $dataarray=explode('base64,', $content);
        	    if(count($dataarray)==2){
        	    	$extn=explode('/', $dataarray[0]);
        	    	if(count($extn)==2){
        	    		$extn=rtrim($extn[1],';');
	        	    	if($extn){
	        	    		$img_name = $imgname."-".time();
			                $data = $dataarray[1];
			                $data = str_replace(' ', '+', $data);
			                $data = base64_decode($data);
			                file_put_contents($dest.$img_name.".".$extn, $data);

			                chmod($dest.$img_name.".".$extn, 0777);
			                return $img_name.".".$extn;
	        	    	}else{
	        	    		return false;
	        	    	}
        	    	}else{
        	    		return false;
        	    	}
        	    }else{
        	    	return false;	
        	    }
        }else{
            return false;
        }
    } 

      /*function sendEmail($email_template=null,$from=null,$to=null,$subject=null){
		$Email = new CakeEmail();
		// prx($email_template);
		$Email->template('common_template')
		    ->emailFormat('html')
		    ->to($to)
		    ->from($from)
		    ->viewVars(array('content' => $email_template))
		    ->subject($subject)
		    ->send();

     	}*/
     	
     function sendEmail($email_template=null,$from=null,$to=null,$subject=null,$sender=null){

     	        $this->Email->to = $to;
		$this->Email->subject=$subject;
		$this->Email->from =$from;
	        $this->Email->sender =$sender;
                $this->Email->template = 'default';
		$this->Email->sendAs = 'both'; 
		/*$this->Email->smtpOptions = array(
		     'port'=>'25',		  
		     'host' => 'mail.braiderbooking.com',		     
		     'username'=>'test@braiderbooking.com',
		     'password'=>'Admin123#'
		);*/
               /* $this->Email->smtpOptions = array(
		     'port'=>'587',		  
		     'host' => 'smtp.mandrillapp.com',		     
		     'username'=>'braiderbooking',
		     'password'=>'C1INt0SeNL1oep1_iKZliQ'
		);

 		$this->Email->delivery = 'smtp';*/
 		$this->Email->send($email_template); 	
 		
     }
public function sendSMS($content=null,$to=null){
	      require_once(APP . 'Vendor' . DS  . 'sms' . DS. 'sms.php');
	      $expertTexting= new experttexting_sms(); // Create SMS object.
	      $expertTexting->from= admin_phone; // Sender of the SMS – PreRegistered through the Customer Area.
	      $expertTexting->to= '1'.$to; // The full International mobile number of the without + or 00
	      $expertTexting->msgtext= $content; // The SMS content.*/
	      $expertTexting->send();
     }


    public function notify($regIds, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::GOOGLE_GCM_URL);
        if (!is_null($this->proxy)) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        }
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostFields($regIds, $data));
        $result = curl_exec($ch);
        if ($result === false) {
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        $this->output = $result;
    }
    /**
     * @return array
     */
    public function getOutputAsArray()
    {
        return json_decode($this->output, true);
    }
    /**
     * @return object
     */
    public function getOutputAsObject()
    {
        return json_decode($this->output);
    }

    private function getHeaders(){
        return [
            'Authorization: key=' . $this->apiKey,
            'Content-Type: application/json'
        ];
    }

    private function getPostFields($regIds, $data){
        $fields = [
            'registration_ids' => is_string($regIds) ? [$regIds] : $regIds,
            'data'             => is_string($data) ? ['message' => $data] : $data,
        ];
        return json_encode($fields, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
    }

     /*Send push notification*/
    function sendNotification( $deviceID = null, $message = null )
     {  
        require_once(APP . 'Vendor' . DS  . 'android' . DS .  'gcm.php');
        require_once(APP . 'Vendor' . DS  . 'android' . DS .  'config.php');
        Configure::write('debug', 2);
        $gcm = new GCM();
        $registatoin_ids = array($deviceID);
        $message = array("messages" => $message);
        $result = $gcm->send_notification($registatoin_ids, $message);
         
        return $result;    
    } 
       
    function sendISONotification( $deviceToken = NULL, $message = NULL ){  
        $passphrase="1234567";
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'braider.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
            );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
//echo ($result);die;
        if (!$result)
            echo 'Message not delivered' . PHP_EOL;
        else
            echo 'Message successfully delivered' . PHP_EOL;

        // Close the connection to the server
        fclose($fp);

    }


}
