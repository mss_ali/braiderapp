<?php
ob_start();
 class NotificationsController extends AppController {
  var $components=array('Session','Email','RequestHandler','Paginator','Resize'); 
  
   function beforeFilter(){
		$this->disableCache();
		parent::beforeFilter();
        if(!$this->CheckAdminSession() && $this->request->prefix=='admin' && !in_array($this->request->action,array('admin_login','admin_forgotPassword','admin_reset_password','admin_chkadminemail'))){
				$this->redirect(array('action' => 'login','admin' => true));
				exit();
		}
	}
 public function index(){
  /*require(APP . 'Vendor' . DS  . 'sms' . DS. 'sms.php');
  $expertTexting= new experttexting_sms(); // Create SMS object.
  $expertTexting->from= '19132697254'; // Sender of the SMS – PreRegistered through the Customer Area.
  $expertTexting->to= '919988570425'; // The full International mobile number of the without + or 00
  $expertTexting->msgtext= 'Demo Test Message'; // The SMS content.*/
  //echo $expertTexting->send();
 /* $this->sendEmail('test msg','mohdali@mastersoftwaresolutions.com','mohdali@mastersoftwaresolutions.com','test email');
      die('email has been sent.');*/
 die;
 }
 /*Start:Api to send message to competitor by braider & vice-versa*/
   public function senMessage(){
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender= false;
      if($this->request->is('post')){
         $data=$_POST;
         $response=array();
         $json=$this->request->input('json_decode');
         $data['access_token']=@$json->access_token;
         $data['to_user']=@$json->to_user;
         $data['msg']=@$json->msg;
         $data['reservation_id']=@$json->reservation_id;
         $fields=array('access_token','to_user','msg');
         $errors=$this->validateFields($data,$fields);
         if(empty($errors)){
            $this->loadModel('User');
            $user=$this->User->find('first',array('conditions'=>array('User.access_token'=>$data['access_token'])));
            if(!empty($user)){
                $checkTo=$this->User->find('first',array('conditions'=>array('User.id'=>$data['to_user'],'User.role'=>$user['User']['role'])));
                if(empty($checkTo)){

                  $existTo=$this->User->find('first',array('conditions'=>array('User.id'=>$data['to_user'])));
                  if(!empty($existTo)){
                       $data['from_user']=$user['User']['id'];
                       if($this->Notification->save($data)){
                      /* Notification code goes here */ 
                          $this->sendSMS($data['msg'],$existTo['User']['phone']);
                       /* Email */
                       $this->loadModel('Template');
                       $template=$this->Template->find('first',array('conditions'=>array('Template.identifier'=>'messageToemail','Template.type'=>'email')));
                       $fields=array('{masg}');
                       $replaced=array($data['msg']);
                       $content=str_replace($fields,$replaced, $template['Template']['content']);
                       $subject=$template['Template']['subject'];
                       $from=SITE_EMAIL;
                       $to=$existTo['User']['email'];
                       $this->sendEmail($content,$from,$to,$subject);
                          
                       /* Email */
                          $response['status']='success';
                          $response['message'] = 'Message has been sent successfully.';
                          $this->response->body(json_encode($response));
                       }else{
                          $response['status']='error';
                          $response['message'] = 'Opps! there is something wrong to send message.';
                          $this->response->body(json_encode($response));
                       }
                  }else{
                    $response['status']='error';
                    $response['message'] = 'To User does not exist.';
                      -$this->response->body(json_encode($response));
                  }     
                }else{
                  $response['status']='error';
                  $response['message'] = 'To User is not valid to send message.';
                  $this->response->body(json_encode($response));
                }
            }else{
              $response['status']='error';
              $response['message'] = 'User is not valid to send message.';
              $this->response->body(json_encode($response));
            }
         }else{
           $response['status']='error';
           $response['message'] = $errors;
           $this->response->body(json_encode($response));
         }
      }else{
          $response['status']='error';
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
      }
   }
 /*End*/
 /*Api to check message is read or not*/
 public function readMessage(){
    Configure::write('debug',2);
    $this->RequestHandler->respondAs('Json'); 
    $this->autoRender= false;
       if($this->request->is('post')){
         $data=$_POST;
         $response=array();
         $json=$this->request->input('json_decode');
         $data['id']=@$json->id;
         $fields=array('id');
         $errors=$this->validateFields($data,$fields);
         if(empty($errors)){ 
            $this->loadModel('Notification');
          if(isset($data['id'])){  
            $update = $this->Notification->updateAll(array("mark_as_read"=> 0),array('id'=>$data['id']));
            return 1;
           }
           else
           {
            return 0;
           }
         }else{
           $response['status']='error';
           $response['message'] = $errors;
           $this->response->body(json_encode($response));
         }
         
      }
 
 }
 /*End*/

 
/*Api to save reservation ordering*/
public function reservationOrder(){ 
      Configure::write('debug',2);
      $this->RequestHandler->respondAs('Json');
      $this->autoRender = false;
      $this->loadModel('Reservation');
      if($this->request->is('post')){
        $data = $_POST;
        $response = array();
        $json = $this->request->input('json_decode');
        $data['id']  = @$json->id;
        $data['orders'] = @$json->orders;
        $fields = array('id','orders');
        $errors = $this->validateFields($data,$fields);
          if(empty($errors)){
             $reservation  = $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('id'=>$data['id']))); prx($reservation);
             foreach ($reservation as $key => $value) {
             $order = $value['Reservation']['orders'];
             }
            if($order <= $data['orders'] && $order != $data['orders']){ 
              $update = $this->Reservation->updateAll(array('Reservation.orders'=>$data['orders']),array('Reservation.id'=>$data['id']));
              $response['status'] = "success";
              $response['message'] = "Updated successfully";
              $this->response->body(json_encode($response));
            }
            else{
              $response['status'] = "error";
              $response['message'] = "Orders number should be greater then prevoius number";
              $this->response->body(json_encode($response)); 
            }
              /*    $fetch_orders = $this->Reservation->find('all',array('contain'=>false,'conditions'=>array('Reservation.orders'=>$data['orders'])));*/
            /* $fetch_orders = $this->Reservation->find('all',array('fields'=>'orders','conditions'=>array('Reservation.orders'=>$data['orders'])));
            foreach ($fetch_orders as $key => $value) {
                $orders = $value['Reservation']['orders']; print_r($orders);
                if($orders <= $data['orders']){ 
                   $updatess = $this->Reservation->updateAll(array('Reservation.orders'=>'Reservation.orders'),array('Reservation.id'=>$data['id']));
                  print_r($updatess); echo "less";
                }else{
                  $updates = $this->Reservation->updateAll(array('Reservation.orders'=>$data['orders']),array('Reservation.id'=>$data['id']));
                     print_r($updates); echo "fail";
                }
            }*/
          }             
          else
          {
          $response['status'] = "false";
          $response['message'] = $errors;
          $this->response->body(json_encode($response));
          }
      }
  }

 /**/

 /*Api for push notification*/
     // public function pushNotification(){
     //    $this->autoRender = false;
     //    App::import('Vendor', 'SimplePush/simplepush');
     // }
  public function pushNotification()
   {

  /*  require_once(APP . 'Vendor' . DS  . 'ios' . DS .  'simplepush.php');
    //require_once(APP . 'Vendor' . DS  . 'ios' . DS .  'ck.pem');
       Configure::write('debug',2);
        $this->RequestHandler->respondAs('Json');
        $this->autoRender= false;
        $response=array();
        $successcount=0;
        $failurcount=0;
        $fields=array('message');
        if($this->request->is('post')){
            $data=$_POST;
            $errors=$this->validateFields($data,$fields);
            if(empty($errors)){
              if($data['message']){
                  $msg=trim($data['message']);
                  $this->loadModel('Notification'); 
                  $deviceIDs = $this->Notification->find('all',array('conditions'=>array('Notification.device_id <> '=>NULL),'fields'=>array('Notification.device_id')));
                  foreach ($deviceIDs as $key => $value) {
                   $result=json_decode($this->sendNotification($value['Notification']['device_id'],$msg));
                   if($result->success){
                      $successcount++;
                   }else{
                     $failurcount++;
                   }
                   continue;
                  }
                  $response['success']=$successcount;
                  $response['apikey']=$this->request->data['apikey'];
                  unset($this->request->data['apikey']);
                  $response['failure']=$failurcount;
                  $this->response->body(json_encode($response));
              }else{
                  $this->response->statusCode(406);
                  $response['status']='error';
                  $response['apikey']=$this->request->data['apikey'];
                  unset($this->request->data['apikey']);
                  $response['message']='Message can not be empty';
                  $this->response->body(json_encode($response));
              }  
            }else{
              $this->response->statusCode(400);
              $response['status']='error';
              $response['apikey']=$this->request->data['apikey'];
              unset($this->request->data['apikey']);
              $response['message']=$errors;
              $this->response->body(json_encode($response));
            }
        }else{
          $this->response->statusCode(403);
          $response['status']='error';
          $response['apikey']=$this->request->data['apikey'];
          unset($this->request->data['apikey']);
          $response['errors'] = 'invalid post request';
          $this->response->body(json_encode($response));
        }
*/
    /*End*/

    /*die('sss');
    $this->autoRender =false;
    $this->layout='admin';
    if($this->request->is('post'))
    { 
      $this->loadModel('Notification'); 
      $data = $this->data;
      $message = trim($data['message']);
      $deviceIDs = $this->Notification->find('all',array('conditions'=>array('Notification.device_id <> '=>NULL),'fields'=>array('Notification.device_id')));
      foreach ($deviceIDs as $key => $value) {
       $this->sendNotification($value['Notification']['device_id'],$message);
      }
        $this->Session->write('success','Notification has been sent !!');
      $this->redirect(array('action' => 'newsLetSub','admin' => true));
  }
*/
  }

 /**/
 
}
?>
