<body>

    <div id="wrapper">

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Reservations</h1>
                </div>
           <div id="page-layout">
 <div class="loadPaginationContent">
 <?php 
    if(isset($name) &&  isset($type)){
    $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"viewReservation/".$type.":".$name."/type:".$type)));  }
     else{
  $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"viewReservation"))); 
 
}
 ?>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12" id="dropdown-list">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                       <?php echo $this->Form->create('filterorder',array('id'=>'filterorder','method'=>'post','url'=>array('controller'=>'shows','action'=>'viewReservation')));?>
                      </div>
                      <div class="filter-part">
        <p style="padding-left:15px;"><label>Filter By</label></td><td>
          <select class="form-control field text full required" id="select" name="data[filter][bytype]">
            <option value="">Select Type</option>
            <option <?php if(@$type == 'State') { ?> selected="selected" <?php } ?> value="State">State</option>
            <option <?php if(@$type == 'Show') { ?> selected="selected" <?php } ?> value="Show">Show</option>
            <option <?php if(@$type == 'Competitor') { ?> selected="selected" <?php } ?>  value="Competitor">Competitor</option>
            <option <?php if(@$type == 'Braider') { ?> selected="selected" <?php } ?>  value="Braider">Braider</option>
         
         </select>
          <input class="form-control" type="text" name="data[filter][value]" id="filtertext" required="required" value="<?php 
          echo @$name;
            
          ?>">
         <input class="sub-bttn" type="button" id="filterby" value="Submit"/>
         
            <?php echo $this->Html->link('Reset',array('controller' =>'shows','action'=>'viewReservation'),array('class'=>'sub-bttn','style'=>'margin-top:10px')); ?></br>
           <?php echo $this->Form->end(); ?>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php
                                 echo "<th>" . $paginator->sort('State.name','State') . "</th>";
                                  echo "<th>" . $paginator->sort('Show.showName','Show') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.braider','Braider Name') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.competitor','Competitor Name') . "</th>";
                                  echo "<th>" . $paginator->sort('Horse.name','Horse Name') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.service_ids','Service') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.total_price', 'Amount') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.created', 'Date Scheduled') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.date', 'Appointment Date') . "</th>";
                                  echo "<th>" . $paginator->sort('Reservation.status', 'Reservation Status') . "</th>";
                                  echo "<th>" . $paginator->sort('Show.divisions', 'Division') . "</th>";
                                 

                                        ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                            <?php
                            if(!empty($reservation))
                            {  //echo "<pre>";print_r($reservation);die;
                                foreach( $reservation as $key=>$value)
                               
                                        {
                                          $test = $value['Reservation']['service_ids'];
                                          $name = @implode(',', $test);
                                          $start_date=new DateTime($value['Reservation']['created']);
                                          $Appointment_date=new DateTime($value['Reservation']['date']); ?>
                                         <td><?php echo $value['State']['name']; ?></td>  
                                          <td><?php echo $value['Show']['showName']; ?></td> 
                                          <td><?php echo $value['Reservation']['braider']?></td> 
                                          <td><?php echo $value['Reservation']['competitor']?></td>
                                          <td><?php echo $value['Horse']['name']?></td>

                                          <?php
                                          $n = 0; 
                                          $getServiceName = $this->requestAction('Shows/getServiceName/'.$name.'/'.$n, array('return'=>true));
                                          ?>
                                          <td><?php echo $getServiceName ?></td>
                                          <td><?php echo $value['Reservation']['total_price']?></td>
                                          <td><?php echo $start_date->format("j, F, Y") ?></td>
                                          <td><?php echo $Appointment_date->format("j, F, Y") ?></td>
				          <td><?php echo $value['Reservation']['status'] ?></td>
                                          <td><?php echo $value['Show']['divisions']?></td>
                                        

                                        </tr>
                                        
                                       <?php }
                              }
                            else
                              {
                                echo "<tr>";
                                echo "<td colspan='8'>No Record Found</td>";
                                echo "</tr>";
                              }

                                        ?>                                       
                                    </tbody>
                                </table>
                                <div class="records-p">
                                 <?php  echo $this->Html->link('Download Listing',array('controller'=>'Shows','action'=>'export_reservation'), array('target'=>'_blank')); ?>
                               </div>

                                 <div class="records-p">     
                           <?php
                         
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </div>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>




                              
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   
  <script type="text/javascript">
        $(document).ready(function(){
         $('#filterby').on('click',function(){
          var isSelect = $('#select').val();
          var filtertext = $('#filtertext').val();
          if(isSelect == ''){
            alert('Please select the value');
            return false;
          } if(filtertext=='') {
            alert('Please Enter the '+ isSelect);
            return false;
          }
          if(isSelect != '' && filtertext != '')
          {
            $('#filterorder').submit();
          }

         });
        });
        </script>


    <script>
    // $(document).ready(function() {
    //     $('#dataTables-example').DataTable({
    //             responsive: true
    //     });
    // });
     </script>

</body>

</html>


















