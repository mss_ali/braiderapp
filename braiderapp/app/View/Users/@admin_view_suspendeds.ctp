        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage User</h1>
                </div>
                <div class="loadPaginationContent">
 <?php 
    if(isset($name) &&  isset($type)){
    $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"viewReservation/".$type.":".$name."/type:".$type)));  }
     else{
  $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"viewReservation"))); 
 
}
 ?>
            </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
               
                <div class="panel panel-default">
                        <div class="panel-heading">
          <?php echo $this->Form->create('filterorder',array('id'=>'filterorder','method'=>'post','url'=>array('controller'=>'Users','action'=>'viewActives')));?>
                      </div>
                      <div class="filter-part">
        <p style="padding-left:15px;"><label>Filter By</label></td><td>
          <select class="form-control field text full required" id="select" name="data[filter][bytype]">
            <option value="">Select Type</option>
            <option <?php if(@$type == 'State') { ?> selected="selected" <?php } ?> value="State">State</option>
            <option <?php if(@$type == 'Show') { ?> selected="selected" <?php } ?> value="Show">Show</option>
            <option <?php if(@$type == 'Competitor') { ?> selected="selected" <?php } ?>  value="Competitor">Competitor</option>
            <option <?php if(@$type == 'Braider') { ?> selected="selected" <?php } ?>  value="Braider">Braider</option>
         
         </select>
          <input class="form-control" type="text" name="data[filter][value]" id="filtertext" required="required" value="<?php 
          echo @$name;
            
          ?>">
         <input class="sub-bttn" type="button" id="filterby" value="Submit"/>
           <a href="<?php echo HTTP_ROOT."admin/shows/viewReservation" ?>"  style="margin-top:10px;"><input class="sub-bttn" type="button" value="Reset"/></a></br>
           <?php echo $this->Form->end(); ?>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div class="row"><div class="col-sm-12">
                                
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php 
                                    echo "<th>" . $paginator->sort('User.name', 'Name') . "</th>";
                                    echo "<th>" . $paginator->sort('User.role', 'Type') . "</th>";
                                    echo "<th>" . $paginator->sort('User.email', 'Email') . "</th>";
                                    echo "<th>" . $paginator->sort('Action') . "</th>";
                                    
                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                     <tr class="odd gradeX">
                                <?php
 
                               foreach ($users as $user)
                               {
                                             echo "<tr>";
                                                echo "<td>{$user['User']['name']}</td>";
                                                echo "<td>{$user['User']['role']}</td>";
                                                echo "<td>{$user['User']['email']}</td>";
                                                echo "<td class='actions'>";
               echo $this->Form->postLink( 'Trash User', array(
                       'action' => 'admin_deleteSuspended', 
                     $user['User']['id']), array(
                            'confirm'=>'Are you sure you want to delete that division?' ));
                                               

                                             echo "</tr>";
     
                              }
                                                    ?>
                                   </tr></tbody>
                                </table>
                               <div class="records-p">             
 <?php
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </div>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>

                   

                            </div></div>
                                        <!-- Pagination -->

                            </div>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>
            <!-- /.row -->