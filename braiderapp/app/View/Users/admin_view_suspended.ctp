        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
               
                <div class="panel panel-default">
                        <div class="panel-heading">
                             Manage Listing 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div class="row"><div class="col-sm-12">
                                
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php 
                                    echo "<th>" . $paginator->sort('User.name', 'Name') . "</th>";
                                    echo "<th>" . $paginator->sort('User.role', 'Type') . "</th>";
                                    echo "<th>" . $paginator->sort('User.email', 'Email') . "</th>";
                                    echo "<th>" . $paginator->sort('User.Status', 'Status') . "</th>";
                                    echo "<th>" . $paginator->sort('Action') . "</th>";
                                    
                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                     <tr class="odd gradeX">
                                <?php
                              if(!empty($users))
                              {
                               foreach ($users as $user)
                               {

                                 
                                        echo "<tr>";
                                                echo "<td>{$user['User']['name']}</td>"; ?>
                                                 <?php
                                                 if($user['User']['role']== 'compititor' || $user['User']['role']== 'compititor'|| $user['User']['role'] !== 'admin' )
                                                 { ?><?php
                                                echo "<td>{$user['User']['role']}</td>"; }
                                                echo "<td>{$user['User']['email']}</td>";
                                                if($user['User']['status'] == 1)
                                                {
                                                echo "<td>Active</td>";
                                                }
                                                else
                                                {
                                                  echo "<td>Suspended</td>";
                                                }
                                                echo "<td class='actions'>";
             
                                   echo $this->Form->postLink( 'Activate user', array(
                       'action' => 'admin_deleteSuspended', 
                     $user['User']['id']), array(
                            'confirm'=>'Are you sure you want Active this user? '));
                         echo $this->Form->postLink('Trash user', array('action' => 'admin_delete',$user['User']['id']),array('confirm'=>'Are you sure you want to delete this user?',$user['User']['id'])); 



                                             echo "</tr>";
     
                                }
                               }
                               else
                               {
                                  echo "<tr>";
                                  echo "<td colspan='5'>No Record Found</td>";
                                  echo "</tr>";
                               } 
                            
                                                 ?>
                                   </tr></tbody>
                                </table>
                              <div class="records-p">             
 <?php
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </div>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>

                   

                            </div></div>
                                        <!-- Pagination -->

                            </div>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>
            <!-- /.row -->
