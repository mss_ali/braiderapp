<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">
    .error {
    	color: red;
		}
    </style>
    <?php 
      echo $this->Html->css('admin/bootstrap/bootstrap.min.css');
      echo $this->Html->css('admin/menu/dist/metisMenu.min.css');
      echo $this->Html->css('admin/sb-admin-2.css');
	    	echo $this->Html->css('admin/font-awesome/css/font-awesome.min.css');
    ?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default" id="login-screen">
                  <div class="m-user"><img alt="login-user" src="http://mastersoftwaretechnologies.com/braider/braiderapp/img/login-user.png"></div>
                  <p>Admin login here!</p>
                    <div class="panel-body">
                        <?php if($this->Session->check('error')){ ?>
							<div class="response-msg error ui-corner-all">
								<?php echo $this->Session->read('error');?>
							</div>
                            <?php $this->Session->delete('error'); ?>
						<?php } ?>
                        
                        <?php if($this->Session->check('success')){ ?>
							<div class="response-msg success ui-corner-all" id="success">
							<span>Success message</span>
					 	 <?php echo $this->Session->read('success');?>
							</div>
						<?php $this->Session->delete('success'); ?>
						<?php } ?>
						<div class="response-msg success ui-corner-all" id="resp_msg" style="display:none;">
							<span>Success message</span>
						<label class="msg_flash"></label>
						</div>	
                        <form role="form" id="adminLoginForm" method="post" action="<?=HTTP_ROOT?>/admin/users/login">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="data[User][email]" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="data[User][password]" type="password" value="">
                                </div>
                               <!--  <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div> -->
                                <!-- Change this to a button or input when using this as a form -->
                               
                                <div class="submit"><input type="submit" value="Login" class="btn btn-lg btn-success btn-block"></div>
                            </fieldset>
                        </form>
                        <div class="forgot-password-btn">
                        <?php
                                echo $this->Html->link(__('Forgot Password'), array('controller' => 'Users', 'action' => 'admin_forgotPassword'), array('style' => 'color:red;font-size:20px;'));

                                ?>
                              </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 	<?php  
          echo $this->Html->script('admin/jquery/jquery.min.js');
          echo $this->Html->script('admin/bootstrap/bootstrap.min.js');
          echo $this->Html->script('admin/menu/metisMenu.min.js');
          echo $this->Html->script('admin/sb-admin-2.js');
          echo $this->Html->script('jquery.validate.js');
    ?>
<script type="text/javascript">
$(document).ready(function(){
   $('#adminLoginForm').validate({
      onfocusout: function (element) {
             $(element).valid();
            },
         rules:
         {
            "data[User][email]":
            {
              
                required: 
                {
                    depends:function()
                    {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true
              }, 
           
            "data[User][password]":
            {
               required:true,
               minlength: 6
            }
         },
         messages:
         {
            "data[User][email]":
            {
               required:'Please enter email.',
               email:'Please enter valid email.'
            },
            "data[User][password]":
            {
               required:"This field is required.",
               minlength: 'Password should be atleast 6 characters long.'
            }
         }
      });
   
   });
</script>


</body>

</html>
