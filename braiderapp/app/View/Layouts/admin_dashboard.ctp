<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>
<?php
     
    echo $this->Html->css('bootstrap.min.css');
    echo $this->Html->css('metisMenu.min.css');
     echo $this->Html->css('timeline.css');
      echo $this->Html->css('sb-admin-2.css');
       echo $this->Html->css('morris.css');
       echo $this->Html->css('font-awesome.min.css');
  
?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
      
      <?php echo $this->element('left-sidebar');?>     
       <?php echo $this->element('header');?>        
   
<?php echo $this->fetch('content'); ?>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery --><?php
    echo $this->Html->script('jquery.min.js');
   
echo $this->Html->script('bootstrap.min.js');
 echo $this->Html->script('metisMenu.min.js');
   echo $this->Html->script('raphael-min.js');
    echo $this->Html->script('morris.min.js');
    echo $this->Html->script('morris-data.js');
      echo  $this->Html->script('sb-admin-2.js');
   
?>
</body>

</html>
