<!DOCTYPE html>
<html lang="en">

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <?php echo $this->Session->flash(); ?>
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Add Division</h3>
                    </div>
                    <div class="panel-body">
                      
                            
                            <?php echo $this->Form->create('Division'); ?>
                             <fieldset>
                               <div class="form-group">                         

                      <?php echo $this->Form->input('name',array('class' => 'form-control','placeholder'=> 'Division Name','label'=>false)); ?>
                     
                      </div>
                        </fieldset>
                            <div class="form-group">
                                    <?php echo $this->Form->submit('Add Division',array('class' => 'btn btn-lg btn-success btn-block'));  ?>
                                </div>
                                           





                          
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>

</html>