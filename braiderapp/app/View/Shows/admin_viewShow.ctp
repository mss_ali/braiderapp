<h2>Users</h2>
 
<!-- link to add new users page -->
<div class='upper-right-opt'>
    <?php echo $this->Html->link( '+ New User', array( 'action' => 'addShow' ) ); ?>
</div>
 
<table style='padding:5px;'>
    <!-- table heading -->
    <tr style='background-color:#fff;'>
        <th>state_id</th>
        <th>showName</th>
        <th>startDate</th>
        <th>endDate</th>
        <th>created</th>
        <th>divisions</th>
        <th>updated</th>
    </tr>
  <?php
  //loop to show all retrieved records
    foreach( $shows as $show ){
     
        echo "<tr>";
            echo "<td>{$show['Show']['id']}</td>";
            echo "<td>{$show['Show']['showName']}</td>";
            echo "<td>{$show['Show']['startDate']}</td>";
            echo "<td>{$show['Show']['endDate']}</td>";
            echo "<td>{$show['Show']['created']}</td>";
            echo "<td>{$show['Show']['divisions']}</td>";
            echo "<td>{$show['Show']['updated']}</td>";
            //here are the links to edit and delete actions
            echo "<td class='actions'>";
                echo $this->Html->link( 'Edit', array('action' => 'edit', $user['User']['id']) );
                 
                //in cakephp 2.0, we won't use get request for deleting records
                //we use post request (for security purposes)
                echo $this->Form->postLink( 'Delete', array(
                        'action' => 'delete', 
                        $user['User']['id']), array(
                            'confirm'=>'Are you sure you want to delete that user?' ) );
            echo "</td>";
        echo "</tr>";
    }
?>
     
</table>