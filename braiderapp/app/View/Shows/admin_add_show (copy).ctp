 
        <div id="page-wrappers">
            <br></br>
                    <div class="panel panel-default add-show">
                      
                        <div class="panel-heading">
                           Add Show
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                             <?php echo $this->Form->create('Show',array('id'=>'addshow')); ?>
                            
                            <form role="form">
                                 <table class="" id="dataTables-example"> 
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">State</label>
                                            <select name = "data[Show][state_id]" id="state_id" class="form-control">
                      <?php foreach ($data as $datas): ?>
                       <option value="<?php echo $datas['State']['id']; ?>">
                            <?php echo $datas['State']['name']; ?></option>
                      <?php endforeach; ?>
                             </select> 
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">Show Name</label>
                                    <?php echo $this->Form->input('showName',array('class'=>"form-control",'placeholder'=> 'Show Name','id'=>"inputSuccess", 'label'=>false)); ?>    
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">Start Date</label>
                                            <input type="text" name = "data[Show][startDate]" class="datepicker form-control" id="txtFrom" placeholder = 'Enter Start Date' >
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">End Date</label>
                                            <input type="text" name = "data[Show][endDate]" class="datepicker form-control" id="txtTo"  placeholder = 'Enter End Date' >
                                        </div>
                                        <div class="form-group has-successs">
                                            <label class="control-label" for="inputSuccess">Division</label>
                                            <select name = "data[Show][divisions]" class="form-control">
                        <?php foreach ($divisions as $division): ?>
                       <option value="<?php echo $division['Division']['name']; ?>">
                            <?php echo $division['Division']['name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                                        </div>
                                   
                    <?php  echo $this->Form->submit('Add Show',array('class' => 'btn btn-primary'));  ?> 
                       </table> 
                    </form>
               
                    
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
           
           
        </div>
<script type="text/javascript">
$(function () {
    $("#txtFrom").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#txtTo").datepicker("option","dateFormat", "yy-mm-dd","minDate", dt);
        }
    });
    $("#txtTo").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#txtFrom").datepicker("option","dateFormat", "yy-mm-dd", "maxDate", dt);
        }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#addshow').validate({
           onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
              "data[Show][state_id]":
                {
                    required:true
                    
                },
              "data[Show][showName]":
               
                {
                    required:true,
                     maxlength: 60
                    
                },
                "data[Show][startDate]":
               
                {
                    required:true
                                         
                },
                "data[Show][endDate]":
               
                {
                    required:true
                                         
                },
              "data[Show][divisions]":
                {
                    required:true
                    
                }


            },
            messages:
            {
               "data[Show][state_id]":
                {
                    required:"Please enter the state id"
                    
                },
             
                "data[Show][showName]":
                {
                     required:"Please enter the Show Name",
                     maxlength:"Please enter less then 60 character"
                  
                },
                "data[Show][startDate]":
                {
                     required:"Please enter the Start Date"
                     
                  
                },
                "data[Show][endDate]":
                {
                     required:"Please enter the End Date"
                     
                  
                },
                "data[Show][divisions]":
                {
                   required:"Please enter Divisions"
                  
                }
             
            }
        
        
        });
        
    
    });
</script>
  
