<div id="sub-nav">
  <div class="page-title">
  
  </div>
</div>
<div id="page-layout">
 <div class="loadPaginationContent"> <div id="wrapper">

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Braiders' Schedules</h1>
                </div>
           <div id="page-layout">
 <div class="loadPaginationContent">
 <?php 
    if(isset($name) &&  isset($type)){
    $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"showSchedule/".$type.":".$name."/type:".$type)));  }
    else{
       $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"showSchedule"))); 
	}
 ?>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12" id="dropdown-list">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                       <?php echo $this->Form->create('filterorder',array('id'=>'filterorder','method'=>'post','url'=>array('controller'=>'shows','action'=>'showSchedule')));?>
                      </div>
                      <div class="filter-part">
        <p style="padding-left:15px;"><label>Filter By</label></td><td>
          <select class="form-control field text full required" id="select" name="data[filter][bytype]">
            <option value="">Select Type</option>
            <option <?php if(@$type == 'Show') { ?> selected="selected" <?php } ?> value="Show">Show</option>
            <option <?php if(@$type == 'Braider') { ?> selected="selected" <?php } ?>  value="Braider">Braider</option>
         </select>
          <input class="form-control" type="text" name="data[filter][value]" id="filtertext" required="required" value="<?php 
          echo @$name;
            
          ?>">
         <input class="sub-bttn" type="button" id="filterby" value="Submit"/>
         
            <?php echo $this->Html->link('Reset',array('controller' =>'shows','action'=>'showSchedule'),array('class'=>'sub-bttn','style'=>'margin-top:10px')); ?></br>
           <?php echo $this->Form->end(); ?>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php
                                 echo "<th>" . $paginator->sort('Show.showName','Show') . "</th>";
                                 echo "<th>" . $paginator->sort('User.name','Braider Name') . "</th>";
                                 echo "<th>" . $paginator->sort('BraiderSchedule.date','Available Date') . "</th>";
                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                            <?php
                            if(!empty($shows))
                            {
                                foreach( $shows as $key=>$value)
                                
                                        {
                                        ?>
                                         <td><?php echo $value['BraiderSchedule']['showname']; ?></td>  
                                          <td><?php echo $value['BraiderSchedule']['braidername']; ?></td> 
                                          <td><?php echo $value['BraiderSchedule']['date']?></td> 
                                        </tr>
                                        
                                       <?php }
                              }
                            else
                              {
                                echo "<tr>";
                                echo "<td colspan='8'>No Record Found</td>";
                                echo "</tr>";
                              }

                                        ?>                                       
                                    </tbody>
                                </table>
                                <div class="records-p">
                                 <?php  echo $this->Html->link('Download Listing',array('controller'=>'Shows','action'=>'export_braidershedule'), array('target'=>'_blank')); ?>
                               </div>

                                 <div class="records-p">     
                           <?php
                         
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </div>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>
                              
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   
    <script type="text/javascript">
        $(document).ready(function(){
         $('#filterby').on('click',function(){
          var isSelect = $('#select').val();
          var filtertext = $('#filtertext').val();
          if(isSelect == ''){
            alert('Please select the value');
            return false;
          } if(filtertext=='') {
            alert('Please Enter the '+ isSelect);
            return false;
          }
          if(isSelect != '' && filtertext != '')
          {
            $('#filterorder').submit();
          }

         });
        });
    </script>
 </div> 
</div>
<div class="clear"></div>
<style type="text/css">
	.table th:first-child {
    width: 232px;
}
</style>	
