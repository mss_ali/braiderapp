<body>

    <div id="wrapper">

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Reservations</h1>
                </div>
           <div id="page-layout">
 <div class="loadPaginationContent">
 <?php 
    if(isset($name) &&  isset($type)){
    $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"viewReservation/".$type.":".$name."/type:".$type)));  }
     else{
  $this->Paginator->options(array('url' => array('controller'=>'Shows','action'=>"viewReservation"))); 
 
}
 ?>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

 <?php echo $this->Form->create('filterorder',array('id'=>'filterorder','method'=>'post','url'=>array('controller'=>'shows','action'=>'viewReservation')));?>
        <p style="margin-left:1%;">Filter By</td><td>
          <select style="width:248px;padding:4px;border:1px solid #abadb3;margin-left:1%;" class="field text full required" id="select" name="data[filter][bytype]">
            <option value="">Select Type</option>
            <option <?php if(@$type == 'State') { ?> selected="selected" <?php } ?> value="State">State</option>
            <option <?php if(@$type == 'Show') { ?> selected="selected" <?php } ?> value="Show">Show</option>
            <option <?php if(@$type == 'Competitor') { ?> selected="selected" <?php } ?>  value="Competitor">Competitor</option>
            <option <?php if(@$type == 'Braider') { ?> selected="selected" <?php } ?>  value="Braider">Braider</option>
         
         </select>
          <input type="text" name="data[filter][value]" id="filtertext" required="required" value="<?php 
          echo @$name;
            
          ?>">
         <input class="sub-bttn" type="button" id="filterby" value="Submit"/>
           <a href="<?php echo HTTP_ROOT."admin/shows/viewReservation" ?>"  style="margin-top:10px;"><input class="sub-bttn" type="button" value="Reset"/></a></br>
           <?php echo $this->Form->end(); ?>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php
                                 echo "<th>" . $paginator->sort('State') . "</th>";
                                 echo "<th>" . $paginator->sort('Show') . "</th>";
                                  echo "<th>" . $paginator->sort('Braider Name') . "</th>";
                                 echo "<th>" . $paginator->sort('Competitor Name') . "</th>";
                                 echo "<th>" . $paginator->sort('Service') . "</th>";
                                 echo "<th>" . $paginator->sort('Amount') . "</th>";
                                 echo "<th>" . $paginator->sort('Date') . "</th>";
                                 echo "<th>" . $paginator->sort('Division') . "</th>";
                                 

                                        ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                            <?php
                                foreach( $reservation as $key=>$value)
                                
                                        {
                                        $start_date=new DateTime($value['Service']['created']); ?>
                                         <td><?php echo $value['State']['name']; ?></td>  
                                          <td><?php echo $value['Show']['showName']; ?></td> 
                                          <td><?php echo $value['Reservation']['braider']?></td> 
                                          <td><?php echo $value['Reservation']['competitor']?></td>
                                          <td><?php echo $value['Service']['name']?></td>
                                          <td><?php echo $value['Service']['price']?></td>
                                          <td><?php echo $start_date->format("j, F, Y") ?></td>
                                          <td><?php echo $value['Show']['divisions']?></td>
                                        

                                        </tr>
                                        
                                       <?php } ?>                                       
                                    </tbody>
                                </table>
                                 <?php  echo $this->Html->link('Download Listing',array('controller'=>'Shows','action'=>'export_reservation'), array('target'=>'_blank')); ?>

                           <?php
                         
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </p>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>




                              
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   
  <script type="text/javascript">
        $(document).ready(function(){
         $('#filterby').on('click',function(){
          var isSelect = $('#select').val();
          var filtertext = $('#filtertext').val();
          if(isSelect == ''){
            alert('Please select the value');
            return false;
          } if(filtertext=='') {
            alert('Please Enter the '+ isSelect);
            return false;
          }
          if(isSelect != '' && filtertext != '')
          {
            $('#filterorder').submit();
          }

         });
        });
        </script>


    <script>
    // $(document).ready(function() {
    //     $('#dataTables-example').DataTable({
    //             responsive: true
    //     });
    // });
     </script>

</body>

</html>

















