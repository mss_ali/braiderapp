        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <div class="row">
               
                <div class="panel panel-default">
               <div class="col-lg-12" id="dropdown-list">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                       <?php echo $this->Form->create('filterorder',array('id'=>'filterorder','method'=>'post','url'=>array('controller'=>'users','action'=>'viewActive')));?>
                      </div>
                      <div class="filter-part">
        <p style="padding-left:15px;"><label>Filter By</label></td><td>
          <select class="form-control field text full required" id="select" name="data[filter][bytype]">
            <option value="">Select Type</option>
            <option <?php if(@$type == 'Name') { ?> selected="selected" <?php } ?> value="Name">Name</option>
            <option <?php if(@$type == 'Email') { ?> selected="selected" <?php } ?> value="Email">Email</option>
          
         </select>
          <input class="form-control" type="text" name="data[filter][value]" id="filtertext" required="required" value="<?php  echo @$name;  ?>">
         <input class="sub-bttn" type="button" id="filterby" value="Submit"/>
           <a href="<?php echo HTTP_ROOT."admin/users/viewActive" ?>"  style="margin-top:10px;"><input class="sub-bttn" type="button" value="Reset"/></a></br>
           <?php echo $this->Form->end(); ?>
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php 
                                    echo "<th>" . $paginator->sort('User.name', 'Name') . "</th>";
                                    echo "<th>" . $paginator->sort('User.role', 'Type') . "</th>";
                                    echo "<th>" . $paginator->sort('User.email', 'Email') . "</th>";
                                    echo "<th>" . $paginator->sort('Action') . "</th>";
                                    
                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                     <tr class="odd gradeX">
                                <?php
 
                               foreach ($users as $user)
                               {
                              // echo "<pre>";  print_r($user);
                                             echo "<tr>";
                                                echo "<td>{$user['User']['name']}</td>";
                                                echo "<td>{$user['User']['role']}</td>";
                                                echo "<td>{$user['User']['email']}</td>";
                                                echo "<td class='actions'>";
              // echo $this->Html->link( 'Active/Deactive', array('action' => 'admin_viewActives', $user['User']['id']) );
                                                if($user['User']['status'] == 1)
                                                {
                                                    echo $this->Form->postLink( 'Active', array(
                       'action' => 'viewActives', 'admin'=>true,
                     $user['User']['id']));
                                   
                                                }
                                               
                                               else
                                               {
                                                  echo $this->Form->postLink( 'Deactive', array(
                       'action' => 'admin_viewActives', 
                     $user['User']['id']));
                                               }

                                             echo "</tr>";
     
                              }
                                                    ?>
                                   </tr></tbody>
                                </table>
                              <div class="records-p">             
 <?php
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </div>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>

                   

                            </div></div>
                                        <!-- Pagination -->

                            </div>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
            </div>
            <!-- /.row -->
             <script type="text/javascript">
        $(document).ready(function(){
         $('#filterby').on('click',function(){
          var isSelect = $('#select').val();
          var filtertext = $('#filtertext').val();
          if(isSelect == ''){
            alert('Please select the value');
            return false;
          } if(filtertext=='') {
            alert('Please Enter the '+ isSelect);
            return false;
          }
          if(isSelect != '' && filtertext != '')
          {
            $('#filterorder').submit();
          }

         });
        });
        </script>