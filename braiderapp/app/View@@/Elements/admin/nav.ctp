 <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?=HTTP_ROOT?>admin/users/dashboard" class="navbar-brand">Braider App Admin</a>
            </div>

            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        
                        <li><a href="<?=HTTP_ROOT?>admin/users/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            
                        </li> -->
                        <li>
                           <!-- <i class="fa fa-dashboard fa-fw"> <?php //echo $this->Html->link('dashboard',array('controller' =>'Users','action'=>'dashboard')); ?></i>  -->
                           
                        </li>
                        <li>
                         <a href="<?php echo HTTP_ROOT?>shows/viewShow">Manage Shows<span class="fa arrow"></span></a> 
                            <ul class="nav nav-second-level">
                                 <li>
                                    <?php echo $this->Html->link('List Shows',array('controller' =>'Shows','action'=>'viewShow')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Add Show',array('controller' =>'Shows','action'=>'addShow')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Import Show',array('controller' =>'Shows','action'=>'importShow')); ?>
                                </li>
                            </ul>
                        </li>
                         <li>
                             <a href="#">Manage Division<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <?php echo $this->Html->link('List Division',array('controller' =>'Divisions','action'=>'view_division')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Add Division',array('controller' =>'Divisions','action'=>'add_division')); ?>
                                </li>
                               
                            </ul>
                        </li>
                        <li>
                             <a href="<?php echo HTTP_ROOT?>admin/Templates/view_template"></i>Email Template</a>
                        </li>
                        <li>
                           <a href="#"></i>Reporting<span class="fa arrow"></span></a>
                       
                           
                            <ul class="nav nav-second-level">
                                <li>
                                    <?php echo $this->Html->link('Users',array('controller' =>'Users','action'=>'viewList')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Reservation List',array('controller' =>'Shows','action'=>'viewReservation')); ?>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                           <a href="#"></i>Block User<span class="fa arrow"></span></a>
                       
                           
                            <ul class="nav nav-second-level">
                                <li>
                                    <?php echo $this->Html->link('Users',array('controller' =>'Users','action'=>'viewActive')); ?>
                                </li>
                                <li>
                                     <?php echo $this->Html->link('Suspended Users',array('controller' =>'Users','action'=>'viewSuspended')); ?>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                      
                                <li>
                                    <?php echo $this->Html->link('CMS Pages',array('controller' =>'CmsPage','action'=>'view_cms_page')); ?>
                                </li>
                       
                         <li>
                           <a href="<?php echo HTTP_ROOT?>admin/Users/sendNotification">Manage Notifications</a> 
                        </li>


                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>