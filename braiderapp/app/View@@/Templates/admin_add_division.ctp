<body>

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Email</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
                
                    <div class="panel panel-default">
                        <?php echo $this->Session->flash(); ?>
                        <div class="panel-heading">
                           Add Show Table
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <?php echo $this->Form->create('Division',array('id'=>'division')); ?>
                                 
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example"> 
                        
                     <div class="form-group">
                    <tr><th>Division Name</th>
                    <td><input type="text" name = "data[Division][name]" placeholder ="Division Name" class = "form-control"></td></tr>
                  </div>
                   
                    
                    </table>
                     <?php   echo $this->Form->submit('Add Division',array('class' => 'btn btn-lg btn-success btn-blocks'));  ?>

             
                            </div>
                          
                           
                        </div>
                     
                    </div>
                  
              
           
           
        </div>
  <script type="text/javascript">
$(document).ready(function(){
   $('#division').validate({
      onfocusout: function (element) {
             $(element).valid();
            },
         rules:
         {
            "data[Division][name]":
            {
              
               required:true
              
           }
         
         },
         messages:
         {
             "data[Division][name]":
            {
               required:'Please enter Division.'
                
            }
           
         }
      });
   
   });
</script>


    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>


</body>

</html>