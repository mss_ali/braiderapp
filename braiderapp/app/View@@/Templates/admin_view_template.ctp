<body>
    <div id="wrapper">
        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Email</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        
                             <?php  echo $this->Html->link('Back to Dashboard',array('controller'=>'Users','action'=>'admin_dashboard'), array('target'=>'_blank')); ?>
                         </div>
                             <div class="panel-body">
                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                           <th align="center">S No</th> 
                                           <th align="center">Identifier</th>
                                           <th align="center">Type</th>
                                           <th align="center">Subject</th>
                                           <th align="center">Content</th>
                                           <th align="center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                            <?php
                            if(!empty($templates))
                                { 
                                    $i=1;
                                foreach( $templates as $template )
                                        {
   
           
                                    echo "<tr>"; ?>
                                        <td><?php echo $i; ?></td> <?php
                                        echo "<td>{$template['Template']['identifier']}</td>";
                                        echo "<td>{$template['Template']['type']}</td>";
                                        echo "<td>{$template['Template']['subject']}</td>";
                                        echo "<td>{$template['Template']['content']}</td>";
                                        echo "<td class='actions'>";
                                        echo $this->Html->link( 'Edit', array('action' => 'admin_edit_template', $template['Template']['id']) );
                                        echo "</td>";
                                        echo "</tr>";
                               
                                
                                $i++; 
                                         } 
                                } 
                              else 
                                { 
                                    echo "<tr>";
                                    echo "<td colspan='7'>No Record Found.</td>";
                                    echo "</tr>";
                                }
                                 ?>          
                             </tr>
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
