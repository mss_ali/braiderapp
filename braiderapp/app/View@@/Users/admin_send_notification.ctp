  <body>

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage Users</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
                
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Notification Email
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                             <?php echo $this->Form->create('User'); ?>
           <form role="form" class="">
            <div class="form-group has-successs">
              <label class="control-label" for="inputSuccess">Competitors</label>
              <input class="form-control inner-radio" type="radio" name="data[type]" value="competitor">
            </div>
            <div class="form-group has-successs">
              <label class="control-label" for="inputSuccess">Braiders</label>
              <input class="form-control inner-radio" type="radio" name="data[type]" value="braider">
            </div>
            <div class="form-group has-successs">
              <label class="control-label" for="inputSuccess">All Users</label>
              <input class="form-control inner-radio" type="radio" name="data[type]" value="admin">
            </div>
            <div class="form-group has-successs">
              <label class="control-label" for="inputSuccess">Notification Text</label>
              <textarea class="form-control" rows="4" cols="50" name="textarea"></textarea>
            </div>
            <?php   echo $this->Form->submit('Send Notification',array('class' => 'btn btn-primary'));  ?>
          </form>
                            </div>
                           
                        </div>
                       
                    </div>
          
        </div>
</body>
<script>

$('input[type="radio"]').on('change', function() {
   $('input[type="radio"]').not(this).prop('checked', false);
});
</script>
</html>

