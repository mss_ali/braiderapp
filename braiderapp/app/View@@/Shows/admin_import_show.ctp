

        <div id="page-wrappers">
           <br></br>
            
                    <div class="panel panel-default">
                      
                        <div class="panel-heading">
                          Import Show
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                              <?php if($this->Session->check('error')){ ?>
                            <div class="response-msg error ui-corner-all">
                                <?php echo $this->Session->read('error');?>
                            </div>
                            <?php $this->Session->delete('error'); ?>
                        <?php } ?>
                        
                        <?php if($this->Session->check('success')){ ?>
                            <div class="response-msg success ui-corner-all" id="success">
                            <span>Success!</span>
                         <?php echo $this->Session->read('success');?>
                            </div>
                        <?php $this->Session->delete('success'); ?>
                        <?php } ?>
                            <div class="dataTable_wrapper">
                             <form  method="post" enctype="multipart/form-data" action="<?= HTTP_ROOT?>/admin/Shows/importShow">
                            
                            <form role="form">
                                <table class="" id="dataTables-example"> 
                                        <div class="form-group has-successs">
                                            <div class="fileUpload btn btn-primary">
                                                <span>Upload</span>
                                                <input id="uploadBtn" name="data[Show][file]" type="file" class="upload" />
                                            </div>
                                             <input id="uploadFile" placeholder="Choose File" disabled="disabled" />
                                        </div>
                                        <input class="import-btn" type="submit" value="Import">
                                </table> 
                            </form>
               
                    
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
              
           
           
        </div>


<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function () {
    document.getElementById("uploadFile").value = this.value;
    };
</script>


