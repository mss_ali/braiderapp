<body>

    <div id="wrapper">

        <div id="page-wrappers">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Manage User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-heading">
                       
 <?php  echo $this->Html->link('Back to Dashboard',array('controller'=>'Users','action'=>'admin_dashboard'), array('target'=>'_blank')); ?>
                        </div>
                        <div id="sub-nav">
  <div class="page-title">
    <h1>Product Details</h1>
  </div>
</div>
<div id="page-layout">
 <div class="loadPaginationContent">  
  <?php echo $this->element('adminElements/admins/product/product_list');?>
 </div> 
</div>
<div class="clear"></div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
<?php $paginator = $this->Paginator;
 ?>
                                 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                 <tr>
                            <?php
                                 echo "<th>" . $paginator->sort('State') . "</th>";
                                 echo "<th>" . $paginator->sort('Show') . "</th>";
                                  echo "<th>" . $paginator->sort('Braider Name') . "</th>";
                                 echo "<th>" . $paginator->sort('Competitor Name') . "</th>";
                                        ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                             <tr class="odd gradeX">
                            <?php
                                foreach( $reservation as $key=>$value )
                                        {?>

                                         <td><?php echo $value['State']['name']; ?></td>  
                                          <td><?php echo $value['Show']['showName']; ?></td> 
                                          <td><?php echo $value['Reservation']['braider']?></td> 
                                          <td><?php echo $value['Reservation']['competitor']?></td>
                                        </tr>
                                        
                                       <?php } ?>                                       
                                    </tbody>
                                </table>
                                 <?php  echo $this->Html->link('Download Listing',array('controller'=>'Shows','action'=>'export_reservation'), array('target'=>'_blank')); ?>

                           <?php
                         
                              echo $this->Paginator->counter(array(
                              'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                              ));
                              ?> 
                        </p>
                        <div class="paging">
                           <?php
                              echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                              echo $this->Paginator->numbers(array('separator' => ''));
                              echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                              ?>
                        </div>




                              
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
           
        </div>
        <!-- /#page-wrapper -->

    </div>
   

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
<html lang="en">
<head>
   
  
 
  <script>
  $(function() {
    $( ".datepicker" ).datepicker({ dateFormat : 'yy-mm-dd'});
  });
  </script>
  <script type="text/javascript">
        $(document).ready(function(){
         $('#filterby').on('click',function(){
          var isSelect = $('#select').val();
          var filtertext = $('#filtertext').val();
          if(isSelect == ''){
            alert('Please select the value');
            return false;
          } if(filtertext=='') {
            alert('Please Enter the '+ isSelect);
            return false;
          }
          if(isSelect != '' && filtertext != '')
          {
            $('#filterorder').submit();
          }

         });
        });
        </script>

</head>

</html>
