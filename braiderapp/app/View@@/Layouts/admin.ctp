<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Braider App');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<link rel="shortcut icon" href="<?php echo HTTP_ROOT?>img/favicon.ico" />
	<?php
		//echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	
	<?php 
	    	echo $this->Html->css('admin/bootstrap/bootstrap.min.css');
	    	//echo $this->Html->css('admin/bootstrap/align.css');
	    	echo $this->Html->css('admin/menu/dist/metisMenu.min.css');
	    	echo $this->Html->css('admin/timeline.css');
	    	echo $this->Html->css('admin/sb-admin-2.css');
	    	echo $this->Html->css('admin/ui.datepicker.css');
	    	echo $this->Html->css('admin/font-awesome/css/font-awesome.min.css');
	    	
	?>
    <?php  
          echo $this->Html->script('admin/jquery/jquery.min.js');
          echo $this->Html->script('admin/bootstrap/bootstrap.min.js');
          echo $this->Html->script('admin/menu/metisMenu.min.js');
          echo $this->Html->script('admin/sb-admin-2.js');
          echo $this->Html->script('admin/jquery-ui.js');
          echo $this->Html->script('jquery.validate.js');
          //echo $this->Html->script('admin/jquery-1.10.2.js');
        ?>

</head>
<body>
	<div id="wrapper">
		<?php echo $this->element('admin/nav'); ?>
	</div>
	<div id="page-wrapper">
    		<?php echo $content_for_layout ?>
    </div>

</body>
</html>
