var app = angular.module('ionicApp', ['ui.router','ui.calendar','ui.bootstrap','multipleDatePicker','ngSanitize','angularUtils.directives.dirPagination','ui.sortable']) ;

// app.constant("API", "http://localhost/projects/braider/braiderapp/braiderapp/");


app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      // .state('home', {
      //   url: '/home',
      //   templateUrl: "template/home.html",
      // controller: 'homeController'
      // })
      
      .state('login', {
        url: '/login',
        templateUrl: "template/login.html"	      
      })
      .state('reservation-list', {
        url: '/reservation-list',
        templateUrl: "template/reservation-list.html",
        controller: 'reservation'      
      })
      .state('detail', {
        url: '/detail',
        templateUrl: "template/compititor-reservation-detail.html"        
      })
      .state('register', {
        url: "/register",
        templateUrl: "template/register.html"
      })
      .state('dashboard', {
        url: "/dashboard",
        templateUrl: "template/dashboard.html",
        controller: 'DashboardController'
      })
      .state('calendar', {
        url: "/calendar",
        templateUrl: "template/calendar.html",
        controller: 'CalendarCtrl',
      })
      .state('my-profile', {
        url: "/my-profile",
        templateUrl: "template/my-profile.html",
        controller: 'DashboardController'
      })
      .state('braid-profile', {
        url: "/braid-profile",
        templateUrl: "template/braid_profile.html",
        controller: 'DashboardController'
      })
      .state('choose-date', {
        url: "/choose-date",
        templateUrl: "template/choose-date.html",
        controller: 'DashboardController'
      })
      .state('choose-a-braider', {
        url: "/choose-a-braider",
        templateUrl: "template/choose-a-braider.html",
        controller: 'DashboardController'
      })
      .state('select-service', {
        url: "/select-service",
        templateUrl: "template/select-service.html",
        controller: 'DashboardController'
      })
      .state('horses', {
        url: "/horses",
        templateUrl: "template/horses.html",
        controller: 'DashboardController'
      })
      .state('view-profile', {
        url: "/view-profile",
        templateUrl: "template/view-profile.html",
        controller: 'DashboardController'
      })
      .state('myhorses', {
        url: "/myhorses",
        templateUrl: "template/myhorses.html",
        controller: 'DashboardController'
      })
      .state('edit-horse', {
        url: "/edit-horse",
        templateUrl: "template/edit-horse.html",
        controller: 'DashboardController'
      })
      .state('add-horse', {
        url: "/add-horse",
        templateUrl: "template/add-horse.html",
        controller: 'DashboardController'
      })
      .state('logout', {
        url: "/logout",
        controller: 'logoutController'
      })
      .state('forgot-password', {
        url: "/forgot-password",
        templateUrl: "template/forgot-password.html",
        controller: 'DashboardController'
      })
      .state('braider_profile', {
        url: "/braider_profile",
	      templateUrl: "template/braider_profile.html",
        controller: 'DashboardController'
      })
      .state('my-services', {
        url: "/my-services",
        templateUrl: "template/my-service.html",
        controller: 'DashboardController'
      })
      .state('myreservation', {
        url: "/myreservation",
        templateUrl: "template/myreservation.html",
        controller: 'DashboardController'
      })
      .state('detail-reservation', {
        url: "/detail-reservation",
        templateUrl: "template/detail-reservation.html",
        controller: 'DashboardController'
      })
      .state('edit-reservation', {
        url: "/edit-reservation",
        templateUrl: "template/edit-reservation.html",
        controller: 'DashboardController'
      })
      .state('reservation-detail', {
        url: "/reservation-detail",
        templateUrl: "template/reservation-detail.html",
        controller: 'DashboardController'
      })
      .state('add-service', {
        url: "/add-service",
        templateUrl: "template/add-service.html",
        controller: 'DashboardController'
      })
      .state('show-schedule', {
        url: "/show-schedule",
        templateUrl: "template/show-schedule.html",
        controller: 'DashboardController'
      })
      .state('add-show', {
        url: "/add-show",
        templateUrl: "template/add-show.html",
        controller: 'DashboardController'

      })
      .state('edit-show', {
        url: "/edit-show",
        templateUrl: "template/edit-show.html",
        controller: 'DashboardController'

      })
    /*  .state('404', {
        url: "/404",
        templateUrl: "template/404err.html",
        controller: 'errorController'
      })*/
      .state('braider-portfolio', {
        url: "/braider-portfolio",
        templateUrl: "template/portfolio.html",
        controller: 'braiderportfolioController'

      });
   $urlRouterProvider.otherwise('/login');
    
  });



app.constant("API", "http://mastersoftwaretechnologies.com/braider/braiderapp/");


app.run(['$rootScope', 'store','$window', function($rootScope, store,$window) {
 
 
/*$rootScope.$on('$routeUpdate', function(scope, next, current) {
   $('.navbar-toggle').trigger('click');
});*/
if(angular.isObject(store.get('userdata'))) {
  $rootScope.currentUser = store.get('userdata');  
}   

 // $window.localStorage.clear()
 $rootScope = {

 
                     userdata : []
                }; 
        
    
}]);

app.controller('errorController', ['$rootScope',function($rootScope){
    $rootScope.layoutHeader  = false ;
    $rootScope.layoutFooter  = false ;
}]);

app.controller('authCtrl',['$window','$scope','$rootScope','$state', '$stateParams','store','$location','$timeout',function($window,$scope,$rootScope,$state, $stateParams,store,$location,$timeout){
   $rootScope.layoutHeader  = true ;
   $rootScope.layoutFooter  = true ;
    $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
    
              $('.btnclosedonclick').click(function(){
               if( $( '.btnclosedonclick' ).hasClass( 'alreadycliCk' ) ){
                $('.btnclosedonclick').removeClass('alreadycliCk');
                $('.btnclosedonclick').trigger('click');

              }else{
               $('.btnclosedonclick').addClass('alreadycliCk');
              }
              
              });


    $rootScope.toState = toState;
       var url = $rootScope.toState;
        
       if(angular.isObject(store.get('userdata'))){
        var userrole =  store.get('userdata')
        if(userrole.User != undefined){
        if(userrole.User.access_token == ''){
            $location.path('/login');
        }else{
          if(url.url == "/login"){
            $timeout( function(){ $window.location.reload();});
            $location.path('/dashboard');
            
          }else if(url.url == "/logout"){
            $location.path('/login');
          }
        }
      }
       
      }else{

        if(url.url == "/termsandcondition"){
         $location.path('/termsandcondition');
        }else if(url.url == "/register"){
         $location.path('/register'); 
        }
         else if(url.url == "/logout"){
         $location.path('/login'); 
        }
         else if(url.url == "/faq"){
         $location.path('/faq'); 
        }
        else{
         $location.path('/login');
         } 

        //$location.path('/login'); 

      }
    });
 }]);


//Array of images which you want to show: Use path you want.
// var images=new Array('../img/login-bg.png','../img/shedule-horse-1.png');
// var nextimage=0;
// doSlideshow();
// function doSlideshow(){
//     if(nextimage>=images.length){nextimage=0;}
//     $('#inner-login')
//     .css('background','url("'+images[nextimage++]+'")')
//     .fadeIn(500,function(){
//         setTimeout(doSlideshow,1000);
//     });
// }

