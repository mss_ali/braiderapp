app.factory("AllService", ["$http", '$q', 'API',
                           function($http, $q, API) {
    return {
        //for login
        login: function(data) {
            var url = API + "users/login"
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: {
                        email : data.email,
                        password : data.password,
                    },
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        //for create an account
        register: function(data) {console.log('dataaa', data)
            var url = API + "users/register"
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: data
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        //for logged in user profile
        getProfile: function(data) {
            var url = API + "users/getProfile/"+data.User.access_token
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        // update profile
        updateProfile: function(entity) {
            var url = API + "users/updateProfile";
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: entity
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //add horse
        horse: function(data) {
            var url = API + "horses/addhorse"
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: data
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //get competitor horses
        getHorses: function(data) {
            var url = API + "horses/getCHorse/"+data.User.access_token
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //get single horse
        getOne: function(data, h_id) {
            var url = API + "horses/getHorse/"+data.User.access_token+"/"+h_id
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //update horse
        update: function(data) {
            var url = API + "horses/updateHorse"
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: data
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //get listing of braiders
        all: function(data) {
            var url = API + "users/getBraiders"
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //get single braider data
        getOneBraider: function(data, b_id) {
            var url = API + "users/getBraiderProfile"+"/"+b_id
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //for braider shows listing
        shows: function(data) {
            var url = API + "shows/showsList"
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //get show schedule
        showsSchedule: function(data) {
            var url = API + "shows/getshowSchedule/"+data.User.access_token
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        

        myReservation: function(data) {
            var url = API + "service/getbraiderReservations/"+data.User.access_token
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        braiderReservation: function(data, reserveId) {
            var url = API + "service/getbraiderReservations/"+data.User.access_token+"/"+reserveId
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        acceptReject: function(data, reserveId, b_id, value) {
            var url = API + "service/accept_reject_reservation_braider/"+reserveId+"/"+b_id+"/"+value
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //listing of shows according to month
        monthshows: function(data, month) {
            var url = API + "shows/showsList/"+month
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        //listing of date in between start and end date
        dates: function(data, startDate, endDate) {
            var url = API + "shows/GetDays/"+startDate+"/"+endDate
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        //listing of available braider
        avail_braider: function(data, show_id, selectedDate) {
            var url = API + "users/showBraiders/"+show_id+"/"+selectedDate
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //add services by braider
        add: function(data) {
            var url = API+"service/addService"
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: data
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data);
            });
            return defer.promise;
        },

        //listing of braider services
        all: function(data) {
            var url = API + "service/getBraiderServices/"+data.User.id
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //listing of braider services
        braidService: function(data, b_id) {
            var url = API + "service/getBraiderServices/"+b_id
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //for competitor schedule reservations
        getReservation: function(data) {
            var url = API + "service/getcompeteterReservations/"+data.User.access_token
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        //for get single reservations
        getSingleReservation: function(data, reservation_id) {
            var url = API + "service/getcompeteterReservations/"+data.User.access_token+"/"+reservation_id
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },

        cancelReservation: function(data, reservation_id, compid) {
            var url = API+"service/cancel_reservation_by_competetior/"+reservation_id+"/"+compid
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data);
            });
            return defer.promise;
        },

        //for create reservation
        createReservation: function(data) {
            var url = API+"users/reservations"
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: data
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data);
            });
            return defer.promise;
        },

        getState: function(data) {
            var url = API + "service/getstate"
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        updateService: function(tabldedata,access_token){
            
            return $http({
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: API+'service/updateService',
                         data: { tabldedata,access_token}
                    }); 

         },

        
    }
}]);

app.factory('Braiderportfolio', ['$http','API',
 function ($http,API) 
    {
      return {
               getPortfolio: function (access_token) 
               {
                    var promise = $http.get(API + 'users/getPortfolio/' + access_token) .then(function(response) {
                    return response.data;
                    }, function (error) {
                    })
                   return promise;
               },
           
               deleteWorkPicture:function(access_token,port_id)
               {
                   var promise = $http.get(API + 'users/deleteWorkPicture/' + access_token +'/'+ port_id) .then(function(response) {
                    return response.data;
                    }, function (error) {
                    })
                   return promise;
             },
             addWorkPicture : function(image,access_token)
             {

                   return $http({
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: API+'users/addWorkPicture',
                         data: { image,access_token}
                    }); 

             },
             addShowByBraider : function(addshowbraiderdata)
             {

                   return $http({
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: API+'shows/addShow',
                         data: { addshowbraiderdata}
                    }); 

             }


         }
    } 
       
   ]);
