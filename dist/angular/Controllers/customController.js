app.controller('homeController',['$scope','$http', '$state', '$rootScope', 'AllService','store', function($scope,$http, $state, $rootScope, AllService, store){
//console.log('store', store)
    // $("#bs-navbar a").click(function () {
    //     setTimeout(function(){
    //         $('[data-toggle = collapse]').click()
    //     }, 100);
    // });
    
	$scope.items = {};
    $scope.openLogin = function() {
        $scope.login = function() {
         
            return AllService.login($scope.items);
        }
        $scope.response = $scope.login()
        $scope.response.then(function(userdata){
            //console.log('userdata',userdata);
        	store.set('userdata', JSON.stringify(userdata));
        	 
        	$scope.user = store.get('userdata');
        	
        	if($scope.user.status == 'error') {
        		$scope.error = 'Email and Password is Required!';
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                if($scope.user.message == 'Invalid username or password') {
                    $scope.error = 'Invalid credentials given.';
                    $scope.items.password = '';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
                if($scope.user.message == 'email is required.') {
                    $scope.error = 'Sorry, it looks like your email is missing.';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
                if($scope.user.message == 'password is required.') {
                    $scope.error = 'Sorry, it looks like your password is missing.';
        		    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
                if($scope.user.message == 'suspended') {
                    $scope.error = 'Your account has been temporarily suspended.';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
        	}else {
        		$rootScope.currentUser = $scope.user.User;
                $state.go('dashboard');
        	}
            
        },function(error) {
            $scope.error = 'Invalid credentials given.';
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        });                
    };

    $scope.register = {};
    $scope.emailmsg = false;
    $scope.openRegister = function() {
        $scope.createAccount = function() {
            if($rootScope.token_ios){
            $scope.register.device_id = $rootScope.token_ios;
            $scope.register.device_type = 'iphone';
            }
            return AllService.register($scope.register);
        }                
        // console.log('data',$scope.register);
        $scope.response = $scope.createAccount()
        $scope.response.then(function(data){
           //console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                
                if(data.errors == "The email address you have entered is already registered with us. Please log into your existing account. Or, if you think this is an error, please contact us") {
                    $scope.text = 'text';
                    setTimeout(function() {
                    $scope.text = '';
                    $scope.$apply();
                    }, 5000);
                }
                if(data.errors == 'Sorry, your password needs to be a minimum of 6 characters') {
                    $scope.msg = 'Sorry, your password needs to be a minimum of 6 characters.';
                    setTimeout(function() {
                    $scope.msg = '';
                    $scope.$apply();
                    }, 5000);
                }
                if(data.errors == 'passwords do not match') {
                    $scope.msg = 'Sorry, your password and confirm password does not match.';
                    setTimeout(function() {
                    $scope.msg = '';
                    $scope.$apply();
                    }, 5000);
                }
                $scope.error = data.message;
               
                setTimeout(function() {
                    $scope.error = '';
                    $scope.errors = '';
                    $scope.$apply();
                }, 8000); 
                
            } else {                
                 $scope.login = function() {

                 return AllService.login($scope.register);
            }
                $scope.response = $scope.login()
                $scope.response.then(function(userdata){
                   // console.log('userdata',userdata);
                    store.set('userdata', JSON.stringify(userdata));
                     
                    $scope.user = store.get('userdata');
                    
                    if($scope.user.status == 'error') {
                        $scope.error = $scope.user.message;
                        if($scope.error == 'email is required.') {
                            $scope.error = 'Sorry, it looks like your email is missing.';
                            setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                            }, 5000);
                        }
                        if($scope.error == 'password is required.') {
                            $scope.error = 'Sorry, it looks like your password is missing.';
                            setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                            }, 5000);
                        }
                        
                    }else {
                        $rootScope.currentUser = $scope.user.User;
                        $state.go('dashboard');
                        $rootScope.accountCreated = 'Account has been successfully created.';
                        setTimeout(function() {
                            $rootScope.accountCreated = '';
                            $rootScope.$apply();
                            }, 5000);
                    }
                    
                },function(error) {
                    $scope.error = 'Invalid credentials given.';
                    setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                        }, 5000);
                });  
                        
                    }                    
                },function(error)  {
                    $scope.error = "Invalid credentials.";
                    setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                        }, 5000);

                });                
            };


    $scope.termsandcondition = function() { console.log('here')
        
        $rootScope.condition = AllService.termsCondition()
        $rootScope.condition.then(function(termsCondition) {//console.log('termsCondition', termsCondition)
            $rootScope.termsCondition = termsCondition.message;
               //console.log('$rootScope.termsCondition', $rootScope.termsCondition)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    };




}]);

app.controller('DashboardController',['$scope','$http', '$state', '$rootScope', 'AllService','store','Braiderportfolio','$timeout', '$window','API', '$location',  function($scope,$http, $state, $rootScope, AllService, store,Braiderportfolio,$timeout, $window, API, $location){

    // $("#bs-navbar a").click(function () {
    //     setTimeout(function(){
    //         $('[data-toggle = collapse]').click()
    //     }, 100);
    // });

    // console.log('store.get', store.get('userdata'))
    // for (var obj in store.get('userdata')) {
    //     console.log('obj', obj.User)
        

    // }
    if(angular.isObject(store.get('userdata'))) {
        $rootScope.currentUser = store.get('userdata');
         if($rootScope.currentUser.User != undefined) 
        $scope.userRole = $rootScope.currentUser.User.role;
        
    }

    //get my Reservation
     $scope.braiderReservation = function() {
        $rootScope.reservation = AllService.myReservation($rootScope.currentUser)
        $rootScope.reservation.then(function(response) {
            $rootScope.reservations = response.braidereservations;
            for(var i=0; i<$rootScope.reservations.length; i++) {

            if($rootScope.reservations[i].status == 'confirm') {
                $rootScope.reservations[i].status = 'Confirmed';
            }
            }
            $rootScope.length = $rootScope.reservations.length;

            var pagesShown = 1;
            var pageSize = 3;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.reservations.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);

        });
     }   
     //get profile
    $scope.profile = function() {
        
        $rootScope.user = AllService.getProfile($rootScope.currentUser)
        $rootScope.user.then(function(profile) {
            $scope.userProfile = profile.User;
            
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);

        });
    }

    //for update profile
    $scope.update = function() {
        var arr = {};
        arr.id = $rootScope.currentUser.User.id,
        arr.access_token = $rootScope.currentUser.User.access_token,
        arr.name = $scope.userProfile.name,
        arr.email = $scope.userProfile.email,
        arr.phone = $scope.userProfile.phone,
        arr.password = $scope.userProfile.password,
        arr.cpassword = $scope.userProfile.cpassword
        //console.log('arr', arr)      
            
       $scope.updateData = function() {
            return AllService.updateProfile(arr);
        }                
        $scope.response = $scope.updateData()
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
                if(data.errors == 'Email is already exist.') {
                    $scope.sameEmail = 'Email already exists!';
                    setTimeout(function() {
                    $scope.sameEmail = '';
                    $scope.$apply();
                }, 5000); 
                } 
            } else {
                $rootScope.updateP = data.message; 

                store.set('userdata', JSON.stringify(data));
                $rootScope.currentUser = store.get('userdata');
                $state.go('dashboard'); 
                $rootScope.user = AllService.getProfile($rootScope.currentUser)
                $rootScope.user.then(function(profile) {//console.log('profile', profile);
                    $scope.userProfile = profile.User;
                    
                }, function(error) {

                    $scope.error = error;
                    setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                        }, 5000);

                });
                setTimeout(function() {
                    $rootScope.updateP = '';
                    $rootScope.$apply();
                }, 5000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        });                
            
    };


    //for add new horse
    $scope.horse ={};
    $scope.preview = '';
    $scope.addHorse = function() {
        $scope.loader = true;
        $scope.horse.access_token = $rootScope.currentUser.User.access_token;
        $scope.horse.image = this.fileData;
       $scope.newHorse = function() {
            return AllService.horse($scope.horse);
        }                
        $scope.response = $scope.newHorse()
        $scope.response.then(function(data){
           // console.log('dataaaa', data)
           
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);   
            } else {
               $rootScope.message = data.message; 
               $state.go('myhorses');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
                
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
                
        });                
            
    } 
    
    
 
    //upload image
  
   $scope.imageuploadbtn = true;
    $scope.showError = false;
    $scope.fileUpload = function(){
        $scope.fileData = null;
        var f = document.getElementById('3d_file').files[0];
        var size = f.size;
        $scope.size = size;
        if(size > 2000000){
             f = null;
          
          $scope.showError = 'Sorry, but that image is too large to upload. Please only upload images 2mb or smaller.';
          $timeout(function() {
          $scope.showError = '';
          }, 2000)
         }else{
         $scope.imageuploadbtn = false;
        }
        if(f) {
        r = new FileReader();
        $scope.filename = f.name;
        $scope.showImage = false;
        $scope.$apply();
        r.onload = function(e){
            $scope.fileData = e.target.result;
        document.getElementById('abc').src= e.target.result ;
        $scope.image = e.target.result; 
        $scope.$apply();
        };

             r.readAsDataURL(f);
        }
         
    };


    $scope.faq3 = function() {
        
            $rootScope.faq1 = AllService.faq($rootScope.currentUser)
            $rootScope.faq1.then(function(faq) { 

                $rootScope.faq2 = faq.message.CmsPage;
                 
            });
        }

    $scope.deleteHorse = function(horseId) {
        var Confirm = confirm("Are you sure you want to delete horse?");
        if (Confirm == true) {
            $rootScope.horse_id = horseId;
            $rootScope.del = AllService.delHorse($rootScope.currentUser, $rootScope.horse_id)
            $rootScope.del.then(function(deleteHorse) {
                $rootScope.deleteHorse = deleteHorse.message;
                    setTimeout(function() {
                        $rootScope.deleteHorse = '';
                        $rootScope.$apply();
                    }, 5000);

            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });

    

        }
        $window.location.reload();
    }



    $scope.faq = function() {
        
            $rootScope.faq = AllService.faq($rootScope.currentUser)
            $rootScope.faq.then(function(faq) {
                $rootScope.faq = faq.message.CmsPage;
                   
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });
    }

    $scope.redirect = function() {
        $state.go('horses');
    }

    //get listing of horses
    $rootScope.noHorse = false;
    $scope.horseListing = function() {
        $rootScope.horses = AllService.getHorses($rootScope.currentUser)
        $rootScope.horses.then(function(horses) {
            $rootScope.horseList = horses.horses;
           
            setTimeout(function(){ 
                if($window.sessionStorage["horseInfo"]!=undefined){
                    $rootScope.horseVal =  JSON.parse($window.sessionStorage["horseInfo"]);
                    if($rootScope.horseVal.horseId!=undefined){ 
                        $("#"+$rootScope.horseVal.horseId+"_selected").show();
                        $("#"+$rootScope.horseVal.horseId+"_select").hide();
                    }
                }
             }, 100);
           
           // $rootScope.horseLength = $rootScope.horseList.length;
            if(horses.status == 'error'){
                $rootScope.noHorse = true;
               $rootScope.noHorse = 'No Horse added yet!';
            }else {
                
                var pagesShown = 1;
                var pageSize = 6;
                
                $scope.paginationLimit = function(data) {
                    return pageSize * pagesShown;
                };
                $scope.hasMoreItemsToShow = function() {
                    return pagesShown < ($rootScope.horseList.length / pageSize);
                };
                $scope.showMoreItems = function() {
                    pagesShown = pagesShown + 1;       
                };  
            }
        }, function(error) {

            $scope.error = error;

        });
    }

    //for selected horse value
    $scope.setHorse = function(horse) {
        $scope.chooseHorse = horse;
    }
    $rootScope.horse_select = true;
   $scope.singleHorse = function(horse_id, event) {

      $rootScope.horseId = horse_id.id;
        if(event != undefined) {
            $rootScope.horsecheckId = event.target.id == $rootScope.horseId  ? 'Selected' : 'Select';
               
           
            if(event.target.id==horse_id.id+'_select'){
                $(".select").show();
                $(".selected").hide();
                $("#"+event.target.id).hide();
                $("#"+horse_id.id+'_selected').show();
            }else if(event.target.id==horse_id.id+'_selected'){
                $(".select").hide();
                $(".selected").show();
                 $("#"+event.target.id).hide();
                $("#"+horse_id.id+'_select').show();
            }
        }

        var horse = {
            "horseId" : $rootScope.horseId
        }
        $window.sessionStorage["horseInfo"] = JSON.stringify(horse);

      localStorage["horseid"] = JSON.stringify(horse_id.id);
            $rootScope.horseOne = AllService.getOne($rootScope.currentUser, $rootScope.horseId)
            $rootScope.horseOne.then(function(horse) {//console.log('horses', horse)
            
            $rootScope.horseSingle = horse.horse;
            
            
            }, function(error) {

                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

            });
    }

    $scope.backHorse = function(horse_id, event) {
        $state.go('horses');
       
    
        $rootScope.horseVal =  JSON.parse($window.sessionStorage["horseInfo"]);
        $scope.textChange = true;
        
            if(event.target.id==horse_id+'_select'){
            $(".select").show();
            $(".selected").hide();
            $("#"+event.target.id).hide();
            $("#"+horse_id+'_selected').show();
        }else if(event.target.id==horse_id+'_selected'){
            $(".select").hide();
            $(".selected").show();
             $("#"+event.target.id).hide();
            $("#"+horse_id+'_select').show();
        }
    }
    
    //for update horse detail
    $scope.updateDetail = function() {
        $scope.horseSingle.image = this.fileData;
        var arr = {};
        arr.access_token = $rootScope.currentUser.User.access_token,
        arr.id = $scope.horseSingle.id,
        arr.name = $scope.horseSingle.name,
        arr.gender = $scope.horseSingle.gender,
        arr.markings = $scope.horseSingle.markings,
        arr.color = $scope.horseSingle.color,
        arr.height = $scope.horseSingle.height,
        arr.quirks = $scope.horseSingle.quirks,
        arr.image = $scope.horseSingle.image
        
        $scope.updateData = function() {
            return AllService.update(arr);
        }                
        $scope.response = $scope.updateData()
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);   
            } else {
               $rootScope.message = data.message;
               $state.go('myhorses');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });         
    }

    //for listing of braider
    $scope.braiderListing = function() {

        $rootScope.braider = AllService.all($rootScope.currentUser)
        $rootScope.braider.then(function(braiders) {
            $rootScope.braiderList = braiders.braiders.User;
        }, function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    //for selected service
  $scope.checkItem = function (id) {
   var checked = false;
   for(var i=0; i<$rootScope.serviceArray.length; i++) {
   if(parseInt(id) == parseInt($rootScope.serviceArray[i])) {
      checked = true;
      }
    }
    return checked;
  };

     
   

   $scope.brader = function(){
    $rootScope.array = [];
        if($window.sessionStorage["serviceInfo"]!= undefined){
            var check_val =  JSON.parse($window.sessionStorage["serviceInfo"]);
            var arr = check_val.arrayService;
            $rootScope.array.concat(arr);
            $rootScope.array = check_val.arrayService;
            $rootScope.notSelected = true;
            
        }
    }

   $rootScope.notSelected = false;
      
    $scope.selectedServices = function($event, arr) {
       // $rootScope.abc = "true";
       
       
        //console.log("the new array count", $rootScope.array.length , $rootScope.array);
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        
        
        if(action == 'add') {
            if($rootScope.array.indexOf(arr) < 0){
                $rootScope.array.push(arr);
            }
            $rootScope.notSelected = true;
             
            
        }else if(action == 'remove') {
            //var id = arr
            var i = $rootScope.array.indexOf(arr);
            if(i != -1) {
                $rootScope.array.splice(i, 1);
                //console.log('$scope.array', $rootScope.array)
            }
            //$rootScope.serviceArray = $scope.array;
        }

        $rootScope.serviceArray = $rootScope.array; 

        var service = {
            "arrayService" : $rootScope.array
        }
        $window.sessionStorage["serviceInfo"] = JSON.stringify(service);
    } 

    
    $scope.backService = function(oldData) {
        $state.go('select-service');
        // console.log("comes in the backService", oldData.length , oldData);
        // $rootScope.editService =  JSON.parse($window.sessionStorage["serviceInfo"]);
        
   
        // if($rootScope.editService.arrayService != null ){
        //     $rootScope.array = $rootScope.editService.arrayService ;     
        // }
        
        
        // console.log('$rootScope.editService', $rootScope.editService);
       // $scope.selectedServices();
    }

   


    //for selected braider
    $scope.setBraider = function(braid, braiderAvailId) {

        sessionStorage.removeItem('serviceInfo');
        sessionStorage.removeItem('horseInfo');
        $rootScope.braider_id = braid;
        $rootScope.braiderAvailableId = braiderAvailId;
        $scope.selectedBraidServices = AllService.braidService($rootScope.currentUser, $rootScope.braider_id)
        $scope.selectedBraidServices.then(function(data){
            $rootScope.braidServices = data.services;
            
             
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
        
    }
    var serviceArray = [];
    var price = 0;
    
    $scope.viewProfile = function() {
        //console.log('$scope.serviceArray', $rootScope.serviceArray, '$rootScope.braidServices', $rootScope.braidServices)
        for(var j=0; j<$rootScope.serviceArray.length; j++) {

            //console.log('$rootScope.serviceArray', $rootScope.serviceArray[j])
            $rootScope.servicesArray = $rootScope.serviceArray[j];

            for(var i=0; i<$rootScope.braidServices.length; i++) {

                //console.log('$rootScope.braidServices', $rootScope.braidServices[i].id)
                $rootScope.braidServicesArray = $rootScope.braidServices[i].id

                if($rootScope.servicesArray == $rootScope.braidServicesArray) {

                    //console.log('here', $rootScope.servicesArray, '===', $rootScope.braidServicesArray)
                    serviceArray.push($rootScope.braidServices[i]);
                    $rootScope.braiderServicesArray = serviceArray;
                    
                }
            }
        }
            for(var k=0; k<$rootScope.braiderServicesArray.length; k++) {
                // console.log(window.Math.round(price + ($rootScope.braiderServicesArray[k].price)));
                // price = parseInt(price) + parseInt($rootScope.braiderServicesArray[k].price);
                price = window.Math.round(price *100)/100 + window.Math.round($rootScope.braiderServicesArray[k].price *100)/100;

                $rootScope.total_price = price;
            }
    }

    //for listing of braider
    $scope.showListing = function() {
    
        $rootScope.shows = AllService.shows($rootScope.currentUser)
        $rootScope.shows.then(function(allshows) {
            $rootScope.showList = allshows.shows;
            //console.log('shows', $rootScope.showList)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    //for show schedule
     $scope.showSchedule = function() {
       $rootScope.showsSchedule = AllService.showsSchedule($rootScope.currentUser)
        $rootScope.showsSchedule.then(function(response) {
            $rootScope.showsSchedule = response.scheduleShow;
            
            //view more functionality
            var pagesShown = 1;
            var pageSize = 8;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.showsSchedule.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };  
        }, function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }


    //for selected show
    var date = [];
    $scope.setShow = function(show) {
        $rootScope.allshows = show;
        $scope.alldate = '';
        $rootScope.nextDate = '';
        $rootScope.allShowsDate = '';
        if($rootScope.backButton != undefined){
            $rootScope.backButton.editDate = '';
        }
        $rootScope.buttonEnable = false;

        $rootScope.dateArray = $scope.selection.splice(0, $scope.selection.length);
        
         if($rootScope.monthshowList) {

            for(var i=0; i<$rootScope.monthshowList.length; i++) {
                
                if($scope.allshows == $rootScope.monthshowList[i].id) {
                     
                    $scope.startDate = $rootScope.monthshowList[i].startDate;
                    $scope.endDate = $rootScope.monthshowList[i].endDate;
                    
                }
                
            }
            var split = $scope.startDate.split(" ");
            var start = split[0];

            var split1 = $scope.endDate.split(" ");
            var end =  split1[0];
           
            $scope.selectedDates = AllService.dates($rootScope.currentUser, start, end)

            $scope.selectedDates.then(function(data){ 

            $rootScope.nextDate = data.dates;
            $rootScope.allShowsDate = data.dates;
            
            $rootScope.braidershowseleteddate = data.dates;

                $rootScope.nextDate = data.dates;
                 

                if($rootScope.nextDate){
                    // for(var i=0; i<$rootScope.nextDate.length; i++) {
                        
                    //     var d = new Date($rootScope.nextDate[i]);
                    //     var n = d.getTime(); 
                        
                        
                    //     date.push(n);

                    // }
                        $rootScope.allShowsDate =  data.dates;
                        $rootScope.braidershowseleteddate = date;

            
                }


            },function(error) {
                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            });
                    
          }
    }

        $scope.selection=[];
        $scope.datetoggleSelection = function toggleSelection(employeeName) {
        var idx = $scope.selection.indexOf(employeeName);
        if (idx > -1) {
        $scope.selection.splice(idx, 1);
       }

     else {

           $scope.selection.push(employeeName);

         }

       };


     $scope.removeselectedHorse = function(h_id){
     
      localStorage.removeItem('horseid');

  }
    $scope.singleHorsePopup = function(horse, event){
        $scope.disableHorseSave = false;
         
        $rootScope.horseId1 = horse.id;

        if(event.target.id==horse.id+'_selected'){
            $(".select").hide();
            $(".selected").show();
             $("#"+event.target.id).hide();
            $("#"+horse.id+'_select').show();
        }else if(event.target.id==horse.id+'_select'){
            $(".select").show();
            $(".selected").hide();
            $("#"+event.target.id).hide();
            $("#"+horse.id+'_selected').show();
        }
    
    }
    $rootScope.finaldate = false;
    $scope.setNewDate = function(date){
    $scope.disableSave = false;
   $scope.availBraider = AllService.avail_braider($rootScope.currentUser, $rootScope.allshows,date)

        $scope.availBraider.then(function(data){ 
        $rootScope.braiderAvailable = data.braiders;
        if($rootScope.braiderAvailable){
            $rootScope.dropSelectedDateId1 =  date;
            $rootScope.finaldate = false;
        }
        else if(!$rootScope.braiderAvailable) {
            $rootScope.finaldate = true;
        $rootScope.noBraider = 'No Braider available for this date. Choose another date.';
        setTimeout(function() {
            $rootScope.noBraider = '';
            $rootScope.$apply();
             }, 5000);
          } 
     if(data.status == 'error'){

                $scope.error = data.errors;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            }
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
      
      } 
       
     
    $rootScope.abc = "false";
    $scope.setDate = function(date) {
         $rootScope.dropSelectedStateId = $scope.allstate;
         $rootScope.dropSelectedMonthId = $scope.allmonth;
         $rootScope.dropSelectedShowsId = $scope.allshows; 
         $rootScope.dropSelectedDateId =  $scope.alldate; 
         
        $rootScope.abc= "true";
        var service= [];
        $rootScope.date = date;
        
        $scope.availBraider = AllService.avail_braider($rootScope.currentUser, $rootScope.allshows, $scope.date)

        $scope.availBraider.then(function(data){ 
        $rootScope.braiderAvailable = data.braiders;
        $rootScope.buttonEnable = false;
        $rootScope.monthfalse = true;
        
        
        if(!$rootScope.braiderAvailable) {
        $rootScope.noBraider = 'No Braider available for this date. Choose another date.';
        setTimeout(function() {
            $rootScope.noBraider = '';
            $rootScope.$apply();
             }, 5000);
          }

        if(data.status == 'error'){

                $scope.error = data.errors;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            }
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });

        var user = {
            "editState": $rootScope.state,
            "editMonth": $rootScope.month,
            "editShow": $rootScope.allshows,
            "editDate": $rootScope.date,
        }
        $window.sessionStorage["dateInfo"] = JSON.stringify(user);
    }



    //for show selected braid profile
    $scope.showProfile = function(braidId) {
        var braider_id = braidId;
        
            $rootScope.braiderOne = AllService.getOneBraider($rootScope.currentUser, braider_id)
            $rootScope.braiderOne.then(function(braider) {
            
            $rootScope.braiderSingle = braider.braider;
            $rootScope.callApi = API;
            
            }, function(error) {

                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 


            });
    }

    //for competitor schedule reservation
    $scope.competitorReservation = function() {
    
        $rootScope.shows = AllService.getReservation($rootScope.currentUser);
        $rootScope.shows.then(function(reservation) {
            $rootScope.reservations = reservation.competeterreservations;
            if($rootScope.reservations[0]){
            $rootScope.compet_id = $rootScope.reservations[0].compid;
            }
            $rootScope.completed_show = AllService.completedReservation($rootScope.compet_id);
            $rootScope.completed_show.then(function(completed) {
                if(completed.status == 'success'){
            $rootScope.completed_res =  completed.reservationlist;
            $rootScope.completelength = $rootScope.completed_res.length;
        }else{
            $rootScope.completelength == completed.msg;
        }
        });
            for(var i=0; i<$rootScope.reservations.length; i++) {

            if($rootScope.reservations[i].status == 'Confirmed') {//console.log('status', $rootScope.reservations[i].status)
                $rootScope.reservations[i].status = 'Confirmed';
            }
            if($rootScope.reservations[i].status == 'Declined') {//console.log('status', $rootScope.reservations[i].status)
                $rootScope.reservations[i].status = 'Canceled';
            }
             
            }
            if($rootScope.reservations.length){
            $rootScope.reserveLength = $rootScope.reservations.length;
            }
            var pagesShown = 1;
            var pageSize = 6;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.reservations.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };
             var pagesShown_comp = 1;
            var pageSize_comp = 6;
            $scope.paginationLimit1 = function(data) {
                return pageSize_comp * pagesShown_comp;
            };
            $scope.hasMoreItemsToShow1 = function() {
                return pagesShown_comp < ($rootScope.completelength / pageSize_comp);
            };
            $scope.showMoreItems1 = function() {
                pagesShown_comp  = pagesShown_comp + 1;       
            };

        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    //for single reservation
   /* $scope.competitorSingleReservation = function(reserveId) {
        $rootScope.reservationId = reserveId.reservation_id;
        
        $rootScope.shows = AllService.getSingleReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.shows.then(function(reservation) {//console.log('Singlereservation', reservation)
            $rootScope.Singlereservation = reservation.competeterreservations;
            
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    } */
 $scope.single = function(msg,id){
        $rootScope.pop_msg = msg;
        $scope.msg = $rootScope.pop_msg.msgid
        $scope.read_msg = AllService.readmsg($scope.msg)
         $scope.messages_list = AllService.competitorMessage(id);
        $scope.messages_list.then(function(message){
                $rootScope.Allmessage = message;
        });
        $scope.read_msg.then(function(msg) {
            //any use !
        });
    }

    //for single reservation
    $scope.competitorSingleReservation = function(reserveId) {
        $location.path('/detail-reservation');
        $rootScope.reservationId = reserveId;
        $rootScope.shows = AllService.getSingleReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.shows.then(function(reservation) {//console.log('Singlereservation', reservation)
            $rootScope.Singlereservation = reservation.competeterreservations;
            $scope.braider_id = reservation.competeterreservations[0].braider_id;
            $scope.comp_id =reservation.competeterreservations[0].compid;
            $rootScope.res1 =reservation.competeterreservations[0].res_number;
        $scope.messages_list = AllService.competitorMessage($rootScope.reservationId);
            $scope.messages_list.then(function(message){
                $rootScope.Allmessage = message;
    });
            
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    $scope.cancelReservation = function(data,datas) {   
        var Confirm = confirm("Are you sure you want to cancel your reservation?");
        if (Confirm == true) {

                   $rootScope.cancelled  = datas; 
                   $rootScope.reserve_Id = data.reservation_id;
                   $rootScope.competitor_Id = data.compid;
             

            $rootScope.cancel = AllService.cancelReservation($rootScope.currentUser, $rootScope.reserve_Id, $rootScope.competitor_Id, $rootScope.cancelled)
            $rootScope.cancel.then(function(cancelReservation) {//console.log('cancelReservation', cancelReservation)
                // $rootScope.cancelReservation = reservation.competeterreservations;
                // console.log('reservations', $rootScope.Singlereservation)
                $rootScope.cancel = 'Reservation has been cancelled!';
                //console.log('cancel', $rootScope.cancel);
                   $state.go('dashboard');
                    setTimeout(function() {
                        $rootScope.cancel = '';
                        $rootScope.$apply();
                    }, 5000);
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });
        }

    }

    // $scope.braiderSingleReservation = function(reservationId) {
    //     $rootScope.reservationId = reservationId.reservation_id;
    //     $rootScope.reservation = AllService.braiderReservation($rootScope.currentUser, $rootScope.reservationId)
    //     $rootScope.reservation.then(function(reservation) {
    //         console.log(reservation);
    //         $rootScope.reservationSingle = reservation.braidereservations;
    //         //console.log('reservations', $rootScope.reservationSingle)
    //     }, function(error) {

    //         $scope.error = error;
    //             setTimeout(function() {
    //                 $scope.error = '';
    //                 $scope.$apply();
    //             }, 5000); 

    //     });

    // }

    $scope.acceptRejectReservation = function(data, value) {
      // console.log('data', data, 'value', value);
         $rootScope.reserve_Id = data.reservation_id;
            $rootScope.braider_Id = data.braider_id;
            $rootScope.value = value;
            var signal = [];
                signal[0] = 'Reject';
                signal[1] = 'Accept';
        console.log(signal[value]);
        var Confirm = confirm("Are you sure you want to " + signal[value] + " your reservation?");
        if (Confirm == true) {
            $rootScope.reserve_Id = data.reservation_id;
            $rootScope.competitor_Id = data.compid;

            $rootScope.cancel = AllService.acceptReject($rootScope.currentUser, $rootScope.reserve_Id, $rootScope.braider_Id,$rootScope.value)
            $rootScope.cancel.then(function(acceptReject) {
                if(acceptReject.message == 'Reservation has been updated!')
                {
		   if(signal[value] == 'Accept') {
		   	 $rootScope.acceptReject = 'Reservation has been accepted!';
		   }else {
			$rootScope.acceptReject = 'Reservation has been cancelled!';
		    }
                    setTimeout(function() {
                        $rootScope.acceptReject = '';
                        $rootScope.$apply();
                    }, 5000);
                }
                   $state.go('dashboard');
                    
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });
        }

    }
    $scope.setalreadySelected = function(){

         $rootScope.dropSelectedStateId;
         $rootScope.dropSelectedMonthId;
         $rootScope.dropSelectedShowsId;
         $rootScope.dropSelectedDateId;
         $scope.createReservation();

    }

    $scope.getState = function() {
        $rootScope.states = AllService.getState($rootScope.currentUser)
        $rootScope.states.then(function(allstates) { 
            $rootScope.allStates = allstates.State;
             $rootScope.monthfalse = false;

        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);

        });
    }
 
    $scope.setState = function(state) {
        
        $rootScope.state = state;
        $scope.allmonth = '';
        $rootScope.month = undefined;
        $rootScope.monthshowList = '';
        $rootScope.nextDate = '';
        $scope.allshows = '';
        $scope.alldate = '';
        $rootScope.monthfalse = true;
        $rootScope.allShowsDate = '';
        if($rootScope.backButton != undefined){
            $rootScope.backButton.editMonth = '';
            $rootScope.backButton.editShow = '';
            $rootScope.backButton.editDate = '';
        }
        // $rootScope.editshow = '';
        // $rootScope.editDate = '';
        $rootScope.buttonEnable = false;
        $rootScope.dateArray = $scope.selection.splice(0, $scope.selection.length);

        for(var i=0; i<$rootScope.allStates.length; i++) {
            
            if($rootScope.state == $rootScope.allStates[i].id) {
                $rootScope.stateName = $rootScope.allStates[i].name;
            }
        }

        $rootScope.stateShow = AllService.monthshows($rootScope.currentUser, $rootScope.month, $rootScope.state)
        $rootScope.stateShow.then(function(showss) {
            
            if(showss.shows.length == 0) {
                $scope.noStateShow = 'No Shows available for this state';
                setTimeout(function() {
                    $scope.noStateShow = '';
                    $scope.$apply();
                }, 3000);
            }
        });
    }
    

    $scope.backDate = function() {
        $rootScope.monthfalse = true;
        $state.go('choose-date');
        $rootScope.backButton =  JSON.parse($window.sessionStorage["dateInfo"]);
        
        $rootScope.monthfalse = true;
        
        $rootScope.buttonEnable = true;
        
    }

   $rootScope.months = [{"name":"All", "id":"0"},{"name":"January", "id":"1"}, {"name":"February", "id":"2"}, {"name":"March", "id":"3"}, {"name":"April", "id":"4"}, {"name":"May", "id":"5"}, {"name":"June", "id":"6"}, {"name":"July", "id":"7"}, {"name":"August", "id":"8"}, {"name":"September", "id":"9"}, {"name":"October", "id":"10"}, {"name":"November", "id":"11"}, {"name":"December", "id":"12"}];
//$rootScope.months = [{"name":"All", "id":0},{"name":"January", "id":1}, {"name":"February", "id":2}, {"name":"March", "id":3}, {"name":"April", "id":4}, {"name":"May", "id":5}, {"name":"June", "id":6}, {"name":"July", "id":7}, {"name":"August", "id":8}, {"name":"September", "id":9}, {"name":"October", "id":10}, {"name":"November", "id":11}, {"name":"December", "id":12}];

    
    //for selected month 
       $scope.setMonth = function(month) {
        
        $rootScope.month = month;
        $rootScope.state = $rootScope.state;
        $scope.allshows = '';
        $scope.alldate = '';
        $rootScope.nextDate = '';
        $rootScope.allShowsDate = ''; 
        if($rootScope.backButton != undefined){
            $rootScope.backButton.editShow = '';
            $rootScope.backButton.editDate = '';
        }
        $rootScope.buttonEnable = false;
        $rootScope.dateArray = $scope.selection.splice(0, $scope.selection.length);

        $rootScope.monthshows = AllService.monthshows($rootScope.currentUser, $rootScope.month, $rootScope.state)
        $rootScope.monthshows.then(function(allshows) { 
            $rootScope.monthshowList = allshows.shows;
             
            if($rootScope.monthshowList == "") {
                $rootScope.noshow = 'No Show available for this month. Choose another month.';
                setTimeout(function() {
                    $rootScope.noshow = '';
                    $rootScope.$apply();
                }, 3000);
            }

            if(allshows.message) {
                $scope.error = allshows.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            }
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    $scope.viewProfileShows = function() { 
        if($rootScope.serviceArray == undefined) {
            $state.go('dashboard')
        }
        for(var i=0; i<$rootScope.monthshowList.length; i++) { 
            if($rootScope.allshows == $rootScope.monthshowList[i].id) { 
                $rootScope.showDisplay = $rootScope.monthshowList[i].showName; 
            }
        }
    }

    $rootScope.confirmDisable = true;
    $scope.dateSelected = [];
    //for add service by braider
    $scope.reserve= {};
    $scope.reservationResponseID = true;
    $scope.createReservation = function() {
        $rootScope.confirmDisable = false;
    var total_price = 0;
     for(var p=0;p<$rootScope.braidServices.length;p++){
            var check = $rootScope.serviceArray.indexOf($rootScope.braidServices[p].id);
            if(check!=-1){
              total_price +=window.Math.round($rootScope.braidServices[p].price * 100)/100; 
             }
            } 
        //$scope.reserve.date = $rootScope.date;
        if($rootScope.horseId1) {
            $scope.reserve.horse_id = $rootScope.horseId1;
        }else {
            $scope.reserve.horse_id = $rootScope.horseId;
        }
        // if($rootScope.dropSelectedDateId){
        //  $scope.reserve.date.push($rootScope.dropSelectedDateId);
         
        // }
        if($rootScope.dropSelectedDateId1){
         $scope.reserve.date = $rootScope.dropSelectedDateId1
          
        
        } else {
            $scope.reserve.date = $rootScope.date;
            
        }

        $scope.dateSelected.push($scope.reserve.date);
        $rootScope.avail_dates = $scope.dateSelected;
        
        if($rootScope.avail_dates) {
            $scope.braider_avail_Dates();
        }
        
        $scope.reserve.total_price = total_price;
        
        $scope.reserve.state_id = $rootScope.state;
        $scope.reserve.show_id = $rootScope.allshows;

        //$scope.reserve.date = $rootScope.date;
        $scope.reserve.braider_id = $rootScope.braider_id;
        $scope.reserve.competitor_id = $rootScope.currentUser.User.id;
        $scope.reserve.access_token = $rootScope.currentUser.User.access_token;
        //$scope.reserve.horse_id = $rootScope.horseId;
        $scope.reserve.service_ids = $rootScope.serviceArray;
        $scope.newReservation = function() {
            return AllService.createReservation($scope.reserve);

          }                
        $scope.response = $scope.newReservation()
        $scope.showGreeting = false;
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);  
            } else { 
               $scope.msg="Thanks for your reservation! Remember once you know your Barn Name, Barn Number and Stall Number, come back to Edit your reservation and provide that information. That makes sure the Braider can actually find you!";
               $scope.showGreeting = true;
               $timeout(function(){
                  $scope.showGreeting = false;
               }, 14000);
               $scope.alldate = '';
             //$state.go('dashboard');
             $scope.disableSave = true;
             $scope.disableHorseSave = true;
             sessionStorage.removeItem('serviceInfo');
             sessionStorage.removeItem('horseInfo');
             if(sessionStorage.removeItem('serviceInfo') && sessionStorage.removeItem('horseInfo')) {
                
             }

          }                    
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });   
        
    } 

    $scope.changeUrl = function() {
       
    $("body").removeClass("modal-open");
  
        $state.go('dashboard');
    }

$scope.braider_avail_Dates = function() {
    
    $scope.braider_avail_id = $rootScope.braiderAvailableId
    $scope.dateselected = $rootScope.avail_dates;
    $scope.neglectDate = AllService.braiderDate($rootScope.currentUser, $scope.braider_avail_id, $scope.dateselected)
    $scope.neglectDate.then(function(response) {
        
        $scope.remaining = response.remaning_dates;
    });
}

      $rootScope.edit = [];
    $scope.editstall = function(services){
        $rootScope.edit[services] = true;
    }

   $scope.updateReservation = function() {
        var reservation = {};
         $rootScope.edit = [];
        reservation.barnName = $scope.Singlereservation[0].barnName;
        reservation.barnNumber = $scope.Singlereservation[0].barnNumber;
        reservation.stallNumber = $scope.Singlereservation[0].stallNumber;
        reservation.reservation_id = $scope.Singlereservation[0].reservation_id;
        reservation.access_token = $rootScope.currentUser.User.access_token;
        reservation.competitor_id = $scope.Singlereservation[0].compid;
        $scope.updatereserve = function() {
            return AllService.updateReservation(reservation);
        }

        $scope.res = $scope.updatereserve()
        $scope.res.then(function(updateReserve) {
            //console.log('updateReserve', updateReserve)
            $rootScope.updateRes = updateReserve.message;
                setTimeout(function() {
                    $rootScope.updateRes = '';
                    $rootScope.$apply();
                }, 5000); 
        });

    }

    $scope.msgBraider = {};
    $scope.messageToBraider = function(reserveId, braidId) {
        $scope.msgBraider.to_user = braidId;
        $scope.msgBraider.access_token = $rootScope.currentUser.User.access_token;
        $scope.msgBraider.msg = $scope.txt;
        $scope.msgBraider.reservation_id = reserveId;
        console.log($scope.msgBraider.reservation_id);
        $scope.sendBraiderMessage = function() {
            return AllService.sendMessage($scope.msgBraider);
        }
        $scope.response = $scope.sendBraiderMessage();
        $scope.response.then(function(data) {
            $rootScope.message = data.message;
             $scope.messages_list = AllService.competitorMessage($scope.msgBraider.reservation_id);
        $scope.messages_list.then(function(message){
                $rootScope.Allmessage = message;
                
        });
            setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000); 
            if(data) {
                $scope.txt = '';
            }
        });
    } 
     //for send message to competitor
    $scope.msgCompetitor = {};
    $scope.messageToCompetitor = function(reserveId, competitorId) {
        $scope.msgCompetitor.to_user = competitorId;
        $scope.msgCompetitor.access_token = $rootScope.currentUser.User.access_token;
        $scope.msgCompetitor.msg = $scope.txt;
        $scope.msgCompetitor.reservation_id = reserveId;
        $scope.sendCompetitorMessage = function() {
            return AllService.sendMessage($scope.msgCompetitor);
        }
        $scope.response = $scope.sendCompetitorMessage()
        $scope.response.then(function(data) {
            $rootScope.message = data.message;
            $scope.messages_list = AllService.competitorMessage($scope.msgCompetitor.reservation_id);
        $scope.messages_list.then(function(message){
                $rootScope.Allmessage = message;
                
        });
            setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
                if(data) {
                    $scope.txt = '';
                } 
        });
    } 


    //for add service by braider
    $scope.service= {};
    $scope.preview = '';
    $scope.addNewService = function() {
        $scope.service.access_token = $rootScope.currentUser.User.access_token;
        $scope.service.braider_id = $rootScope.currentUser.User.id;
        $scope.newService = function() {
            return AllService.add($scope.service);
        }                
        $scope.response = $scope.newService()
        $scope.response.then(function(data){
            //console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);  
            } else {
               $rootScope.message = data.message;
               $state.go('my-services');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);  
            }                    
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });   
        
    } 

    //for all braider services
    $rootScope.noService = false;
    $scope.braider_services = function() {

        $scope.listBraiderServices = AllService.all($rootScope.currentUser)
        $scope.listBraiderServices.then(function(data){
             $rootScope.AllBraiderServices = data.services;
           // console.log('$rootScope.AllBraiderServices', $rootScope.AllBraiderServices)
             if(!$rootScope.AllBraiderServices) {
                $rootScope.noService = true;
                $rootScope.noService = 'No services yet!'
            }
            var pagesShown = 1;
            var pageSize = 10;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.AllBraiderServices.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            }; 
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });

    }
    $scope.editingData = [];
    $scope.modify = function(tableData){
        $scope.editingData[tableData.id] = true;
    };
    $scope.updatetable = function(tableData){
        var tabledata = tableData
        var access_token = $rootScope.currentUser.User.access_token
        //$scope.editingData[tableData.id] = false;
        AllService.updateService(tabledata,access_token).then(function(data)
       {
        
        if(data.data.status == 'error'){
            $scope.err = data.data.message[0];
            if($scope.err == 'name is required.'){
                $scope.msg = 'Name is required'
                setTimeout(function() {
                    $scope.msg = '';
                    $scope.$apply();
                }, 5000); 
            }else if($scope.err == 'price is required.'){
                $scope.msg = 'Price is required'
                setTimeout(function() {
                    $scope.msg = '';
                    $scope.$apply();
                }, 5000); 
            }else {
                $scope.editingData[tableData.id] = false;
            }
            
        }
        if(data.data.status == 'success') {
            $window.location.reload();
        }
        if(data.data.status == 'success') {
            $scope.serviceUpdate = 'Service has been updated successfully';
        }

       }); 

    };

     $scope.selection=[];

     $scope.datetoggleSelection = function($event, date) {
    
         var check = $event.target;
         var checkState = (check.checked ? 'add' : 'remove');
         
         if(checkState == 'add') {
            $scope.selection.push(date);
        }else if(checkState == 'remove') {
            var selectedValue = date;
            var i = $scope.selection.indexOf(date);
            if(i != -1) {
                $scope.selection.splice(i, 1);
                console.log('selection11', $scope.selection)
            }
        }
        
            $rootScope.dateArray = $scope.selection;
            
     };

     

     $scope.datetoggleSelection1 = function($event, date) {
        
         var check = $event.target;
         var checkState = (check.checked ? 'add' : 'remove');
         
         if(checkState == 'add') {
            $rootScope.selection1.push(date);
        }else if(checkState == 'remove') {
            var selectedValue = date;
            var i = $rootScope.selection1.indexOf(date);
            if(i != -1) {
                $rootScope.selection1.splice(i, 1);
                
            }
        }
        
            $rootScope.dateArray = $rootScope.selection1;
            
     };


    

     //for add show by braider
    $scope.show= {};
    //$scope.show.status = true;
    $scope.preview = '';
    $scope.braideraddshow = function() {
        
        $scope.show.access_token = $rootScope.currentUser.User.access_token;
        $scope.show.braider_id = $rootScope.currentUser.User.id;
        $scope.show.state_id = $rootScope.state;
        $scope.show.month_id = $rootScope.month;
        $scope.show.show_id = $rootScope.allshows;
        $scope.show.status = $scope.show.status;

        //$scope.show.dates_available = $rootScope.braidershowseleteddate;$scope.selection
        //$scope.show.dates_available = $scope.selection;

        $scope.show.dates_available = $scope.dateArray;
 
        
        $scope.newShow = function() {
            return Braiderportfolio.addShowByBraider($scope.show);
        }                
        $scope.response = $scope.newShow()
        $scope.response.then(function(data){
            

               $rootScope.showAdded = data.data.message;
               $state.go('show-schedule');
                setTimeout(function() {
                    $rootScope.showAdded = '';
                    $rootScope.$apply();
                }, 5000);  
                             
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        }); 
        //}
    } 

 
// =================Add show by braider function Ends======================= 

//listing of braider available shows
$rootScope.havingNoShow = false;
$scope.getShows = function() {
   
    $scope.listAllShows = AllService.getShows($rootScope.currentUser)
        $scope.listAllShows.then(function(data){
            $rootScope.listShows = data.BraiderAvialablity;
            
            
            if($rootScope.listShows == '') {
                $rootScope.havingNoShow = true;
                $rootScope.havingNoShow = 'No Shows..';
            }
            var pagesShown = 1;
            var pageSize = 9;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.listShows.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            }; 
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
}

//get single show on the basis of show id
$scope.getSingleShow = function(showId) {
    $rootScope.braider_availId = showId;

    $scope.SingleShow = AllService.getSingleShow($rootScope.currentUser, $rootScope.braider_availId)
        $scope.SingleShow.then(function(data){

            $rootScope.singleShow = data.BraiderAvialablity;
            $rootScope.singlestate = $rootScope.singleShow.state_id;
            $rootScope.singleshow = $rootScope.singleShow.show_id;
 
            /*$rootScope.datesAvailable = $rootScope.singleShow.dates_available;
            $scope.selection = $rootScope.singleShow.dates_available;
            console.log($scope.selection);*/
 
            $rootScope.datesAvailable = $rootScope.singleShow.all_dates_available;
            $rootScope.all_dates_available = $rootScope.singleShow.all_dates_available;
            
 
            $rootScope.braidershowseleteddate = $rootScope.singleShow.dates_available;
             $scope.selection = $rootScope.singleShow.dates_available;
             $rootScope.selection1 = $rootScope.singleShow.dates_available;
            $rootScope.singlemonth = data.BraiderAvialablity.month_id;
            
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
}


$scope.selectDatespreselect = function(date){
      // console.log('datehdgshd', date)
       for(var i=0;i <$rootScope.singleShow.dates_available.length;i++){
        var index = $rootScope.singleShow.dates_available.indexOf(date);
         if(index != -1){
            return true;
          }else{
            return false;

          }
          
       }

}


//update show
$scope.updateShow = function() {

  for(var i = 0;i < $scope.selection.length;i++){
             $rootScope.braidershowseleteddate.push($scope.selection[i]);
        }
 

     var data = {};
        data.id = $scope.singleShow.id,
        data.access_token = $rootScope.currentUser.User.access_token,
        data.state_id = $scope.singlestate,
        data.show_id = $scope.singleshow,
        data.month_id = $scope.singlemonth,
        data.dates_available = $rootScope.braidershowseleteddate,
        //data.status = $scope.status
        //console.log('arr', arr)      
            
       $scope.updateShowData = function() {
            return AllService.updateShow(data);
        }                
        $scope.response = $scope.updateShowData()
        $scope.response.then(function(response){
           
            if ( data.status == 'error' ) {
                $scope.error = response.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);   
            } else {
                $rootScope.message = response.message; 
                //$state.go('dashboard');  
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        }); 

}

$scope.forgot = function(email) {
    
    $scope.forgotPassword = AllService.forgotPswd(email);
    $scope.forgotPassword.then(function(data){
       // console.log('dataaaaaaaaa forget', data)
        $rootScope.message = data.message; 
        $state.go('login');
        setTimeout(function() {
            $rootScope.message = '';
            $rootScope.$apply();
        }, 5000);
});
}

$scope.newPass = {};
$scope.reset = function() {
    $scope.newPass.access_token = $rootScope.currentUser.User.access_token;
    $scope.newPass.id = $rootScope.currentUser.User.id;
    $scope.resetPassword = AllService.updatePassword($scope.newPass);
    $scope.resetPassword.then(function(data){
        if(data.status=='error'){
            $rootScope.passErr = data.errors; 
            setTimeout(function() {
            $rootScope.passErr = '';
            $rootScope.$apply();
        }, 5000);
        }else{
            $rootScope.pass = data.result; 
        }
        setTimeout(function() {
            $rootScope.pass = '';
            $rootScope.$apply();
        }, 5000);
});
}

}]);

app.controller('logoutController',['$scope','$rootScope','$window','$location', function($scope,$rootScope,$window,$location){
     $rootScope.currentUser = '';
     localStorage.removeItem('userdata');
     $location.path('/login');
}]);


app.controller('braiderportfolioController',['$scope','$rootScope', 'store','$window','Braiderportfolio','$timeout',function($scope,$rootScope, store,$window,Braiderportfolio,$timeout){

    $scope.loader = false;
    Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
    {
        $scope.loader = false;
       if(data.status=='success' && data.hasOwnProperty('message')){
       $scope.message = 'No image added yet.'
      }else{
         $scope.protfolio = data; 
         
         var pagesShown = 1;
            var pageSize = 6;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.protfolio.portfolio.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            }; 
        }
    });

    $scope.removePortfoliimage = function(p_id)
    {
    var access_token = $rootScope.currentUser.User.access_token;   
    Braiderportfolio.deleteWorkPicture(access_token,p_id).then(function(data){
    if(data.status=='success' && data.hasOwnProperty('message')){
      Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
      {
       $scope.protfolio = data; 
       if(data.status=='success' && data.hasOwnProperty('message')){
       //$scope.message = data.
      }else{
         $scope.protfolio = data;
         
         $scope.message = data.message; 
         $scope.detetePic = 'Image has been deleted successfully.'
            setTimeout(function() {
                    $scope.detetePic = '';
                    $scope.$apply();
                }, 5000);
        }
       });
          
     } 
    });

    $window.location.reload(); 
 }

    $scope.imageuploadbtn = true;
    $scope.showError = false;
    $scope.fileUpload1 = function(){
        $scope.fileData = null;
        var f = document.getElementById('3d_file').files[0];
        var size = f.size;
        $scope.size = size;
        if(size > 2000000){
             f = null;
          
          $scope.showError = 'Sorry, but that image is too large to upload. Please only upload images 2mb or smaller.';
          $timeout(function() {
          $scope.showError = '';
          }, 2000)
         }else{
         $scope.imageuploadbtn = false;
        }
        if(f) {
        r = new FileReader();
        $scope.filename = f.name;
        $scope.showImage = false;
        $scope.$apply();
        r.onload = function(e){
            $scope.fileData = e.target.result;
        document.getElementById('abc').src= e.target.result ;
        $scope.image = e.target.result; 
        $scope.$apply();
        };

             r.readAsDataURL(f);
        }
         
    };
    
    $scope.submit = function(){
        $scope.loader = true;
    var access_token = $rootScope.currentUser.User.access_token; 
    
      var imagedata = {

                            'image' : $scope.image,
                            'caption' : $scope.caption
                     };
                     $scope.image='';
                    $scope.caption='';
                 Braiderportfolio.addWorkPicture(imagedata,access_token).success(function(data) {
                   if(data.status=='success' && data.hasOwnProperty('message')){
                    
                    Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
                      {
                       if(data.status=='success' && data.hasOwnProperty('message')){
                       $scope.message = 'No image added yet.'

                      }else{
                         $scope.protfolio = data; 
                        }
                        $rootScope.imageAdded = 'Image has been added successfully';
                            setTimeout(function() {
                            $rootScope.imageAdded = '';
                            $rootScope.$apply();
                        }, 5000);
                            $window.location.reload();
                       });
                                     
                   };         
                             


                
               }).error(function(result){
                     
               }); 
               //$window.location.reload(); 

                            
                         


    }
        


}]);
 


