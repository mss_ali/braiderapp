app.controller('homeController',['$scope','$http', '$state', '$rootScope', 'AllService','store', function($scope,$http, $state, $rootScope, AllService, store){
//console.log('store', store)


	$scope.items = {};
    $scope.openLogin = function() {
        $scope.login = function() {
         
            return AllService.login($scope.items);
        }
        $scope.response = $scope.login()
        $scope.response.then(function(userdata){
            //console.log('userdata',userdata);
        	store.set('userdata', JSON.stringify(userdata));
        	 
        	$scope.user = store.get('userdata');
        	
        	if($scope.user.status == 'error') {
        		$scope.error = 'Email and Password is Required!';
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                if($scope.user.message == 'Invalid username or password') {
                    $scope.error = 'Invalid credentials given.';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
                if($scope.user.message == 'email is required.') {
                    $scope.error = 'Sorry, it looks like your email is missing.';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
                if($scope.user.message == 'password is required.') {
                    $scope.error = 'Sorry, it looks like your password is missing.';
        		    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
                if($scope.user.message == 'suspended') {
                    $scope.error = 'Your account has been temporarily suspended.';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 4000);
                }
        	}else {
        		$rootScope.currentUser = $scope.user.User;
                $state.go('dashboard');
        	}
            
        },function(error) {
            $scope.error = 'Invalid credentials given.';
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        });                
    };

    $scope.register = {};
    $scope.openRegister = function() {
        $scope.createAccount = function() {
            console.log($scope.register);
            return AllService.register($scope.register);
        }                
        $scope.response = $scope.createAccount()
        $scope.response.then(function(data){
           //console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.errors = data.errors;
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.errors = '';
                    $scope.$apply();
                }, 5000); 
                
            } else {                
                 $scope.login = function() {

                 return AllService.login($scope.register);
            }
                $scope.response = $scope.login()
                $scope.response.then(function(userdata){
                    //console.log('userdata',userdata);
                    store.set('userdata', JSON.stringify(userdata));
                     
                    $scope.user = store.get('userdata');
                    
                    if($scope.user.status == 'error') {
                        $scope.error = $scope.user.message;
                        if($scope.error == 'email is required.') {
                            $scope.error = 'Sorry, it looks like your email is missing.';
                            setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                            }, 5000);
                        }
                        if($scope.error == 'password is required.') {
                            $scope.error = 'Sorry, it looks like your password is missing.';
                            setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                            }, 5000);
                        }
                    }else {
                        $rootScope.currentUser = $scope.user.User;
                        $state.go('dashboard');
                    }
                    
                },function(error) {
                    $scope.error = 'Invalid credentials given.';
                    setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                        }, 5000);
                });  
                        
                    }                    
                },function(error)  {
                    $scope.error = "Invalid credentials.";
                    setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                        }, 5000);

                });                
            };


    $scope.termsandcondition = function() { console.log('here')
        
        $rootScope.condition = AllService.termsCondition()
        $rootScope.condition.then(function(termsCondition) {//console.log('termsCondition', termsCondition)
            $rootScope.termsCondition = termsCondition.message;
               console.log('$rootScope.termsCondition', $rootScope.termsCondition)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    };




}]);

app.controller('DashboardController',['$scope','$http', '$state', '$rootScope', 'AllService','store','Braiderportfolio','$timeout', '$window','API', function($scope,$http, $state, $rootScope, AllService, store,Braiderportfolio,$timeout, $window, API){

    if(angular.isObject(store.get('userdata'))) {
        $rootScope.currentUser = store.get('userdata');
         
        $scope.userRole = $rootScope.currentUser.User.role;
        
    }
    //get my Reservation
     $scope.braiderReservation = function() {
        $rootScope.reservation = AllService.myReservation($rootScope.currentUser)
        $rootScope.reservation.then(function(response) {
            console.log('response', response);
            $rootScope.reservations = response.braidereservations;
            for(var i=0; i<$rootScope.reservations.length; i++) {

            if($rootScope.reservations[i].status == 'confirm') {console.log('status', $rootScope.reservations[i].status)
                $rootScope.reservations[i].status = 'Confirmed';
            }
            }
            $rootScope.length = $rootScope.reservations.length;

            console.log('reservations', $rootScope.reservations);
            var pagesShown = 1;
            var pageSize = 3;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.reservations.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);

        });
     }   
     //get profile
    $scope.profile = function() {
        
        $rootScope.user = AllService.getProfile($rootScope.currentUser)
        $rootScope.user.then(function(profile) {console.log('profile', profile);
            $scope.userProfile = profile.User;
            
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);

        });
    }

    //for update profile
    $scope.update = function() {
        var arr = {};
        arr.id = $rootScope.currentUser.User.id,
        arr.access_token = $rootScope.currentUser.User.access_token,
        arr.name = $scope.userProfile.name,
        arr.email = $scope.userProfile.email,
        arr.phone = $scope.userProfile.phone
        //console.log('arr', arr)      
            
       $scope.updateData = function() {
            return AllService.updateProfile(arr);
        }                
        $scope.response = $scope.updateData()
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
                if(data.errors == 'Email is already exist.') {
                    $scope.sameEmail = 'Email already exists!';
                    setTimeout(function() {
                    $scope.sameEmail = '';
                    $scope.$apply();
                }, 5000); 
                } 
            } else {
                $rootScope.updateP = data.message; 

                store.set('userdata', JSON.stringify(data));
                $rootScope.currentUser = store.get('userdata');
                $state.go('dashboard'); 
                $rootScope.user = AllService.getProfile($rootScope.currentUser)
                $rootScope.user.then(function(profile) {console.log('profile', profile);
                    $scope.userProfile = profile.User;
                    
                }, function(error) {

                    $scope.error = error;
                    setTimeout(function() {
                            $scope.error = '';
                            $scope.$apply();
                        }, 5000);

                });
                setTimeout(function() {
                    $rootScope.updateP = '';
                    $rootScope.$apply();
                }, 5000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        });                
            
    };


    //for add new horse
    $scope.horse ={};
    $scope.preview = '';
    $scope.addHorse = function() {
        $scope.horse.access_token = $rootScope.currentUser.User.access_token;
        $scope.horse.image = this.fileData;
       $scope.newHorse = function() {
            return AllService.horse($scope.horse);
        }                
        $scope.response = $scope.newHorse()
        $scope.response.then(function(data){
           // console.log('dataaaa', data)
           
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);   
            } else {
               $rootScope.message = data.message; 
               $state.go('myhorses');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
                
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
                
        });                
            
    } 
    
    
 
    //upload image
  
   $scope.imageuploadbtn = true;
    $scope.showError = false;
    $scope.fileUpload = function(){
        $scope.fileData = null;
        var f = document.getElementById('3d_file').files[0];
        var size = f.size;
        $scope.size = size;
        if(size > 2000000){
             f = null;
          console.log("comes in if case");
          $scope.showError = 'Sorry, but that image is too large to upload. Please only upload images 2mb or smaller.';
          $timeout(function() {
          $scope.showError = '';
          }, 2000)
         }else{
         $scope.imageuploadbtn = false;
        }
        if(f) {console.log('if');
        r = new FileReader();
        $scope.filename = f.name;
        $scope.showImage = false;
        $scope.$apply();
        r.onload = function(e){
            $scope.fileData = e.target.result;
        document.getElementById('abc').src= e.target.result ;
        $scope.image = e.target.result; 
        $scope.$apply();
        };

             r.readAsDataURL(f);
        }
         
    };


    $scope.faq3 = function() {
        
            $rootScope.faq1 = AllService.faq($rootScope.currentUser)
            $rootScope.faq1.then(function(faq) { 

                $rootScope.faq2 = faq.message.CmsPage;
                console.log('$rootScope.faq2', $rootScope.faq2)
                 
            });
        }

    $scope.deleteHorse = function(horseId) {
        var Confirm = confirm("Are you sure you want to delete horse?");
        if (Confirm == true) {
            $rootScope.horse_id = horseId;
            $rootScope.del = AllService.delHorse($rootScope.currentUser, $rootScope.horse_id)
            $rootScope.del.then(function(deleteHorse) {console.log('deleteHorse', deleteHorse)
                $rootScope.deleteHorse = deleteHorse.message;
                    setTimeout(function() {
                        $rootScope.deleteHorse = '';
                        $rootScope.$apply();
                    }, 5000);

            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });

    

        }
        $window.location.reload();
    }



    $scope.faq = function() {console.log('here')
        
            $rootScope.faq = AllService.faq($rootScope.currentUser)
            $rootScope.faq.then(function(faq) {console.log('faq', faq)
                $rootScope.faq = faq.message.CmsPage;
                   
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });
    }

    $scope.redirect = function() {
        $state.go('horses');
    }

    //get listing of horses
    $rootScope.noHorse = false;
    $scope.horseListing = function() {
        $rootScope.horses = AllService.getHorses($rootScope.currentUser)
        $rootScope.horses.then(function(horses) {
            $rootScope.horseList = horses.horses;
           // $rootScope.horseLength = $rootScope.horseList.length;
            if(horses.status == 'error'){
                $rootScope.noHorse = true;
               $rootScope.noHorse = 'No Horse added yet!';
            }else {
                
                var pagesShown = 1;
                var pageSize = 6;
                
                $scope.paginationLimit = function(data) {
                    return pageSize * pagesShown;
                };
                $scope.hasMoreItemsToShow = function() {
                    return pagesShown < ($rootScope.horseList.length / pageSize);
                };
                $scope.showMoreItems = function() {
                    pagesShown = pagesShown + 1;       
                };  
            }
        }, function(error) {

            $scope.error = error;

        });
    }

    //for selected horse value
    $scope.setHorse = function(horse) {
        $scope.chooseHorse = horse;
    }
   $scope.singleHorse = function(horse_id) {
      $rootScope.horseId = horse_id.id;
      localStorage["horseid"] = JSON.stringify(horse_id.id);
            $rootScope.horseOne = AllService.getOne($rootScope.currentUser, $rootScope.horseId)
            $rootScope.horseOne.then(function(horse) {//console.log('horses', horse)
            
            $rootScope.horseSingle = horse.horse;
            console.log('horsesingle', $rootScope.horseSingle)
            
            }, function(error) {

                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

            });
    }
    
    //for update horse detail
    $scope.updateDetail = function() {
        $scope.horseSingle.image = this.fileData;
        var arr = {};
        arr.access_token = $rootScope.currentUser.User.access_token,
        arr.id = $scope.horseSingle.id,
        arr.name = $scope.horseSingle.name,
        arr.gender = $scope.horseSingle.gender,
        arr.markings = $scope.horseSingle.markings,
        arr.color = $scope.horseSingle.color,
        arr.height = $scope.horseSingle.height,
        arr.quirks = $scope.horseSingle.quirks,
        arr.image = $scope.horseSingle.image
        
        $scope.updateData = function() {
            return AllService.update(arr);
        }                
        $scope.response = $scope.updateData()
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);   
            } else {
               $rootScope.message = data.message;
               $state.go('myhorses');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });         
    }

    //for listing of braider
    $scope.braiderListing = function() {

        $rootScope.braider = AllService.all($rootScope.currentUser)
        $rootScope.braider.then(function(braiders) {
            $rootScope.braiderList = braiders.braiders.User;
        }, function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    //for selected service
  $scope.checkItem = function (id) {
   var checked = false;
   for(var i=0; i<$rootScope.serviceArray.length; i++) {
   if(parseInt(id) == parseInt($rootScope.serviceArray[i])) {
      checked = true;
      }
    }
    return checked;
  };

     
   $scope.array = [];
   
     console.log('$scope.arrLen', $scope.arrLen)   
    $scope.selectedServices = function($event, arr) {
       // $rootScope.abc = "true";
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        console.log('action', action)
        if(action == 'add') {
            $scope.array.push(arr);
            
        }else if(action == 'remove') {
            var id = arr
            var i = $scope.array.indexOf(id);
            if(i != -1) {
                $scope.array.splice(i, 1);
            }
            //$rootScope.serviceArray = $scope.array;
        }
        $rootScope.serviceArray = $scope.array; 
    } 
    
    
    

   


    //for selected braider
    $scope.setBraider = function(braid) {
        $rootScope.braider_id = braid;
        $scope.selectedBraidServices = AllService.braidService($rootScope.currentUser, $rootScope.braider_id)
        $scope.selectedBraidServices.then(function(data){
            $rootScope.braidServices = data.services;
            
             
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
        
    }
    var serviceArray = [];
    var price = 0;
    
    $scope.viewProfile = function() {
        //console.log('$scope.serviceArray', $rootScope.serviceArray, '$rootScope.braidServices', $rootScope.braidServices)
        for(var j=0; j<$rootScope.serviceArray.length; j++) {

            //console.log('$rootScope.serviceArray', $rootScope.serviceArray[j])
            $rootScope.servicesArray = $rootScope.serviceArray[j];

            for(var i=0; i<$rootScope.braidServices.length; i++) {

                //console.log('$rootScope.braidServices', $rootScope.braidServices[i].id)
                $rootScope.braidServicesArray = $rootScope.braidServices[i].id

                if($rootScope.servicesArray == $rootScope.braidServicesArray) {

                    //console.log('here', $rootScope.servicesArray, '===', $rootScope.braidServicesArray)
                    serviceArray.push($rootScope.braidServices[i]);
                    $rootScope.braiderServicesArray = serviceArray;
                    
                    console.log('$rootScope.braiderServicesArray', $rootScope.braiderServicesArray)
                }
            }
        }
            for(var k=0; k<$rootScope.braiderServicesArray.length; k++) {
                price = parseInt(price) + parseInt($rootScope.braiderServicesArray[k].price);
                $rootScope.total_price = price;
            }
    }

    //for listing of braider
    $scope.showListing = function() {
    
        $rootScope.shows = AllService.shows($rootScope.currentUser)
        $rootScope.shows.then(function(allshows) {
            $rootScope.showList = allshows.shows;
            //console.log('shows', $rootScope.showList)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    //for show schedule
     $scope.showSchedule = function() {
       $rootScope.showsSchedule = AllService.showsSchedule($rootScope.currentUser)
        $rootScope.showsSchedule.then(function(response) {
            $rootScope.showsSchedule = response.scheduleShow;
            console.log('showsschedule', response.scheduleShow)
            //view more functionality
            var pagesShown = 1;
            var pageSize = 8;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.showsSchedule.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };  
        }, function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }


    //for selected show
    var date = [];
    $scope.setShow = function(show) {
        $rootScope.allshows = show;
        
         if($rootScope.monthshowList) {

            for(var i=0; i<$rootScope.monthshowList.length; i++) {
                
                if($scope.allshows == $rootScope.monthshowList[i].id) {
                     
                    $scope.startDate = $rootScope.monthshowList[i].startDate;
                    $scope.endDate = $rootScope.monthshowList[i].endDate;
                    
                }
                
            }
            var split = $scope.startDate.split(" ");
            var start = split[0];

            var split1 = $scope.endDate.split(" ");
            var end =  split1[0];
           
            $scope.selectedDates = AllService.dates($rootScope.currentUser, start, end)

            $scope.selectedDates.then(function(data){ 
                $rootScope.nextDate = data.dates;
                 

                if($rootScope.nextDate){
                    // for(var i=0; i<$rootScope.nextDate.length; i++) {
                        
                    //     var d = new Date($rootScope.nextDate[i]);
                    //     var n = d.getTime(); 
                        
                        
                    //     date.push(n);

                    // }
                        $rootScope.allShowsDate =  data.dates;;
                        $rootScope.braidershowseleteddate = date;

            
                }

            },function(error) {
                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            });
                    
          }
    }
     $scope.removeselectedHorse = function(h_id){
     
      localStorage.removeItem('horseid');

  }
    $scope.singleHorsePopup = function(horse){
         
        $rootScope.horseId1 = [];
        var previosid = angular.fromJson(localStorage.getItem('horseid'));
        console.log('previos',previosid);
        $rootScope.horseId1.push(previosid);
        var index = $rootScope.horseId1.indexOf(horse.id)
        if(index!= -1){
            $rootScope.horseId1.splice(index,1)
        }else{
         $rootScope.horseId1.push(horse.id);
        }

    }
    $scope.setNewDate = function(date){

     $rootScope.dropSelectedDateId1 =  date
    }
    $rootScope.abc = "false";
    $scope.setDate = function(date) {
         $rootScope.dropSelectedStateId = $scope.allstate;
         console.log('$rootScope.dropSelectedStateId', $rootScope.dropSelectedStateId) 
         $rootScope.dropSelectedMonthId = $scope.allmonth;
         $rootScope.dropSelectedShowsId = $scope.allshows; 
         $rootScope.dropSelectedDateId =  $scope.alldate; 
         
        $rootScope.abc= "true";
        var service= [];
        $rootScope.date = date;
        $scope.availBraider = AllService.avail_braider($rootScope.currentUser, $rootScope.allshows, $scope.date)

        $scope.availBraider.then(function(data){ 
        $rootScope.braiderAvailable = data.braiders;
        console.log('$rootScope.braiderAvailable', $rootScope.braiderAvailable)
        // var pagesShown = 1;
        //     var pageSize = 6;
            
        //     $scope.paginationLimit = function(data) {
        //         return pageSize * pagesShown;
        //     };
        //     $scope.hasMoreItemsToShow = function() {
        //         return pagesShown < ($rootScope.braiderAvailable.length / pageSize);
        //     };
        //     $scope.showMoreItems = function() {
        //         pagesShown = pagesShown + 1;       
        //     };
        if(!$rootScope.braiderAvailable) {
        $rootScope.noBraider = 'No Braider available for this date. Choose another date.';
        setTimeout(function() {
            $rootScope.noBraider = '';
            $rootScope.$apply();
             }, 5000);
          }

        if(data.status == 'error'){

                $scope.error = data.errors;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            }
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
    }


    //for show selected braid profile
    $scope.showProfile = function(braidId) {console.log('braidId', braidId)
        var braider_id = braidId;
        
            $rootScope.braiderOne = AllService.getOneBraider($rootScope.currentUser, braider_id)
            $rootScope.braiderOne.then(function(braider) {console.log('braider', braider)
            
            $rootScope.braiderSingle = braider.braider;
            $rootScope.callApi = API;
            console.log('$rootScope.braiderSingle', $rootScope.braiderSingle)
            }, function(error) {

                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 


            });
    }

    //for competitor schedule reservation
    $scope.competitorReservation = function() {
    
        $rootScope.shows = AllService.getReservation($rootScope.currentUser)
        $rootScope.shows.then(function(reservation) {console.log('reservation', reservation)
            $rootScope.reservations = reservation.competeterreservations;
               
            console.log('$rootScope.reservations', $rootScope.reservations)
            for(var i=0; i<$rootScope.reservations.length; i++) {

            if($rootScope.reservations[i].status == 'confirm') {console.log('status', $rootScope.reservations[i].status)
                $rootScope.reservations[i].status = 'Confirmed';
            }
            }
            $rootScope.reserveLength = $rootScope.reservations.length;
            
            var pagesShown = 1;
            var pageSize = 6;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.reservations.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };

        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    //for single reservation
    $scope.competitorSingleReservation = function(reserveId) {
        $rootScope.reservationId = reserveId.reservation_id;
        
        $rootScope.shows = AllService.getSingleReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.shows.then(function(reservation) {//console.log('Singlereservation', reservation)
            $rootScope.Singlereservation = reservation.competeterreservations;
            console.log('competitorreservations', $rootScope.Singlereservation)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    $scope.cancelReservation = function(data) {
        var Confirm = confirm("Are you sure you want to cancel your reservation?");
        if (Confirm == true) {
                $rootScope.reserve_Id = data.reservation_id;
            $rootScope.competitor_Id = data.compid;

            $rootScope.cancel = AllService.cancelReservation($rootScope.currentUser, $rootScope.reserve_Id, $rootScope.competitor_Id)
            $rootScope.cancel.then(function(cancelReservation) {console.log('cancelReservation', cancelReservation)
                // $rootScope.cancelReservation = reservation.competeterreservations;
                // console.log('reservations', $rootScope.Singlereservation)
                $rootScope.cancel = cancelReservation.message;
                   $state.go('myreservation');
                    setTimeout(function() {
                        $rootScope.cancel = '';
                        $rootScope.$apply();
                    }, 5000);
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });
        }

    }

    $scope.braiderSingleReservation = function(reservationId) {
        $rootScope.reservationId = reservationId.reservation_id;
        

        $rootScope.reservation = AllService.braiderReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.reservation.then(function(reservation) {
            $rootScope.reservationSingle = reservation.braidereservations;
            //console.log('reservations', $rootScope.reservationSingle)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });

    }

    $scope.acceptRejectReservation = function(data, value) {
      // console.log('data', data, 'value', value);
         $rootScope.reserve_Id = data.reservation_id;
            $rootScope.braider_Id = data.braider_id;
            $rootScope.value = value;
            var signal = [];
                signal[0] = 'Reject';
                signal[1] = 'Accept';

        var Confirm = confirm("Are you sure you want to " + signal[value] + " your reservation?");
        if (Confirm == true) {
            $rootScope.reserve_Id = data.reservation_id;
            $rootScope.competitor_Id = data.compid;

            $rootScope.cancel = AllService.acceptReject($rootScope.currentUser, $rootScope.reserve_Id, $rootScope.braider_Id,$rootScope.value)
            $rootScope.cancel.then(function(acceptReject) {console.log('cancelReservation', acceptReject)
                
                $rootScope.acceptReject = acceptReject.message;
                   $state.go('dashboard');
                    setTimeout(function() {
                        $rootScope.cancel = '';
                        $rootScope.$apply();
                    }, 5000);
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 5000); 

            });
        }

    }
    $scope.setalreadySelected = function(){

         $rootScope.dropSelectedStateId;
         $rootScope.dropSelectedMonthId;
         $rootScope.dropSelectedShowsId;
         $rootScope.dropSelectedDateId;
    }

    $scope.getState = function() {
        $rootScope.states = AllService.getState($rootScope.currentUser)
        $rootScope.states.then(function(allstates) { 
            $rootScope.allStates = allstates.State;
             

        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);

        });
    }
 
    $scope.setState = function(state) {
        console.log(state);
        $rootScope.state = state;
        for(var i=0; i<$rootScope.allStates.length; i++) {
            
            if($rootScope.state == $rootScope.allStates[i].id) {
                $rootScope.stateName = $rootScope.allStates[i].name;
            }
        }

        $rootScope.stateShow = AllService.monthshows($rootScope.currentUser, $rootScope.month, $rootScope.state)
        $rootScope.stateShow.then(function(showss) {
            console.log('showss', showss)
        });
    }

   $rootScope.months = [{"name":"All", "id":"0"},{"name":"January", "id":"1"}, {"name":"February", "id":"2"}, {"name":"March", "id":"3"}, {"name":"April", "id":"4"}, {"name":"May", "id":"5"}, {"name":"June", "id":"6"}, {"name":"July", "id":"7"}, {"name":"August", "id":"8"}, {"name":"September", "id":"9"}, {"name":"October", "id":"10"}, {"name":"November", "id":"11"}, {"name":"December", "id":"12"}];
//$rootScope.months = [{"name":"All", "id":0},{"name":"January", "id":1}, {"name":"February", "id":2}, {"name":"March", "id":3}, {"name":"April", "id":4}, {"name":"May", "id":5}, {"name":"June", "id":6}, {"name":"July", "id":7}, {"name":"August", "id":8}, {"name":"September", "id":9}, {"name":"October", "id":10}, {"name":"November", "id":11}, {"name":"December", "id":12}];


    //for selected month 
       $scope.setMonth = function(month) {
        $rootScope.month = month;
        $rootScope.state = $rootScope.state;

        $rootScope.monthshows = AllService.monthshows($rootScope.currentUser, $rootScope.month, $rootScope.state)
        $rootScope.monthshows.then(function(allshows) { 
            $rootScope.monthshowList = allshows.shows;
             
            if($rootScope.monthshowList == "") {
                $rootScope.noshow = 'No Show available for this month. Choose another month.';
                setTimeout(function() {
                    $rootScope.noshow = '';
                    $rootScope.$apply();
                }, 3000);
            }

            if(allshows.message) {
                $scope.error = allshows.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
            }
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });
    }

    $scope.viewProfileShows = function() { 
        for(var i=0; i<$rootScope.monthshowList.length; i++) { 
            if($rootScope.allshows == $rootScope.monthshowList[i].id) { 
                $rootScope.showDisplay = $rootScope.monthshowList[i].showName; 
            }
        }
    }

 
    //for add service by braider
    $scope.reserve= {};
    $scope.reservationResponseID = true;
    $scope.createReservation = function() {
    var total_price = 0;
     for(var p=0;p<$rootScope.braidServices.length;p++){
            var check = $rootScope.serviceArray.indexOf($rootScope.braidServices[p].id);
            if(check!=-1){
              total_price +=parseInt($rootScope.braidServices[p].price); 
             }
            } 
        $scope.reserve.date = [];
        $scope.reserve.horse_id = [];
        if($rootScope.dropSelectedDateId){
         $scope.reserve.date.push($rootScope.dropSelectedDateId);
        }
        if($rootScope.dropSelectedDateId1){
         $scope.reserve.date.push($rootScope.dropSelectedDateId1);   
        } 
         /*if($rootScope.horseId){
         $scope.reserve.horse_id.push($rootScope.horseId);
        }*/
        /*if($rootScope.horseId1){*/
         $scope.reserve.horse_id = $rootScope.horseId1
         if(!$rootScope.horseId1){
            var previosid = angular.fromJson(localStorage.getItem('horseid'));
            $scope.reserve.horse_id = previosid;
         }
         console.log($scope.reserve.horse_id);
        //}
        $scope.reserve.total_price = total_price;
        console.log('Final Price',$scope.reserve.total_price);
        $scope.reserve.state_id = $rootScope.state;
        $scope.reserve.show_id = $rootScope.allshows;

        //$scope.reserve.date = $rootScope.date;
        $scope.reserve.braider_id = $rootScope.braider_id;
        $scope.reserve.competitor_id = $rootScope.currentUser.User.id;
        $scope.reserve.access_token = $rootScope.currentUser.User.access_token;
        //$scope.reserve.horse_id = $rootScope.horseId;
        $scope.reserve.service_ids = $rootScope.serviceArray;
        $scope.newReservation = function() {
            return AllService.createReservation($scope.reserve);

          }                
        $scope.response = $scope.newReservation()
        $scope.showGreeting = false;
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);  
            } else { 
               $scope.msg="Reservation has been successfully scheduled You can Make another Reservation by open the above tabs as edit date";
               $scope.showGreeting = true;
               $timeout(function(){
                  $scope.showGreeting = false;
               }, 20000);
             $state.go('dashboard');
          }                    
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });   
        
    } 

    $scope.updateReservation = function() {
        var reservation = {};

        reservation.barnName = $scope.Singlereservation[0].barnName;
        reservation.barnNumber = $scope.Singlereservation[0].barnNumber;
        reservation.stallNumber = $scope.Singlereservation[0].stallNumber;
        reservation.reservation_id = $scope.Singlereservation[0].reservation_id;
        reservation.access_token = $rootScope.currentUser.User.access_token;
        reservation.competitor_id = $scope.Singlereservation[0].compid;

        $scope.updatereserve = function() {
            return AllService.updateReservation(reservation);
        }

        $scope.res = $scope.updatereserve()
        $scope.res.then(function(updateReserve) {
            $state.go('myreservation');
            //console.log('updateReserve', updateReserve)
            $rootScope.updateRes = updateReserve.message;
                setTimeout(function() {
                    $rootScope.updateRes = '';
                    $rootScope.$apply();
                }, 5000); 
        });

    }

    $scope.msgBraider = {};
    $scope.messageToBraider = function(reserveId, braidId) {
        $scope.msgBraider.to_user = braidId;
        $scope.msgBraider.access_token = $rootScope.currentUser.User.access_token;
        $scope.msgBraider.msg = $scope.txt;
        $scope.msgBraider.reservation_id = reserveId;
        console.log('$scope.msgBraider', $scope.msgBraider)
        $scope.sendBraiderMessage = function() {
            return AllService.sendMessage($scope.msgBraider);
        }
        $scope.response = $scope.sendBraiderMessage()
        $scope.response.then(function(data) {
            console.log('send message', data)
            $rootScope.message = data.message;
            setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000); 
        });
    } 
     //for send message to competitor
    $scope.msgCompetitor = {};
    $scope.messageToCompetitor = function(reserveId, competitorId) {
        $scope.msgCompetitor.to_user = competitorId;
        $scope.msgCompetitor.access_token = $rootScope.currentUser.User.access_token;
        $scope.msgCompetitor.msg = $scope.txt;
        $scope.msgCompetitor.reservation_id = reserveId;
        console.log('$scope.msgBraider', $scope.msgBraider)
        $scope.sendCompetitorMessage = function() {
            return AllService.sendMessage($scope.msgCompetitor);
        }
        $scope.response = $scope.sendCompetitorMessage()
        $scope.response.then(function(data) {
            console.log('send message', data)
            $rootScope.message = data.message;
            setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000); 
        });
    } 


    //for add service by braider
    $scope.service= {};
    $scope.preview = '';
    $scope.addNewService = function() {
        $scope.service.access_token = $rootScope.currentUser.User.access_token;
        $scope.service.braider_id = $rootScope.currentUser.User.id;
        $scope.newService = function() {
            return AllService.add($scope.service);
        }                
        $scope.response = $scope.newService()
        $scope.response.then(function(data){
            //console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);  
            } else {
               $rootScope.message = data.message;
               $state.go('my-services');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);  
            }                    
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        });   
        
    } 

    //for all braider services
    $rootScope.noService = false;
    $scope.braider_services = function() {

        $scope.listBraiderServices = AllService.all($rootScope.currentUser)
        $scope.listBraiderServices.then(function(data){
             $rootScope.AllBraiderServices = data.services;
           // console.log('$rootScope.AllBraiderServices', $rootScope.AllBraiderServices)
             if(!$rootScope.AllBraiderServices) {console.log('here')
                $rootScope.noService = true;
                $rootScope.noService = 'No services yet!'
            }
            var pagesShown = 1;
            var pageSize = 10;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.AllBraiderServices.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            }; 
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });

    }
    $scope.editingData = [];
    $scope.modify = function(tableData){
        $scope.editingData[tableData.id] = true;
    };
    $scope.updatetable = function(tableData){
        var tabledata = tableData
        var access_token = $rootScope.currentUser.User.access_token
        $scope.editingData[tableData.id] = false;
        AllService.updateService(tabledata,access_token).then(function(data)
       {
        console.log('submitted!!');
       }); 

    };

     $scope.selection=[];

     $scope.datetoggleSelection = function($event, date) {
    
         var check = $event.target;
         var checkState = (check.checked ? 'add' : 'remove');
         console.log('checkState', checkState);
         if(checkState == 'add') {
            $scope.selection.push(date);
        }else if(checkState == 'remove') {
            var selectedValue = date;
            var i = $scope.selection.indexOf(date);
            if(i != -1) {
                $scope.selection.splice(i, 1);
                console.log('selection11', $scope.selection)
            }
        }
        console.log('selection22', $scope.selection)
            $rootScope.dateArray = $scope.selection;
            console.log('$rootScope.dateArray', $rootScope.dateArray);
     };

     

     $scope.datetoggleSelection1 = function($event, date) {
        console.log('$event', $event, 'date', date, $rootScope.selection1);
         var check = $event.target;
         var checkState = (check.checked ? 'add' : 'remove');
         console.log('checkState', checkState);
         if(checkState == 'add') {
            $rootScope.selection1.push(date);
        }else if(checkState == 'remove') {
            var selectedValue = date;
            var i = $rootScope.selection1.indexOf(date);
            if(i != -1) {
                $rootScope.selection1.splice(i, 1);
                console.log('selection11', $rootScope.selection1)
            }
        }
        console.log('selection22', $rootScope.selection1)
            $rootScope.dateArray = $rootScope.selection1;
            console.log('$rootScope.dateArray', $rootScope.dateArray);
     };


    

     //for add show by braider
    $scope.show= {};
    //$scope.show.status = true;
    $scope.preview = '';
    $scope.braideraddshow = function() {
        
        $scope.show.access_token = $rootScope.currentUser.User.access_token;
        $scope.show.braider_id = $rootScope.currentUser.User.id;
        $scope.show.state_id = $rootScope.state;
        $scope.show.month_id = $rootScope.month;
        $scope.show.show_id = $rootScope.allshows;
        $scope.show.status = $scope.show.status;
        $scope.show.dates_available = $scope.dateArray;
        console.log('scope.show', $scope.show)
        $scope.newShow = function() {
            return Braiderportfolio.addShowByBraider($scope.show);
        }                
        $scope.response = $scope.newShow()
        $scope.response.then(function(data){
            console.log('dataaaa', data)
            
               $rootScope.showAdded = data.data.message;
               $state.go('show-schedule');
                setTimeout(function() {
                    $rootScope.showAdded = '';
                    $rootScope.$apply();
                }, 5000);  
                             
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 

        }); 
        //}
    } 

// =================Add show by braider function Ends======================= 

//listing of braider available shows
$rootScope.havingNoShow = false;
$scope.getShows = function() {
   
    $scope.listAllShows = AllService.getShows($rootScope.currentUser)
        $scope.listAllShows.then(function(data){console.log('dataaaa', data)
            $rootScope.listShows = data.BraiderAvialablity;
            console.log($rootScope.listShows);
            
            if($rootScope.listShows == '') {console.log('here')
                $rootScope.havingNoShow = true;
                $rootScope.havingNoShow = 'No Shows..';
            }
            var pagesShown = 1;
            var pageSize = 8;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.listShows.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            }; 
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
}

//get single show on the basis of show id
$scope.getSingleShow = function(showId) {
    console.log('showId', showId)
    $rootScope.braider_availId = showId;
console.log('$rootScope.months', $rootScope.months)

    $scope.SingleShow = AllService.getSingleShow($rootScope.currentUser, $rootScope.braider_availId)
        $scope.SingleShow.then(function(data){console.log('dataaaa', data)

            $rootScope.singleShow = data.BraiderAvialablity;
            $rootScope.singlestate = $rootScope.singleShow.state_id;
            $rootScope.singleshow = $rootScope.singleShow.show_id;
            $rootScope.datesAvailable = $rootScope.singleShow.all_dates_available;
            $rootScope.all_dates_available = $rootScope.singleShow.all_dates_available;
            
            $rootScope.braidershowseleteddate = $rootScope.singleShow.dates_available;
             $scope.selection = $rootScope.singleShow.dates_available;
             $rootScope.selection1 = $rootScope.singleShow.dates_available;
            $rootScope.singlemonth = data.BraiderAvialablity.month_id;
            console.log('$rootScope.selection1',$rootScope.selection1);
            console.log('$rootScope.singlemonth', $rootScope.singlemonth)
            //$rootScope.status = $rootScope.singleShow.status;


        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000); 
        });
}


$scope.selectDatespreselect = function(date){
      // console.log('datehdgshd', date)
       for(var i=0;i <$rootScope.singleShow.dates_available.length;i++){
        var index = $rootScope.singleShow.dates_available.indexOf(date);
         if(index != -1){
            return true;
          }else{
            return false;

          }
          
       }

}

// $scope.reset = function() {
    
//     $scope.singlemonth = '';
//     $scope.singleshow = '';
//     $scope.dateArray = '';
// }

//update show
$scope.updateShow = function() {

  for(var i = 0;i < $scope.selection.length;i++){
             $rootScope.braidershowseleteddate.push($scope.selection[i]);
        }
 

     var data = {};
        data.id = $scope.singleShow.id,
        data.access_token = $rootScope.currentUser.User.access_token,
        data.state_id = $scope.singlestate,
        data.show_id = $scope.singleshow,
        data.month_id = $scope.singlemonth,
        data.dates_available = $rootScope.braidershowseleteddate,
        //data.status = $scope.status
        //console.log('arr', arr)      
            
       $scope.updateShowData = function() {
            return AllService.updateShow(data);
        }                
        $scope.response = $scope.updateShowData()
        $scope.response.then(function(response){
           console.log('response', response)
            if ( data.status == 'error' ) {
                $scope.error = response.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);   
            } else {
                $rootScope.message = response.message; 
                //$state.go('dashboard');  
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 5000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        }); 

}

$scope.forgot = function(email) {
    console.log('email', email);
    $scope.forgotPassword = AllService.forgotPswd(email);
    $scope.forgotPassword.then(function(data){
        console.log('dataaaaaaaaa forget', data)
        $rootScope.message = data.message; 
        $state.go('login');
        setTimeout(function() {
            $rootScope.message = '';
            $rootScope.$apply();
        }, 5000);
});
}

}]);

app.controller('logoutController',['$scope','$rootScope','$window','$location', function($scope,$rootScope,$window,$location){
     $rootScope.currentUser = '';
     localStorage.removeItem('userdata');
     $location.path('/login');
}]);

/*Update Profile*/
app.controller('updateprofile',['$scope','$rootScope','$window','$location', function($scope,$rootScope,$window,$location){
     
    //$rootScope.currentUser=store.get('userdata');

}]);
app.controller('braiderportfolioController',['$scope','$rootScope', 'store','$window','Braiderportfolio','$timeout',function($scope,$rootScope, store,$window,Braiderportfolio,$timeout){

    
    Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
    {
       if(data.status=='success' && data.hasOwnProperty('message')){
       $scope.message = 'No image added yet.'
      }else{
         $scope.protfolio = data; 
         
         var pagesShown = 1;
            var pageSize = 6;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.protfolio.portfolio.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            }; 
        }
    });

    $scope.removePortfoliimage = function(p_id)
    {
    var access_token = $rootScope.currentUser.User.access_token;   
    Braiderportfolio.deleteWorkPicture(access_token,p_id).then(function(data){
    if(data.status=='success' && data.hasOwnProperty('message')){
      Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
      {
       $scope.protfolio = data; 
       if(data.status=='success' && data.hasOwnProperty('message')){
       //$scope.message = data.
      }else{
         $scope.protfolio = data;
         console.log('$scope.protfolio', $scope.protfolio)
         $scope.message = data.message; 
         $scope.detetePic = 'Image has been deleted successfully.'
            setTimeout(function() {
                    $scope.detetePic = '';
                    $scope.$apply();
                }, 5000);
        }
       });
          
     } 
    });

    $window.location.reload(); 
 }

    $scope.imageuploadbtn = true;
    $scope.showError = false;
    $scope.fileUpload1 = function(){
        $scope.fileData = null;
        var f = document.getElementById('3d_file').files[0];
        var size = f.size;
        $scope.size = size;
        if(size > 2000000){
             f = null;
          console.log("comes in if case");
          $scope.showError = 'Sorry, but that image is too large to upload. Please only upload images 2mb or smaller.';
          $timeout(function() {
          $scope.showError = '';
          }, 2000)
         }else{
         $scope.imageuploadbtn = false;
        }
        if(f) {
        r = new FileReader();
        $scope.filename = f.name;
        $scope.showImage = false;
        $scope.$apply();
        r.onload = function(e){
            $scope.fileData = e.target.result;
        document.getElementById('abc').src= e.target.result ;
        $scope.image = e.target.result; 
        $scope.$apply();
        };

             r.readAsDataURL(f);
        }
         
    };
    
    $scope.submit = function(){
    var access_token = $rootScope.currentUser.User.access_token; 
    console.log($scope.file);
      var imagedata = {

                            'image' : $scope.image,
                            'caption' : $scope.caption
                     };
                     $scope.image='';
                    $scope.caption='';
                 Braiderportfolio.addWorkPicture(imagedata,access_token).success(function(data) {
                   if(data.status=='success' && data.hasOwnProperty('message')){
                    
                    Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
                      {
                       if(data.status=='success' && data.hasOwnProperty('message')){
                       $scope.message = 'No image added yet.'

                      }else{
                         $scope.protfolio = data;  
                        }
                       });
                                     
                   };         
                             


                
               }).error(function(result){
                     
               }); 
               $window.location.reload(); 

                            
                         


    }
        


}]);
 


