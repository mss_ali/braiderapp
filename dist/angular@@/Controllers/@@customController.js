// app.controller('navController',['$scope','$http',function($scope,$http){
//  $scope.login = function(){
		 
// 	}
//  $scope.register = function(){
// 	}

// }]);


app.controller('homeController',['$scope','$http', '$state', '$rootScope', 'AllService','store', function($scope,$http, $state, $rootScope, AllService, store){
	$scope.items = {};
    $scope.openLogin = function() {
        $scope.login = function() {
            return AllService.login($scope.items);
        }
        $scope.response = $scope.login()
        $scope.response.then(function(userdata){
        	store.set('userdata', JSON.stringify(userdata));
        	 
        	$scope.user = store.get('userdata');
        	
        	if($scope.user.status == 'error') {
        		$scope.error1 = $scope.user.message;
                if($scope.error1 == 'email is required.') {
                    $scope.error = 'Sorry, it looks like your email is missing.';
                    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 2000);
                }
                if($scope.error1 == 'password is required.') {
                    $scope.error = 'Sorry, it looks like your password is missing.';
        		    setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                    }, 2000);
                }
        	}else {
        		$rootScope.currentUser = $scope.user.User;
                $state.go('dashboard');
        	}
            
        },function(error) {
            $scope.error = 'Invalid credentials given.';
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);
        });                
    };

    $scope.register = {};
    $scope.openRegister = function() {
        $scope.createAccount = function() {
            return AllService.register($scope.register);
        }                
        $scope.response = $scope.createAccount()
        $scope.response.then(function(data){
           // console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.errors = data.errors;
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.errors = '';
                    $scope.$apply();
                }, 2000); 
                if($scope.error == 'agree is required.') {
                   $scope.error = 'Terms and Conditions is required.'
                   setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);  
                }
            } else {
                                    
                $rootScope.message = data.message; 
                $state.go('login');  
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 2000);
                
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);

        });                
    };

    

}]);

app.controller('DashboardController',['$scope','$http', '$state', '$rootScope', 'AllService','store','Braiderportfolio', function($scope,$http, $state, $rootScope, AllService, store,Braiderportfolio){

    if(angular.isObject(store.get('userdata'))) {
        $rootScope.currentUser = store.get('userdata');
        $scope.userRole = $rootScope.currentUser.User.role;
        
    }
    //get my Reservation
     $scope.braiderReservation = function() {
        $rootScope.reservation = AllService.myReservation($rootScope.currentUser)
        $rootScope.reservation.then(function(response) {
            //console.log('response', response);
            $rootScope.reservations = response.braidereservations;
            //console.log('reservations', $rootScope.reservations);
            var pagesShown = 1;
            var pageSize = 4;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($rootScope.reservations.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);

        });
     }   
     //get profile
    $scope.profile = function() {
        
        $rootScope.user = AllService.getProfile($rootScope.currentUser)
        $rootScope.user.then(function(profile) {
            $scope.userProfile = profile.User;
            
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);

        });
    }

    //for update profile
    var arr = {};
    $scope.update = function() {
        arr.id = $rootScope.currentUser.User.id,
        arr.access_token = $rootScope.currentUser.User.access_token,
        arr.name = $scope.userProfile.name,
        arr.email = $scope.userProfile.email,
        arr.phone = $scope.userProfile.phone
        //console.log('arr', arr)      
            
       $scope.updateData = function() {
            return AllService.updateProfile(arr);
        }                
        $scope.response = $scope.updateData()
        $scope.response.then(function(data){
           // console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);   
            } else {
                $rootScope.message = data.message; 
                $state.go('dashboard');  
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 2000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);
        });                
             
    };


    //for add new horse
    $scope.horse ={};
    $scope.preview = '';
    $scope.addHorse = function() {
        $scope.horse.access_token = $rootScope.currentUser.User.access_token;
        $scope.horse.image = this.fileData;
       $scope.newHorse = function() {
            return AllService.horse($scope.horse);
        }                
        $scope.response = $scope.newHorse()
        $scope.response.then(function(data){
           // console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);   
            } else {
               $rootScope.message = data.message; 
               $state.go('dashboard');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 2000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
        });                
            
    } 
    
    //upload image
    $scope.fileUpload = function(){
        $scope.fileData = null;
        var f = document.getElementById('3d_file').files[0],
        r = new FileReader();
        $scope.filename = f.name;
        $scope.showImage = false;
        $scope.$apply();
        r.onload = function(e){//console.log('e', e)
            $scope.fileData = e.target.result;

        //  $scope.fileData1 = e.target.result;
        document.getElementById('abc').src= e.target.result ;
        $scope.image = e.target.result; 
        //console.log('image', $scope.image)
        $scope.$apply();
            
        };
             r.readAsDataURL(f);
    }; 


    //get listing of horses
    $scope.horseListing = function() {
        $rootScope.horses = AllService.getHorses($rootScope.currentUser)
        $rootScope.horses.then(function(horses) {
            $rootScope.horseList = horses.horses;
        }, function(error) {

            $scope.error = error;

        });
    }

    //for selected horse value
    $scope.setHorse = function(horse) {
        $scope.chooseHorse = horse;
    }

    //for get single horse details
    $scope.singleHorse = function(horse_id) {
        $rootScope.horseId = horse_id.id;
        //console.log('horseId', horseId)
            $rootScope.horseOne = AllService.getOne($rootScope.currentUser, $rootScope.horseId)
            $rootScope.horseOne.then(function(horse) {//console.log('horses', horse)
            
            $rootScope.horseSingle = horse.horse;
            //console.log('horsesingle', $rootScope.horseSingle)
            
            }, function(error) {

                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

            });
    }
    
    //for update horse detail
    $scope.updateDetail = function() {
        $scope.horseSingle.image = this.fileData;
        var arr = {};
        arr.access_token = $rootScope.currentUser.User.access_token,
        arr.id = $scope.horseSingle.id,
        arr.name = $scope.horseSingle.name,
        arr.gender = $scope.horseSingle.gender,
        arr.markings = $scope.horseSingle.markings,
        arr.color = $scope.horseSingle.color,
        arr.height = $scope.horseSingle.height,
        arr.quirks = $scope.horseSingle.quirks,
        arr.image = $scope.horseSingle.image
        
        $scope.updateData = function() {
            return AllService.update(arr);
        }                
        $scope.response = $scope.updateData()
        $scope.response.then(function(data){
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);   
            } else {
               $rootScope.message = data.message;
               $state.go('dashboard');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 2000);
            }                    
        },function(error)  {
            $scope.error = "Please fill all fields";
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
        });         
    }

    //for listing of braider
    $scope.braiderListing = function() {

        $rootScope.braider = AllService.all($rootScope.currentUser)
        $rootScope.braider.then(function(braiders) {
            $rootScope.braiderList = braiders.braiders.User;
        }, function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });
    }

    //for selected service
    $scope.array = [];
    $scope.selectedServices = function($event, arr) {
        $rootScope.abc = "true";
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        //console.log('action', action)
        if(action == 'add') {
            $scope.array.push(arr);
            $rootScope.serviceArray = $scope.array;
            //console.log('arrrrrr', $scope.serviceArray)
        }else if(action == 'remove') {
            var id = arr
            var i = $scope.array.indexOf(id);
            if(i != -1) {
                $scope.array.splice(i, 1);
            }
            $rootScope.serviceArray = $scope.array;
           // console.log('$scope.array', $scope.serviceArray)
        }
    } 
  
   


    //for selected braider
    $scope.setBraider = function(braid) {console.log('braid', braid)
        $rootScope.braider_id = braid;
        $scope.selectedBraidServices = AllService.braidService($rootScope.currentUser, $rootScope.braider_id)
        $scope.selectedBraidServices.then(function(data){
            $rootScope.braidServices = data.services;
            //console.log('$rootScope.braidServices', $rootScope.braidServices)
             
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
        });
        
    }
    var serviceArray = [];
    var price = 0;
    
    $scope.viewProfile = function() {
        //console.log('$scope.serviceArray', $rootScope.serviceArray, '$rootScope.braidServices', $rootScope.braidServices)
        for(var j=0; j<$rootScope.serviceArray.length; j++) {

            //console.log('$rootScope.serviceArray', $rootScope.serviceArray[j])
            $rootScope.servicesArray = $rootScope.serviceArray[j];

            for(var i=0; i<$rootScope.braidServices.length; i++) {

                //console.log('$rootScope.braidServices', $rootScope.braidServices[i].id)
                $rootScope.braidServicesArray = $rootScope.braidServices[i].id

                if($rootScope.servicesArray == $rootScope.braidServicesArray) {

                    //console.log('here', $rootScope.servicesArray, '===', $rootScope.braidServicesArray)
                    serviceArray.push($rootScope.braidServices[i]);
                    $rootScope.braiderServicesArray = serviceArray;
                    
                    //console.log('$rootScope.braiderServicesArray', $rootScope.braiderServicesArray)
                }
            }
        }
            for(var k=0; k<$rootScope.braiderServicesArray.length; k++) {
                price = parseInt(price) + parseInt($rootScope.braiderServicesArray[k].price);
                $rootScope.total_price = price;
            }
    }

    //for listing of braider
    $scope.showListing = function() {
    
        $rootScope.shows = AllService.shows($rootScope.currentUser)
        $rootScope.shows.then(function(allshows) {
            $rootScope.showList = allshows.shows;
            //console.log('shows', $rootScope.showList)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });
    }

    //for show schedule
     $scope.showSchedule = function() {
       $rootScope.showsSchedule = AllService.showsSchedule($rootScope.currentUser)
        $rootScope.showsSchedule.then(function(response) {
            $rootScope.showsSchedule = response.scheduleShow;
            console.log('showsschedule', response.scheduleShow.length)
            //view more functionality
            var pagesShown = 1;
            var pageSize = 8;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.showsSchedule.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };  
        }, function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });
    }


    //for selected show
    $scope.setShow = function(show, shows) {
        $rootScope.allshows = show;
        //console.log('allshows', $scope.allshows, 'shows', shows)
        if(shows && shows.$$state.value) {

            for(var i=0; i<shows.$$state.value.shows.length; i++) {
                //console.log('showId', shows.$$state.value.shows[i].id);
                if($scope.allshows == shows.$$state.value.shows[i].id) {
                    //console.log('here', shows.$$state.value.shows[i].id, '==', $scope.allshows) 
                    $scope.startDate = shows.$$state.value.shows[i].startDate;
                    $scope.endDate = shows.$$state.value.shows[i].endDate;
                    
                }
                
            }
            var split = $scope.startDate.split(" ");
            var start = split[0];

            var split1 = $scope.endDate.split(" ");
            var end =  split1[0];
           
            $scope.selectedDates = AllService.dates($rootScope.currentUser, start, end)
            $scope.selectedDates.then(function(data){ 
                $rootScope.nextDate = data.dates;
                
            },function(error) {
                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
            });
                    
         }
    }
    $rootScope.abc = "false";
    //for selected date
    $scope.setDate = function(date) {
        $rootScope.abc= "true";
        var service= [];
        $rootScope.date = date;
        //console.log('$scope.date', $rootScope.date, '$rootScope.allshows', $rootScope.allshows)
        $scope.availBraider = AllService.avail_braider($rootScope.currentUser, $rootScope.allshows, $scope.date)
        $scope.availBraider.then(function(data){ //console.log('daaaaataaaa', data)
            $rootScope.braiderAvailable = data.braiders;
            
            console.log('$rootScope.braiderAvailable', $rootScope.braiderAvailable)
            if(!$rootScope.braiderAvailable) {
                //console.log('no braider available')
                $rootScope.noBraider = 'No Braider available for this date. Choose another date.';
                setTimeout(function() {
                    $rootScope.noBraider = '';
                    $rootScope.$apply();
                }, 2000);
            }
            
            if(data.status == 'error'){
                $scope.error = data.errors;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
            }
        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
        });
    }


    //for show selected braid profile
    $scope.showProfile = function(braidId) {
        var braider_id = braidId;
        
            $rootScope.braiderOne = AllService.getOneBraider($rootScope.currentUser, braider_id)
            $rootScope.braiderOne.then(function(braider) {
            
            $rootScope.braiderSingle = braider.braider;
            
            }, function(error) {

                $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 


            });
    }

    //for competitor schedule reservation
    $scope.competitorReservation = function() {
    
        $rootScope.shows = AllService.getReservation($rootScope.currentUser)
        $rootScope.shows.then(function(reservation) {//console.log('reservation', reservation)
            $rootScope.reservations = reservation.competeterreservations;

        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });
    }

    //for single reservation
    $scope.competitorSingleReservation = function(reserveId) {
        $rootScope.reservationId = reserveId.reservation_id;
        
        $rootScope.shows = AllService.getSingleReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.shows.then(function(reservation) {//console.log('Singlereservation', reservation)
            $rootScope.Singlereservation = reservation.competeterreservations;

        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });
    }

    $scope.cancelReservation = function(data) {
        var Confirm = confirm("Are you sure you want to cancel your reservation?");
        if (Confirm == true) {
                $rootScope.reserve_Id = data.reservation_id;
            $rootScope.competitor_Id = data.compid;

            $rootScope.cancel = AllService.cancelReservation($rootScope.currentUser, $rootScope.reserve_Id, $rootScope.competitor_Id)
            $rootScope.cancel.then(function(cancelReservation) {
                
                $rootScope.cancel = cancelReservation.message;
                   $state.go('dashboard');
                    setTimeout(function() {
                        $rootScope.cancel = '';
                        $rootScope.$apply();
                    }, 2000);
            }, function(error) {

                $scope.error = error;
                    setTimeout(function() {
                        $scope.error = '';
                        $scope.$apply();
                    }, 2000); 

            });
        }

    }

    $scope.braiderSingleReservation = function(reservationId) {
        $rootScope.reservationId = reservationId.reservation_id;
        

        $rootScope.reservation = AllService.braiderReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.reservation.then(function(reservation) {
            $rootScope.reservationSingle = reservation.braidereservations;
            //console.log('reservations', $rootScope.reservationSingle)
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });

    }

    $scope.acceptRejectReservation = function(data, value) {
       // console.log('data', data, 'value', value);
         $rootScope.reserve_Id = data.reservation_id;
            $rootScope.braider_Id = data.braider_id;
            $rootScope.value = value;
        // var Confirm = confirm("Are you sure you want to cancel your reservation?");
        // if (Confirm == true) {
        //         $rootScope.reserve_Id = data.reservation_id;
        //     $rootScope.competitor_Id = data.compid;

        //     $rootScope.cancel = AllService.acceptReject($rootScope.currentUser, $rootScope.reserve_Id, $rootScope.competitor_Id)
        //     $rootScope.cancel.then(function(acceptReject) {console.log('cancelReservation', acceptReject)
                
        //         $rootScope.acceptReject = acceptReject.message;
        //            $state.go('dashboard');
        //             setTimeout(function() {
        //                 $rootScope.cancel = '';
        //                 $rootScope.$apply();
        //             }, 2000);
        //     }, function(error) {

        //         $scope.error = error;
        //             setTimeout(function() {
        //                 $scope.error = '';
        //                 $scope.$apply();
        //             }, 2000); 

        //     });
        // }

    }

    $scope.getState = function() {
        $rootScope.states = AllService.getState($rootScope.currentUser)
        $rootScope.states.then(function(allstates) {
            $rootScope.allStates = allstates.State;
            
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);

        });
    }

    $scope.setState = function(state) {
        $rootScope.state = state;
        
        for(var i=0; i<$rootScope.allStates.length; i++) {
            
            if($rootScope.state == $rootScope.allStates[i].id) {
                $rootScope.stateName = $rootScope.allStates[i].name;
            }
        }
    }

    $scope.months = [{name:'All', id:'0'},{name:'January', id:'1'}, {name:'February', id:'2'}, {name:'March', id:'3'}, {name:'April', id:'4'}, {name:'May', id:'5'}, {name:'June', id:'6'}, {name:'July', id:'7'}, {name:'August', id:'8'}, {name:'September', id:'9'}, {name:'October', id:'10'}, {name:'November', id:'11'}, {name:'December', id:'12'}];

    //for selected month 
    $scope.setMonth = function(month) {
        $scope.month = month;
        $rootScope.monthshows = AllService.monthshows($rootScope.currentUser, $scope.month)
        $rootScope.monthshows.then(function(allshows) {
            $rootScope.monthshowList = allshows.shows;
            

            if($rootScope.monthshowList == "") {
                $rootScope.noshow = 'No Show available for this month. Choose another month.';
                setTimeout(function() {
                    $rootScope.noshow = '';
                    $rootScope.$apply();
                }, 2000);
            }

            if(allshows.message) {
                $scope.error = allshows.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
            }
        }, function(error) {

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });
    }

    $scope.viewProfileShows = function() {
        //console.log('$rootScope.allshows', $rootScope.allshows, '$rootScope.monthshowList', $rootScope.monthshowList)
        for(var i=0; i<$rootScope.monthshowList.length; i++) {
           // console.log($rootScope.monthshowList[i])
            if($rootScope.allshows == $rootScope.monthshowList[i].id) {
               // console.log($rootScope.allshows, '===', $rootScope.monthshowList[i].id)
                $rootScope.showDisplay = $rootScope.monthshowList[i].showName;
               // console.log('$rootScope.showDisplay', $rootScope.showDisplay)
            }
        }
    }

    //for add service by braider
    $scope.reserve= {};
    //$scope.preview = '';
    $scope.createReservation = function() {
        $scope.reserve.state_id = $rootScope.state;
        $scope.reserve.show_id = $rootScope.allshows;
        $scope.reserve.date = $rootScope.date;
        $scope.reserve.braider_id = $rootScope.braider_id;
        $scope.reserve.competitor_id = $rootScope.currentUser.User.id;
        $scope.reserve.access_token = $rootScope.currentUser.User.access_token;
        $scope.reserve.horse_id = $rootScope.horseId;
        $scope.reserve.service_ids = $rootScope.serviceArray;
        $scope.reserve.total_price = $rootScope.total_price;
        $scope.newReservation = function() {
            return AllService.createReservation($scope.reserve);
        }                
        $scope.response = $scope.newReservation()
        $scope.response.then(function(data){
            //console.log('dataaaaFinal', data)
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);  
            } else {
               $rootScope.scheduleBraid = data.message;
               //$state.go('dashboard');
                setTimeout(function() {
                    $rootScope.scheduleBraid = '';
                    $rootScope.$apply();
                }, 2000);  
            }                    
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });   
        
    } 


    //for add service by braider
    $scope.service= {};
    $scope.preview = '';
    $scope.addNewService = function() {
        $scope.service.access_token = $rootScope.currentUser.User.access_token;
        $scope.service.braider_id = $rootScope.currentUser.User.id;
        $scope.newService = function() {
            return AllService.add($scope.service);
        }                
        $scope.response = $scope.newService()
        $scope.response.then(function(data){
            //console.log('dataaaa', data)
            if ( data.status == 'error' ) {
                $scope.error = data.message;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000);  
            } else {
               $rootScope.message = data.message;
               $state.go('dashboard');
                setTimeout(function() {
                    $rootScope.message = '';
                    $rootScope.$apply();
                }, 2000);  
            }                    
        },function(error)  {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 

        });   
        
    } 

    //for all braider services
    $scope.braider_services = function() {

        $scope.listBraiderServices = AllService.all($rootScope.currentUser)
        $scope.listBraiderServices.then(function(data){
            if(data.status==="success" && data.hasOwnProperty('message')){
             $rootScope.AllBraiderServicesMessage = 'No service is available' 
            }else{
             $rootScope.AllBraiderServices = data.services;
             }

        },function(error) {
            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 2000); 
        });

    }
    $scope.editingData = [];
    $scope.modify = function(tableData){
        $scope.editingData[tableData.id] = true;
    };
    $scope.updatetable = function(tableData){
        var tabledata = tableData
        var access_token = $rootScope.currentUser.User.access_token
        $scope.editingData[tableData.id] = false;
        AllService.updateService(tabledata,access_token).then(function(data)
       {
        
       }); 

    };
   http://mastersoftwaretechnologies.com/braider/braiderapp/shows/addShow
   {"state_id":"1","show_id":"1","dates_available":["2015-07-11","2015-07-12","2015-07-13"],"status":"1","access_token":"caca42b9e4f4c722edfb8cefa0582f39"}

   // =================Add show by braider function start=======================
    $scope.show = {};
    $scope.braideraddshow = function(){
        console.log('state', $rootScope.state, )
        $scope.show.access_token =  $rootScope.currentUser.User.access_token;
        $scope.show.state_id = $rootScope.state;
        $scope.show.showname = 
       var braideraddshowdata =  {

                                 'state_id': $scope.allstate,
                                 'show_id':$scope.showname,
                                 'dates_available': $scope.braidershowseleteddate,
                                  'access_token':$rootScope.currentUser.User.access_token, 
                                  'status':'1',
                                  'braider_id':$rootScope.currentUser.User.id
                              }
                console.log('braideraddshowdata',braideraddshowdata)

                Braiderportfolio.addShowByBraider(braideraddshowdata).then(function(data){
                   if(data.status=="success" && data.hasOwnProperty("message")){
                    console.log('Show added');
                   }
                   $state.go('show-schedule')
                });                  
    },
    $scope.braidershowseleteddate = []
    $scope.logInfos = function(event, date) {
        event.preventDefault() 
        date.selected = !date.selected
        var clickdate = date.valueOf()
        if(date.selected){
         $scope.braidershowseleteddate.push(clickdate);
         }else{
               var index = $scope.braidershowseleteddate.indexOf(clickdate);
               $scope.braidershowseleteddate.splice(index, 1);
          }
        }
// =================Add show by braider function Ends=======================        

}]);


app.controller('logoutController',['$scope','$rootScope','$window','$location', function($scope,$rootScope,$window,$location){
     
     localStorage.removeItem('userdata');
     $rootScope.currentUser = null
     $location.path('/login');
     location.reload();




}]);

app.controller('braiderportfolioController',['$scope','$rootScope', 'store','$window','Braiderportfolio',function($scope,$rootScope, store,$window,Braiderportfolio){

    
    Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
    {
       if(data.status=='success' && data.hasOwnProperty('message')){
       $scope.message = 'No image added yet.'
      }else{
         $scope.protfolio = data;  
        }
    });

    $scope.removePortfoliimage = function(p_id)
    {
    var access_token = $rootScope.currentUser.User.access_token;   
    Braiderportfolio.deleteWorkPicture(access_token,p_id).then(function(data){
    if(data.status=='success' && data.hasOwnProperty('message')){
      Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
      {
       if(data.status=='success' && data.hasOwnProperty('message')){
       $scope.message = 'No image added yet.'
      }else{
         $scope.protfolio = data;  
        }
       });
          
     } 
    });
    }
    $scope.fileUpload = function(){
        $scope.fileData = null;
        var f = document.getElementById('3d_file').files[0],
        r = new FileReader();
        $scope.filename = f.name;
        $scope.showImage = false;
        $scope.$apply();
        r.onload = function(e){
            $scope.fileData = e.target.result;
        document.getElementById('abc').src= e.target.result ;
        $scope.image = e.target.result; 
        $scope.$apply();
        };
             r.readAsDataURL(f);
    }; 
    
    $scope.submit = function(){
    var access_token = $rootScope.currentUser.User.access_token; 
      var imagedata = {

                            'image' : $scope.image,
                            'caption' : $scope.caption
                     };
                     $scope.image='';
                    $scope.caption='';
                 Braiderportfolio.addWorkPicture(imagedata,access_token).success(function(data) {
                   if(data.status=='success' && data.hasOwnProperty('message')){
                    
                    Braiderportfolio.getPortfolio($rootScope.currentUser.User.access_token).then(function(data)
                      {
                       if(data.status=='success' && data.hasOwnProperty('message')){
                       $scope.message = 'No image added yet.'

                      }else{
                         $scope.protfolio = data;  
                        }
                       });
                                     
                   };         
                             
                            
                           }).error(function(result){
                                 
                           }); 

    }


}]);
 


