app.controller('CalendarCtrl',
   function($scope,AllService,$rootScope,store, $compile, $timeout, uiCalendarConfig,$filter,$location) {
  if(angular.isObject(store.get('userdata'))) {
        $rootScope.currentUser = store.get('userdata');
        $rootScope.braider_id = $rootScope.currentUser.User.id;
         if($rootScope.currentUser.User != undefined) 
        $scope.userRole = $rootScope.currentUser.User.role;
    }
    //get my Reservation
      $scope.eventSources2 = [];
      $rootScope.valuess = [];
      $rootScope.braider_url =[];
      $rootScope.avialablity=[];
      $rootScope.braider_date =[];
      $rootScope.reservation = AllService.avail_reservation($rootScope.braider_id);
      $rootScope.reservation.then(function(response) {
             angular.forEach(response, function(values) {
              if(values !== true){
              $scope.msg =  values.msg;
              $scope.reser = values.reservation;
              $scope.date_resp = values.date;
           $scope.date_filter=  $filter('date')(new Date($scope.date_resp), 'yyyy-MM-dd');
           $scope.month=  $filter('date')(new Date($scope.date_resp), 'MM');
           $scope.date=  $filter('date')(new Date($scope.date_resp), 'dd');
           $scope.year=  $filter('date')(new Date($scope.date_resp), 'yyyy');
           if($scope.reser == '0'){
               $rootScope.valuess.push({data:$scope.date_resp,start: new Date($scope.year,$scope.month - 1,$scope.date),res:$scope.reser,allDay:true ,msg: $scope.msg});
           }else{
               $rootScope.braider_url.push({data:$scope.date_resp,start: new Date($scope.year,$scope.month - 1,$scope.date),res:$scope.reser,allDay:true ,msg: $scope.msg});
               $rootScope.braider_date.push($scope.date_resp);
           }
                 }});
           angular.forEach(response[0].avialablity,function(avail_date){
            $scope.date_perr= moment(avail_date).format('YYYY-MM-DD');
             $scope.month=  $filter('date')(new Date(avail_date), 'MM');
           $scope.date = $filter('date')(new Date(avail_date), 'dd');
           $scope.year1=  $filter('date')(new Date(avail_date), 'yyyy');
           $rootScope.avialablity.push({data:avail_date,start: $scope.date_perr,res:'0',allDay:true ,msg:'0'});
           })
            $scope.eventSources2.push($rootScope.valuess,$rootScope.braider_url,$rootScope.avialablity);
            /* Render Tooltip */
  //   $scope.eventRender = function( event, element, view ) {
  //       $compile(element)($scope);
  //       $scope.ddMMyy = $filter('date')(new Date(), 'yyyy-MM-dd');
  //        $('[class*="fc"]').filter(function() {
  //        return $(this).data('date') == event.data;
  //         }).addClass("intro2");
  //             if(event.res !='0'){
  //                 $('[class*="fc"]').filter(function(){
  //                 return $(this).data('date') == event.data;
  //                 }).html("<div class='p'><br/><img height=10px width=10px src=dist/css/nail.png></div>").addClass("intro2");
  //                     if(event.msg != '0'){
  //                         $('[class*="fc"]').filter(function() {
  //                         return $(this).data('date') == event.data;
  //                         }).html("<div class='p'><br/><img height=10px width=10px src=dist/css/nail.png><br><img height=20px width=30px src=dist/css/mail.png></div>")
  //                         .addClass("intro2");
  //                     }
  //             }
  // }
  $scope.eventRender = function( event, element, view ) {
        $compile(element)($scope);
        $scope.datew = moment(event.start).format("YYYY-MM-DD");
        $scope.ddMMyy = moment().format("YYYY-MM-DD");
         $('[class*="fc"]').filter(function() {
         return $(this).data('date') == event.data;
          }).addClass("intro2");
              if(event.res !='0'){
                var dateString = moment(event.data).format('YYYY-MM-DD');
                $('.calendar').find('.fc-day-number[data-date1="' + dateString + '"]').css('cursor', 'pointer');
                  $('[class*="fc"]').filter(function(){
                  return $(this).data('date') == event.data;
                  }).html("<div class='p'><br/><img height=10px width=10px src=dist/css/nail.png></div>").css('cursor', 'pointer').addClass("intro2");
                      if(event.msg != '0'){
                          $('[class*="fc"]').filter(function() {
                          return $(this).data('date') == event.data;
                          }).html("<div class='p'><br/><img height=10px width=10px src=dist/css/nail.png><br><img height=20px width=30px src=dist/css/mail.png></div>")
                          .css('cursor', 'pointer').addClass("intro2");
                      }
              }
  }
    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 600,
        editable: false,
      // handleWindowResize:true,
      //   timezone:'America/Los Angeles',
        ignoreTimezone:false,
        allDayDefault:true,
         timezone : 'local',
                 header:{
          center: 'title',
          right: 'today',
          left: 'prev,next'
        },

          dayClick: $scope.alertOnEventClick,
        // eventClick: $scope.alertOnEventClick,
        eventRender: $scope.eventRender,
      },

    };
    // $scope.eventSources2 = [$scope.calEventsExt]
        }, function(error) {

            $scope.error = error;
            setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        });
/**
 * calendarDemoApp - 0.9.0
 */
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    // $scope.changeTo = 'Hungarian';
    // /* event source that pulls from google.com */
    // $scope.eventSource = {
    //         url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
    //         className: 'gcal-event',           // an option!
    //         currentTimezone: 'America/Chicago' // an option!
    // };
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    //with this you can handle the events that generated by clicking the day(empty spot) in the calendar
// $scope.dayClick = function (date, allDay, jsEvent, view) {
   
//         alert('you clicked' + date);
    
// };
    /* alert on eventClick */
    $scope.alertOnEventClick = function( date, cell){
      var braider_date = moment(date).format('YYYY-MM-DD');
       angular.forEach($rootScope.braider_date, function(value1) {
    var date1 =  moment(value1).format('YYYY-MM-DD');
        if (date1 == braider_date) {
      $location.path('/reservation-list');
      $rootScope.date = date1;
        }
});
     }
    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      $timeout(function() {
        if(uiCalendarConfig.calendars[calendar]){
          uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
      });
    };

     // $scope.eventSources2.push( $scope.eventsF);
});
app.controller('reservation',function($scope,$http,$rootScope,$filter,AllService,$location){
        $scope.date_reservation = moment($rootScope.date).format('DD');
        $scope.month_reservation = moment($rootScope.date).format('MM'); 
        $scope.year_reservation = moment($rootScope.date).format('YYYY');  
        var monthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December' ];
        var date_reservation = moment($rootScope.date).format('YYYY-MM-DD');
        $scope.month = monthNames[$scope.month_reservation-1];
        $rootScope.all_reservation = AllService.reservation(date_reservation,$rootScope.currentUser);
        $rootScope.all_reservation.then(function(response) {
            $rootScope.braider =  response.braidereservations;
            if($rootScope.braider ==''){
              $location.path('/calendar');
            }
               });
        $scope.nextdate = function(date){
          if(date === undefined){
            date = date_reservation;
          }
          $rootScope.all_reservation = AllService.reservation1(date,$rootScope.currentUser,'next');
          $rootScope.all_reservation.then(function(response) {
             if(response.statusCode){
                  $rootScope.next_button = true;
          }  else{
            $rootScope.prev_button = false;
          $rootScope.braider =  response.braidereservations;
            $rootScope.date = response.braidereservations[0].date;
             $scope.date_reservation = moment($rootScope.date).format('DD');
        $scope.month_reservation = moment($rootScope.date).format('MM'); 
        $scope.year_reservation = moment($rootScope.date).format('YYYY');
        $scope.month = monthNames[$scope.month_reservation-1];
        }   });
        
        };
        $scope.prevdate = function(date){
           if(date === undefined){
            date = date_reservation;
          }
          $rootScope.all_reservation = AllService.reservation1(date,$rootScope.currentUser,'previous');
          $rootScope.all_reservation.then(function(response) {
           if(response.statusCode){
                $rootScope.prev_button = true;
          }  else{
              $rootScope.next_button = false;
            $rootScope.braider =  response.braidereservations;
              $rootScope.date = response.braidereservations[0].date;
             $scope.date_reservation = moment($rootScope.date).format('DD');
             $scope.month_reservation = moment($rootScope.date).format('MM'); 
             $scope.year_reservation = moment($rootScope.date).format('YYYY');  
             $scope.month = monthNames[$scope.month_reservation-1];
          }   
   });
        };  
         $scope.braiderSingleReservation = function(reservationId) {
          $location.path('/reservation-detail');
        $rootScope.reservationId = reservationId.reservation_id;
        $rootScope.reservation = AllService.braiderReservation($rootScope.currentUser, $rootScope.reservationId)
        $rootScope.reservation.then(function(reservation) {
            $rootScope.reservationSingle = reservation.braidereservations; 
            $rootScope.res_number2  = reservation.braidereservations[0].res_number;
            $scope.braider_id = reservation.braidereservations[0].braider_id;
            $scope.comp_id =reservation.braidereservations[0].compitetor_id;
            $scope.messages_list = AllService.competitorMessage($rootScope.reservationId);
            $scope.messages_list.then(function(message){
                $rootScope.Allmessage = message;
                
    });
        }, function(error){

            $scope.error = error;
                setTimeout(function() {
                    $scope.error = '';
                    $scope.$apply();
                }, 5000);
        });
    }
$scope.sortableOptions = {
stop: function(data,e,tr) {
      $rootScope.resvervation_id = [];
angular.forEach($rootScope.braider,function(response){
  if(response.reservation_id){
    $rootScope.resvervation_id.push(response.reservation_id);
  }
});
$scope.reoder  = AllService.arrangement($rootScope.resvervation_id);
$scope.reoder.then(function(message){
});  
},
axis: 'X'
};
});
app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});
